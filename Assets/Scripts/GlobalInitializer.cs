﻿using System.Collections.Generic;
using Cinemachine;
using Common.LateSystems;
using Common.UpdateSystems;
using Scripts;
using Scripts.HUD.Components;
using DI.Builders;
using EnvironmentSpawn.Components;
using EnvironmentSpawn.Systems;
using Gameplay.Components;
using Gameplay.Systems;
using GUI.Components;
using GUI.Containers;
using Level.Components;
using Level.Systems;
using Scellecs.Morpeh;
using Player;
using Player.Systems;
using Pools;
using Res.Components;
using Res.Systems;
using RoadSpawn.Systems;
using Scores.Components;
using Scores.Systems;
using Scripts.GUI;
using Scripts.HUD;
using Scripts.NPCSpawn.Systems;
using Services;
using Settings.Components;
using Settings.ScriptableObjects.References;
using Settings.Systems;
using SoundPlaying.ScriptableObjects;
using SoundPlaying.UpdateSystems;
using UnityEngine;
using Vehicles;

public class GlobalInitializer : MonoBehaviour
{
    private static List<ISystemsFeature> _features = new List<ISystemsFeature>
    {
#if !VEHICLES_TEST_ENABLED
        new HudFeature(),
        new GUISystemsFeature(),
#endif
        new VehiclesSystemsFeature(),
    };

    [SerializeField] private ReferencesContainer _referencesContainer;
    [SerializeField] private SoundData _soundContainer;
    [SerializeField] private SettingsContainer _settingsContainer;
    
    private void Start()
    {
        IContainerBuilder containerBuilder = new ContainerBuilder();
        
        foreach (var referenceObject in _referencesContainer.References)
        {
            containerBuilder.RegisterAs(referenceObject.GetType(), referenceObject);
        }
        containerBuilder.Register(_soundContainer);
        containerBuilder.Register(FindObjectOfType<UiCanvasContainer>());
        containerBuilder.Register<ICameraService>(new CameraService(FindObjectOfType<CinemachineBrain>()));
        containerBuilder.Register(new AudioSourcePool());
        containerBuilder.Register(_settingsContainer);

        var settings = FindObjectOfType<Scripts.Settings.Mono.GlobalSettings>();
        containerBuilder.Register(settings.soundSettings);
        containerBuilder.Register(settings.levelSettings);
        containerBuilder.Register(settings.vehicleSettings);
        containerBuilder.Register(settings.hudSettings);
        
        containerBuilder.Register<IPlayerData>(new PlayerData());

        int groupIndex = 0;
        var world = World.Default;

        var commonGroup = world.CreateSystemsGroup();

        // ------- Initializers
        commonGroup.AddInitializer(containerBuilder.CreateAndRegister<SoundFactorySystem>());
        commonGroup.AddInitializer(containerBuilder.Create<StartGameSystem>());

        // ------- Update systems
        // Gameplay
        commonGroup.AddSystem(containerBuilder.Create<LoseGameSystem>());

        // Player
        commonGroup.AddSystem(containerBuilder.Create<PlayerInitializeDataSystem>());
        commonGroup.AddSystem(containerBuilder.Create<PlayerWriteDataSystem>());
        
        // Settings
        commonGroup.AddSystem(containerBuilder.Create<ReadGlobalSettingsSystem>());
        commonGroup.AddSystem(containerBuilder.Create<WriteGlobalSettingsSystem>());
        
        // Sound systems 
        commonGroup.AddSystem(containerBuilder.Create<SoundAudioClipSystem>());
        commonGroup.AddSystem(containerBuilder.Create<SoundUpdateOnSettingsChangeSystem>());
        commonGroup.AddSystem(containerBuilder.Create<SoundVolumeSystem>());
        commonGroup.AddSystem(containerBuilder.Create<SoundLoopSystem>());
        commonGroup.AddSystem(containerBuilder.Create<SoundPlaySystem>());
        commonGroup.AddSystem(containerBuilder.Create<SoundCheckFinishSystem>());

#if !VEHICLES_TEST_ENABLED
        // Resources
        commonGroup.AddSystem(containerBuilder.Create<UpdateMoneyViewSystem>());
        
        // Level
        commonGroup.AddSystem(containerBuilder.Create<InitializeLevelSystem>());
        commonGroup.AddSystem(containerBuilder.Create<InitializePlayerSystem>());
        commonGroup.AddSystem(containerBuilder.Create<GetCurrentScoreSystem>());
        commonGroup.AddSystem(containerBuilder.Create<ResetScoresSystem>());
        
        // NPC
        commonGroup.AddSystem(containerBuilder.Create<DestroyNpcSpawnerSystem>());
        commonGroup.AddSystem(containerBuilder.Create<InitializeNPCSpawnerSystem>());
        commonGroup.AddSystem(containerBuilder.Create<NPCSpawnSystem>());
        commonGroup.AddSystem(containerBuilder.Create<DestroyNpcOnDistanceSystem>());
        
        commonGroup.DeleteHere<InitializeLevel>();
        
        commonGroup.AddSystem(containerBuilder.Create<CleanLevelSystem>());
#endif
        
        // Common
        commonGroup.AddSystem(containerBuilder.Create<TimerUpdateSystem>());
        
        // Road spawn systems
        commonGroup.AddSystem(containerBuilder.Create<RoadSpawnInitializeSystem>());
        commonGroup.AddSystem(containerBuilder.Create<RoadSpawnSystem>());
        commonGroup.AddSystem(containerBuilder.Create<EnvironmentSpawnSystem>());
        commonGroup.AddSystem(containerBuilder.Create<ApplyRandomTextureSystem>());
        
        commonGroup.DeleteHereLate<PlayerUpdateResource>();
        commonGroup.DeleteHereLate<GlobalSettingsChanged>();
        commonGroup.DeleteHereLate<ReadGlobalSettings>();
        commonGroup.DeleteHereLate<WriteGlobalSettings>();
        commonGroup.DeleteHereLate<RandomTextureInitialize>();
        commonGroup.DeleteHereLate<DestroyHud>();
        commonGroup.DeleteHereLate<CreateCountDown>();
        commonGroup.DeleteHereLate<UiInitializePanel>();
        commonGroup.DeleteHereLate<ResetScores>();
        commonGroup.DeleteHereLate<PauseGame>();
        commonGroup.DeleteHereLate<ResumeGame>();

        // Cleanup systems
        commonGroup.AddSystem(containerBuilder.Create<SoundCleanupSystem>());
        commonGroup.AddSystem(containerBuilder.Create<DestroyCurrentLevelSystem>());
        commonGroup.AddSystem(containerBuilder.Create<DestroyCleanupSystem>());
        commonGroup.AddSystem(containerBuilder.Create<DestroySystem>());
        commonGroup.DeleteHereLate<DestroyCurrentLevel>();

        world.AddSystemsGroup(groupIndex++, commonGroup);

        foreach (var feature in _features)
        {
            world.AddSystemsGroup(groupIndex++, feature.GetGroup(world, containerBuilder));
        }
    }
}