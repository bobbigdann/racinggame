﻿using System.Collections.Generic;
using JsonModels;
using Res.Data;

namespace Player
{
    public interface IPlayerData
    {
        Dictionary<ResourceType, int> Resources { set; get; }
        List<int> Vehicles { set; get; }

        void Initialize(UserModel model);
    }
}