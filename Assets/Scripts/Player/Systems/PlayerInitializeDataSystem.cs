﻿using System;
using System.Linq;
using Constants;
using DI.Attributes;
using JsonModels;
using Scellecs.Morpeh;
using Newtonsoft.Json;
using Player.Components;
using Res.Data;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Player.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class PlayerInitializeDataSystem : ISystem
    {
        [Inject] private readonly IPlayerData _playerData;
        [Inject] private Stash<PlayerInitializeData> _playerInitializeData;
        
        public World World { get; set; }

        private Filter _readFilter;
        
        public void OnAwake()
        {
            _readFilter = World.Filter.With<PlayerInitializeData>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_readFilter.IsEmpty()) return;
            
            var json = PlayerPrefs.GetString(PlayerPrefsKeys.PlayerData, string.Empty);
            if (json.Equals(string.Empty))
            {
                var userModel = new UserModel();
                foreach (var item in Enum.GetValues(typeof(ResourceType)).Cast<ResourceType>())
                {
                    userModel.Resources.Add(item, default);
                }
                userModel.Vehicles.Add(0);
                _playerData.Initialize(userModel);
            }
            else
            {
                _playerData.Initialize(JsonConvert.DeserializeObject<UserModel>(json));
            }

            foreach (var entity in _readFilter)
            {
                _playerInitializeData.Remove(entity);
            }
        }

        public void Dispose()
        {
            _readFilter = null;
        }
    }
}