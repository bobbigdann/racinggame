﻿using Constants;
using DI.Attributes;
using JsonModels;
using Scellecs.Morpeh;
using Newtonsoft.Json;
using Res.Components;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Player.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class PlayerWriteDataSystem : ISystem
    {
        [Inject] private readonly IPlayerData _playerData;
        
        public World World { get; set; }

        private Filter _updateResourcesFilter;
        
        public void OnAwake()
        {
            _updateResourcesFilter = World.Filter.With<PlayerUpdateResource>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_updateResourcesFilter.IsEmpty()) return;

            var userModel = new UserModel
            {
                Vehicles = _playerData.Vehicles,
                Resources = _playerData.Resources
            };
            
            PlayerPrefs.SetString(PlayerPrefsKeys.PlayerData, JsonConvert.SerializeObject(userModel));
            PlayerPrefs.Save();
        }
        
        public void Dispose()
        {
            _updateResourcesFilter = null;
        }
    }
}