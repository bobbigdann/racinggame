﻿using System.Collections.Generic;
using JsonModels;
using Res.Data;

namespace Player
{
    public class PlayerData : IPlayerData
    {
        public Dictionary<ResourceType, int> Resources { get; set; }
        public List<int> Vehicles { set; get; } = new();

        public void Initialize(UserModel userModel)
        {
            Resources = userModel.Resources;
            Vehicles = userModel.Vehicles;
        }
    }
}