﻿using Cinemachine;
using UnityEngine;

namespace Services
{
    public class CameraService : ICameraService
    {
        public Camera Camera { get; }
        public ICinemachineCamera CinemachineCamera { get; }

        private readonly Transform _cameraTransform;
        private readonly Transform _virtualTransform;

        public CameraService(CinemachineBrain brain)
        {
            Camera = brain.OutputCamera;
            CinemachineCamera = brain.ActiveVirtualCamera as CinemachineFreeLook;

            _cameraTransform = Camera.transform;
            _virtualTransform = CinemachineCamera.VirtualCameraGameObject.transform;
        }

        public void SetLookAt(Transform transform)
        {
            CinemachineCamera.LookAt = transform;
        }

        public void SetFollowTo(Transform transform)
        {
            CinemachineCamera.Follow = transform;
        }

        public void SetPosition(Vector3 position, bool isForce = false)
        {
            if (isForce) _cameraTransform.position = position;
            _virtualTransform.position = position;
        }

        public void SetRotation(Quaternion rotation, bool isForce = false)
        {
            if (isForce) _cameraTransform.rotation = rotation;
            _virtualTransform.rotation = rotation;
        }
    }
}