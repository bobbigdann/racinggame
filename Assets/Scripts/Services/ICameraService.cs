﻿using Cinemachine;
using UnityEngine;

namespace Services
{
    public interface ICameraService
    {
        Camera Camera { get; }
        ICinemachineCamera CinemachineCamera { get; }
        void SetLookAt(Transform transform);
        void SetFollowTo(Transform transform);
        void SetPosition(Vector3 position, bool isForce);
        void SetRotation(Quaternion rotation, bool isForce);
    }
}