﻿using System.Runtime.CompilerServices;
using Common.LateSystems;
using Common.UpdateSystems;
using Scellecs.Morpeh;

namespace Scripts
{
    public static class SystemsGroupExtensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DeleteHere<T>(this SystemsGroup group) where T : struct, IComponent
        {
            group.AddSystem(new DeleteHere<T>());
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DeleteHereLate<T>(this SystemsGroup group) where T : struct, IComponent
        {
            group.AddSystem(new DeleteHereLate<T>());
        }
    }
}