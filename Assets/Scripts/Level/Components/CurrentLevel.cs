﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Level.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct CurrentLevel : IComponent
    {
        public int levelId;
        public int vehicleId;
        public int trafficId;
    }
}