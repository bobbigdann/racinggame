﻿using Common.Components;
using DI.Attributes;
using Level.Components;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Level.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class DestroyCurrentLevelSystem : ILateSystem
    {
        [Inject] private Stash<Destroy> _destroyCache;

        public World World { get; set; }

        private Filter _destroyFilter;
        private Filter _levelFilter;

        public void OnAwake()
        {
            _destroyFilter = World.Filter.With<DestroyCurrentLevel>();
            _levelFilter = World.Filter.With<CurrentLevel>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_destroyFilter.IsEmpty()) return;

            foreach (var levelEntity in _levelFilter)
            {
                _destroyCache.Add(levelEntity);
            }
        }

        public void Dispose()
        {
            _destroyFilter = null;
            _levelFilter = null;
        }
    }
}