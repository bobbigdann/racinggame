﻿using Scripts.HUD.Components;
using DI.Attributes;
using Level.Components;
using Scellecs.Morpeh;
using RoadSpawn.Components;
using Scores.Components;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Level.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class InitializeLevelSystem : ISystem
    {
        [Inject] private LevelsDataResources _levelsData;

        [Inject] private Stash<CurrentLevel> _currentLevel;
        [Inject] private Stash<RoadSpawnRotate180> _roadSpawnRotate180;
        [Inject] private Stash<ResetScores> _resetScores;
        [Inject] private Stash<InitializeHud> _initializeHud;
        [Inject] private Stash<CreateCountDown> _createCountDown;
        
        public World World { get; set; }

        private Filter _initializeFilter;
        private Filter _currentLevelFilter;

        public void OnAwake()
        {
            _initializeFilter = World.Filter.With<InitializeLevel>();
            _currentLevelFilter = World.Filter.With<CurrentLevel>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_currentLevelFilter.IsEmpty() || _initializeFilter.IsEmpty()) return;

            int levelId = 0;
            foreach (var entity in _currentLevelFilter)
            {
                ref var currentLevel = ref _currentLevel.Get(entity);
                levelId = currentLevel.levelId;
            }
            
            foreach (var entity in _initializeFilter)
            {
                var levelData = _levelsData.GetDataByLevelId(levelId);
                var roadSpawner = Object.Instantiate(levelData.RoadSpawner);

                float roadWigth = roadSpawner.GetSerializedData().elementWidth;
                var roadsideSpawnerLeft = Object.Instantiate(levelData.RoadsideSpawner);
                float leftSpawnerOffset = roadWigth + roadsideSpawnerLeft.GetSerializedData().elementWidth;
                leftSpawnerOffset /= 2f;
                roadsideSpawnerLeft.transform.position = Vector3.left * leftSpawnerOffset;

                var roadsideSpawnerRight = Object.Instantiate(levelData.RoadsideSpawner);
                float rightSpawnerOffset = roadWigth + roadsideSpawnerRight.GetSerializedData().elementWidth;
                rightSpawnerOffset /= 2f;
                roadsideSpawnerRight.transform.position = Vector3.right * rightSpawnerOffset;
                _roadSpawnRotate180.Add(roadsideSpawnerRight.Entity);

                _resetScores.Add(World.CreateEntity());
                _initializeHud.Add(World.CreateEntity());
                _createCountDown.Add(World.CreateEntity());
            }
        }

        public void Dispose()
        {
            _initializeFilter = null;
        }
    }
}