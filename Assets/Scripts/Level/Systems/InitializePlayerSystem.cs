﻿using System;
using DI.Attributes;
using Level.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using Scripts.Settings.Mono;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Player;
using Vehicles.MonoComponents;
using Object = UnityEngine.Object;

namespace Level.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class InitializePlayerSystem : ISystem
    {
        [Inject] private PlayerVehiclesResources _playerVehicles;
        [Inject] private VehiclePhysicsParameters _physicsParameters;
        [Inject] private HudSettings _hudSettings;

        [Inject] private Stash<CurrentLevel> _currentLevel;
        [Inject] private Stash<VehicleControlledByLocalPlayer> _vehicleControlledByPlayer;
        [Inject] private Stash<VehicleStartVelocity> _vehicleStartVelocity;
        [Inject] private Stash<VehiclePlayerCollisionReceiverLink> _vehiclePlayerCollisionReceiverLink;
        [Inject] private Stash<VehicleLocalInputsFromHud> _vehicleLocalInputsFromHud;
        
        public World World { get; set; }

        private Filter _initializeFilter;
        private Filter _currentLevelFilter;

        public void OnAwake()
        {
            _initializeFilter = World.Filter.With<InitializeLevel>();
            _currentLevelFilter = World.Filter.With<CurrentLevel>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_currentLevelFilter.IsEmpty() || _initializeFilter.IsEmpty()) return;

            int vehicleId = 0;
            foreach (var entity in _currentLevelFilter)
            {
                ref var currentLevel = ref _currentLevel.Get(entity);
                vehicleId = currentLevel.vehicleId;
            }
            
            foreach (var entity in _initializeFilter)
            {
                if (vehicleId < 0 || vehicleId >= _playerVehicles.vehicles.Count)
                    throw new ArgumentException("Wrong player vehicle ID at level initalizing");

                var playerGo = Object.Instantiate(_playerVehicles.vehicles[vehicleId].gameVehicle);
                var collisionReceiver = playerGo.AddComponent<VehicleCollisionReceiver>();
                var provider = playerGo.GetComponent<EntityProvider>();
                var playerEntity = provider.Entity;
                
                _vehicleControlledByPlayer.Add(playerEntity);
                _vehicleStartVelocity.Set(playerEntity, new VehicleStartVelocity
                {
                    value = _physicsParameters.playerStartVelocity,
                });
                _vehiclePlayerCollisionReceiverLink.Set(playerEntity, new VehiclePlayerCollisionReceiverLink
                {
                    value = collisionReceiver,
                });
                
                if (_hudSettings.hudControlsEnabled)
                    _vehicleLocalInputsFromHud.Add(playerEntity);
            }
        }

        public void Dispose()
        {
            _initializeFilter = null;
        }
    }
}