﻿using Common.Components;
using DI.Attributes;
using Level.Components;
using Scellecs.Morpeh;
using Services;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Level.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class CleanLevelSystem : ISystem
    {
        [Inject] private readonly ICameraService _cameraService;

        [Inject] private Stash<Destroy> _destroy;
        [Inject] private Stash<CleanLevel> _cleanLevel;
        
        public World World { get; set; }

        private Filter _cleanFilter;
        private Filter _levelObjectsFilter;
        
        public void OnAwake()
        {
            _cleanFilter = World.Filter.With<CleanLevel>();
            _levelObjectsFilter = World.Filter.With<RuntimeLevelObject>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var cleanEntity in _cleanFilter)
            {
                _cleanLevel.Remove(cleanEntity);
                _cameraService.SetPosition(Vector3.zero, true);
            
                foreach (var entity in _levelObjectsFilter)
                {
                    _destroy.Set(entity);
                }
            }
        }

        public void Dispose()
        {
            _cleanFilter = null;
            _levelObjectsFilter = null;
        }
    }
}