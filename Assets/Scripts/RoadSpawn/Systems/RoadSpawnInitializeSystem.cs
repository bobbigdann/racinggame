﻿using System.Collections.Generic;
using DI.Attributes;
using Scellecs.Morpeh;
using RoadSpawn.Components;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace RoadSpawn.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class RoadSpawnInitializeSystem : ISystem
    {
        [Inject] private Stash<RoadSpawner> _roadSpawner;
        [Inject] private Stash<RoadSpawnerInitialized> _roadSpawnerInitialized;

        public World World { get; set; }

        private Filter _spawnerFilter;

        public void OnAwake()
        {
            _spawnerFilter = World.Filter
                .With<RoadSpawner>()
                .Without<RoadSpawnerInitialized>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _spawnerFilter)
            {
                ref var spawner = ref _roadSpawner.Get(entity);
                spawner.spawnedRoadElements = new LinkedList<GameObject>();
                
                _roadSpawnerInitialized.Add(entity);
            }
        }

        public void Dispose()
        {
            _spawnerFilter = null;
        }
    }
}