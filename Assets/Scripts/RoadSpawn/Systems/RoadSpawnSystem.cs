﻿using Scripts.Settings.Settings;
using DI.Attributes;
using Scellecs.Morpeh;
using RoadSpawn.Components;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Player;

namespace RoadSpawn.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class RoadSpawnSystem : ISystem
    {
        [Inject] private LevelSettings _levelSettings;
        
        [Inject] private Stash<RoadSpawner> _roadSpawner;
        [Inject] private Stash<RoadSpawnRotate180> _roadSpawnRotate180;
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;

        public World World { get; set; }

        private Filter _spawnerFilter;
        private Filter _playerCarFilter;
        
        public void OnAwake()
        {
            _spawnerFilter = World.Filter
                .With<RoadSpawner>()
                .With<RoadSpawnerInitialized>();
            
            _playerCarFilter = World.Filter
                .With<VehicleControlledByLocalPlayer>()
                .With<VehicleUnityView>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_playerCarFilter.IsEmpty() || _spawnerFilter.IsEmpty()) return;

            float playerZ = 0f;
            foreach (var carEntity in _playerCarFilter)
            {
                ref var unityView = ref _vehicleUnityView.Get(carEntity);
                playerZ = unityView.transform.position.z;
            }
            
            foreach (var entity in _spawnerFilter)
            {
                ref var spawner = ref _roadSpawner.Get(entity);
                float rotationValue = _roadSpawnRotate180.Has(entity) ? 180f : 0f;
                        
                if (spawner.spawnedRoadElements.Count == 0)
                {
                    SpawnRoadElement(ref spawner, -_levelSettings.roadDestroyDistance, rotationValue);
                    continue;
                }
                        
                var first = spawner.spawnedRoadElements.First.Value;
                float firstBorder = first.transform.position.z + spawner.elementLength / 2;
                if (firstBorder < playerZ - _levelSettings.roadDestroyDistance)
                {
                    spawner.spawnedRoadElements.RemoveFirst();
                    Object.Destroy(first.gameObject);
                }

                if (spawner.spawnedRoadElements.Count == 0)
                {
                    SpawnRoadElement(ref spawner, -_levelSettings.roadDestroyDistance, rotationValue);
                    continue;
                }
                        
                var last = spawner.spawnedRoadElements.Last.Value;
                float lastBorder = last.transform.position.z + spawner.elementLength / 2;
                if (lastBorder > playerZ + _levelSettings.roadSpawnDistance) continue;

                SpawnRoadElement(ref spawner, lastBorder, rotationValue);
            }
        }

        private static void SpawnRoadElement(ref RoadSpawner spawner, float lastBorder, float rotation)
        {
            var element = spawner.elements[Random.Range(0, spawner.elements.Count)];
            var spawnedElement = Object.Instantiate(element, spawner.elementsParent);
            spawnedElement.gameObject.SetActive(true);
            spawnedElement.transform.localPosition = Vector3.forward * (lastBorder + spawner.elementLength / 2);
            spawnedElement.transform.localRotation = Quaternion.Euler(0f, rotation, 0f);
            spawner.spawnedRoadElements.AddLast(spawnedElement);
        }

        public void Dispose()
        {
            _spawnerFilter = null;
            _playerCarFilter = null;
        }
     }
}