﻿using Scellecs.Morpeh;
using System;
using System.Collections.Generic;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace RoadSpawn.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct RoadSpawner : IComponent
    {
        public Transform elementsParent;
        public List<GameObject> elements;
        public float elementLength;
        public float elementWidth;
        
        public LinkedList<GameObject> spawnedRoadElements;
    }
}