﻿using UnityEngine;

namespace GUI.Containers
{
    public class UiCanvasContainer : MonoBehaviour
    {
        [field: SerializeField] public Transform Screens { private set; get; }
        [field: SerializeField] public Transform Widgets { private set; get; }
        [field: SerializeField] public Transform Notifications { private set; get; }
    }
}