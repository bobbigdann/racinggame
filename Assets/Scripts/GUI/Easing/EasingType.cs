﻿namespace Scripts.GUI
{
    public enum EasingType
    {
        Linear,
        InBounce,
        InBack,
        InCirc,
        InCubic,
        InElastic,
        InSine,
        InOutBack,
        InOutBounce,
        InOutCirc,
        InOutCubic,
        InOutElastic,
        InOutSine,
    }
}