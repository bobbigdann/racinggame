﻿using UnityEngine.UIElements;
using UnityEngine.UIElements.Experimental;

namespace Scripts.GUI
{
    public static class EasingsUtils
    {
        public static float Evaluate(float t, EasingType mode)
        {
            return mode switch
            {
                EasingType.Linear => Easing.Linear(t),
                EasingType.InBounce => Easing.InBounce(t),
                EasingType.InBack => Easing.InBack(t),
                EasingType.InCirc => Easing.InCirc(t),
                EasingType.InCubic => Easing.InCubic(t),
                EasingType.InElastic => Easing.InElastic(t),
                EasingType.InSine => Easing.InSine(t),
                EasingType.InOutBack => Easing.InOutBack(t),
                EasingType.InOutBounce => Easing.InOutBounce(t),
                EasingType.InOutCirc => Easing.InOutCirc(t),
                EasingType.InOutCubic => Easing.InOutCubic(t),
                EasingType.InOutElastic => Easing.InOutElastic(t),
                EasingType.InOutSine => Easing.InOutSine(t),
                _ => Easing.Linear(t)
            };
        }
        
        public static float EvaluateInvert(float t, EasingType mode)
        {
            return 1 - Evaluate(t, mode);
        }
    }
}