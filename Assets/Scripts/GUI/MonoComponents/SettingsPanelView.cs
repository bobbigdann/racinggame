﻿using Scripts.HUD.MonoComponents;
using UnityEngine;
using UnityEngine.UI;

namespace MonoComponents
{
    public class SettingsPanelView : MonoBehaviour
    {
        [field: SerializeField] public Button BackButton { private set; get; }
        [field: SerializeField] public Slider MusicSlider { private set; get; }
        [field: SerializeField] public Slider EffectsSlider { private set; get; }
        [field: SerializeField] public UiToggleButton HudControlsToggle { private set; get; }
    }
}