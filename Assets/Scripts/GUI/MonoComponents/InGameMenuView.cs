﻿using UnityEngine;
using UnityEngine.UI;

namespace MonoComponents
{
    public class InGameMenuView : MonoBehaviour
    {
        public Button menuButton;
        public Button settingsButton;
        public Button closeButton;
    }
}