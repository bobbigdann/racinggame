﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GUI.MonoComponents.SelectLevelPanel
{
    public class SelectLevelPanelView : MonoBehaviour
    {
        public Color defaultLevelColor;
        public Color selectedLevelColor;
        public Transform levelItemsParent;
        public Button backButton;
        public Button startButton;
        public LevelItem levelItemPrefab;
        
        [HideInInspector] public LevelItem currentSelectedLevel;
        [HideInInspector] public List<LevelItem> levelItems;
    }
}