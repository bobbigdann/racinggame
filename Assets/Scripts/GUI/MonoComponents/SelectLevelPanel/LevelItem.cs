﻿using UnityEngine;
using UnityEngine.UI;

namespace GUI.MonoComponents.SelectLevelPanel
{
    public class LevelItem : MonoBehaviour
    {
        public Image selectionImage;
        public Image levelImage;
        public Button button;

        [HideInInspector] public int levelId;
    }
}