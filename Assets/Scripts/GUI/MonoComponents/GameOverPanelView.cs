﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MonoComponents
{
    public class GameOverPanelView : MonoBehaviour
    {
        public TextMeshProUGUI scoreText;
        public TextMeshProUGUI moneyText;
        public Button restartButton;
        public Button goToMenuButton;
    }
}