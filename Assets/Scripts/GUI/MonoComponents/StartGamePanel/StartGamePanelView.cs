﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GUI.MonoComponents.StartGamePanel
{
    public class StartGamePanelView : MonoBehaviour
    {
        public Button startGameButton;
        public Button settingsButton;
        public Button nextCarButton;
        public Button prevCarButton;

        [Space] 
        public Button buyButton;
        public TextMeshProUGUI priceText;

        [Header("Parameters")]
        public Image accelerationFull;
        public TextMeshProUGUI accelerationText;
        [Space]
        public Image maxSpeedFull;
        public TextMeshProUGUI maxSpeedText;
        [Space]
        public Image controllabilityFull;
        public TextMeshProUGUI controllabilityText;
    }
}