﻿using DI.Builders;
using GUI.UpdateSystems.GameOverPanel;
using GUI.UpdateSystems.SettingsPanel;
using GUI.UpdateSystems.StartGamePanel;
using Scellecs.Morpeh;
using Scripts.GUI.UpdateSystems.GameOverPanel;
using Scripts.GUI.UpdateSystems.InGameMenuPanel;
using Scripts.GUI.UpdateSystems.SelectLevelPanel;

namespace Scripts.GUI
{
    public class GUISystemsFeature : ISystemsFeature
    {
        public SystemsGroup GetGroup(World world, IContainerBuilder container)
        {
            var group = world.CreateSystemsGroup();
            
            group.AddSystem(container.Create<StartGamePanelCreateSystem>());
            group.AddSystem(container.Create<StartGamePanelInitializeSystem>());
            group.AddSystem(container.Create<UpdateSelectedVehicleSystem>());
            group.AddSystem(container.Create<StartGamePanelCleanupSystem>());

            group.AddSystem(container.Create<SettingsPanelCreateSystem>());
            group.AddSystem(container.Create<SettingsPanelInitializeSystem>());

            group.AddSystem(container.Create<GameOverPanelCreateSystem>());
            group.AddSystem(container.Create<GameOverPanelInitializeSystem>());

            group.AddSystem(container.Create<SelectLevelPanelCreateSystem>());
            group.AddSystem(container.Create<SelectLevelPanelInitializeSystem>());
            group.AddSystem(container.Create<SelectLevelPanelPlaySystem>());

            group.AddSystem(container.Create<InGameMenuCreateSystem>());
            group.AddSystem(container.Create<InGameMenuInitializeSystem>());
            
            return group;
        }
    }
}