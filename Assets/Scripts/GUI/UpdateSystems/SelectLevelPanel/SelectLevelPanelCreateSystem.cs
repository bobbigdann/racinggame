﻿using DI.Attributes;
using GUI.Components;
using GUI.Components.SelectLevelPanel;
using GUI.Components.StartPanel;
using GUI.Containers;
using Scellecs.Morpeh;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Scripts.GUI.UpdateSystems.SelectLevelPanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class SelectLevelPanelCreateSystem : ISystem
    {
        [Inject] private readonly ScreenResources _screenResources;
        [Inject] private readonly UiCanvasContainer _uiCanvasContainer;

        [Inject] private Stash<UiMonoLink> _uiMonoLink;
        [Inject] private Stash<UiInitializePanel> _uiInitializePanel;
        [Inject] private Stash<UiCreateSelectLevelPanel> _uiCreateSelectLevelPanel;
        [Inject] private Stash<UiSelectLevelPanel> _uiSelectLevelPanel;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<UiCreateSelectLevelPanel>()
                .With<CurrentSelectedVehicle>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                _uiCreateSelectLevelPanel.Remove(entity);
                
                _uiMonoLink.Set(entity, new UiMonoLink
                {
                    Value = Object.Instantiate(_screenResources.SelectLevelPanel, _uiCanvasContainer.Screens)
                });
                _uiSelectLevelPanel.Add(entity);
                _uiInitializePanel.Add(entity);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}