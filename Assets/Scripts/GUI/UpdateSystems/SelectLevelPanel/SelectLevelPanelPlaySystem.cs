﻿using Common.Components;
using Constants;
using DI.Attributes;
using GUI.Components;
using GUI.Components.SelectLevelPanel;
using GUI.Components.StartPanel;
using GUI.MonoComponents.SelectLevelPanel;
using Level.Components;
using Scellecs.Morpeh;
using Services;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace GUI.UpdateSystems.StartGamePanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class SelectLevelPanelPlaySystem : ISystem
    {
        [Inject] private ICameraService _cameraService;
        
        [Inject] private Stash<CurrentLevel> _currentLevel;
        [Inject] private Stash<InitializeLevel> _initializeLevel;
        [Inject] private Stash<Destroy> _destroy;
        [Inject] private Stash<CurrentSelectedVehicle> _currentSelectedVehicle;
        [Inject] private Stash<SelectLevelPanelPlay> _selectLevelPanelPlay;
        [Inject] private Stash<UiMonoLink> _uiMonoLink;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<CurrentSelectedVehicle>()
                .With<SelectLevelPanelPlay>()
                .With<UiMonoLink>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var monoLink = ref _uiMonoLink.Get(entity);
                var component = monoLink.Value.GetComponent<SelectLevelPanelView>();
                
                ref var selectedVehicle = ref _currentSelectedVehicle.Get(entity);
                var levelEntity = World.CreateEntity();
                
                PlayerPrefs.SetInt(PlayerPrefsKeys.VehicleId, selectedVehicle.value);
                
                _cameraService.CinemachineCamera.VirtualCameraGameObject.SetActive(true);
                _currentLevel.Set(levelEntity, new CurrentLevel
                {
                    levelId = component.currentSelectedLevel != null ? component.currentSelectedLevel.levelId : 0,
                    vehicleId = selectedVehicle.value,
                    trafficId = 0,
                });
                _initializeLevel.Add(levelEntity);
                _selectLevelPanelPlay.Remove(entity);
                _destroy.Add(entity);
            }
        }

        public void Dispose()
        {
        }
    }
}