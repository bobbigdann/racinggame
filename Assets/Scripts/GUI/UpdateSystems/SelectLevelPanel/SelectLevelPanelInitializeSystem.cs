﻿using Common.Components;
using DI.Attributes;
using GUI.Components;
using GUI.Components.SelectLevelPanel;
using GUI.Components.StartPanel;
using GUI.MonoComponents.SelectLevelPanel;
using Scellecs.Morpeh;
using Settings.ScriptableObjects.References;
using SoundPlaying.Data;
using SoundPlaying.UpdateSystems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Scripts.GUI.UpdateSystems.SelectLevelPanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class SelectLevelPanelInitializeSystem : ISystem
    {
        [Inject] private SoundFactorySystem _soundFactory;
        [Inject] private LevelsDataResources _levelResources;
        
        [Inject] private Stash<UiMonoLink> _uiMonoLink;
        [Inject] private Stash<Destroy> _destroy;
        [Inject] private Stash<SelectLevelPanelPlay> _selectLevelPanelPlay;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<UiInitializePanel>()
                .With<UiSelectLevelPanel>()
                .With<UiMonoLink>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var monoLink = ref _uiMonoLink.Get(entity);
                var view = monoLink.Value.GetComponent<SelectLevelPanelView>();

                bool selectedImage = false;
                for (int i = 0; i < _levelResources.data.Count; i++)
                {
                    var levelData = _levelResources.data[i];
                    var item = Object.Instantiate(view.levelItemPrefab, view.levelItemsParent);
                    item.gameObject.SetActive(true);
                    item.levelId = i;
                    item.levelImage.sprite = levelData.LevelImage;
                    item.selectionImage.color = view.defaultLevelColor;

                    item.button.onClick.AddListener(() =>
                    {
                        if (view.currentSelectedLevel != null)
                            view.currentSelectedLevel.selectionImage.color = view.defaultLevelColor;

                        view.currentSelectedLevel = item;
                        item.selectionImage.color = view.selectedLevelColor;
                    });

                    if (selectedImage) continue;

                    item.selectionImage.color = view.selectedLevelColor;
                    selectedImage = true;
                    view.currentSelectedLevel = item;
                }

                view.backButton.onClick.AddListener(() =>
                {
                    _soundFactory.Create(SoundType.ClickButton).Play();
                    World.CreateEntity().AddComponent<UiCreateStartGamePanel>();
                    
                    _destroy.Add(entity);
                });
                
                view.startButton.onClick.AddListener(() =>
                {
                    _soundFactory.Create(SoundType.ClickButton).Play();
                    _selectLevelPanelPlay.Add(entity);
                });
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}