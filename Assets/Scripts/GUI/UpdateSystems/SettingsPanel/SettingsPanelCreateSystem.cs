﻿using DI.Attributes;
using GUI.Components;
using GUI.Components.SettingsPanel;
using GUI.Containers;
using Scellecs.Morpeh;
using Settings.ScriptableObjects.References;
using UnityEngine;

namespace GUI.UpdateSystems.SettingsPanel
{
    public class SettingsPanelCreateSystem : ISystem
    {
        [Inject] private readonly ScreenResources _screenResources;
        [Inject] private readonly UiCanvasContainer _uiCanvasContainer;

        [Inject] private Stash<UiSettingsPanel> _settingsPanelCache;
        [Inject] private Stash<UiInitializePanel> _initializeCache;
        [Inject] private Stash<UiMonoLink> _uiMonoCache;
        [Inject] private Stash<UiCreateSettingsPanel> _uiCreateSettingsPanel;
        
        public World World { get; set; }

        private Filter _createPanelFilter;
        
        public void OnAwake()
        {
            _createPanelFilter = World.Filter.With<UiCreateSettingsPanel>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _createPanelFilter)
            {
                _uiMonoCache.Add(entity, new UiMonoLink()
                {
                    Value = Object.Instantiate(_screenResources.SettingsPanel, _uiCanvasContainer.Screens)
                });

                _settingsPanelCache.Add(entity);
                _initializeCache.Add(entity);
                _uiCreateSettingsPanel.Remove(entity);
            }
        }
        
        public void Dispose()
        {
            _createPanelFilter = null;
        }
    }
}