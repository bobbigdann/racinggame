﻿using Common.Components;
using Scripts.Settings.Settings;
using DI.Attributes;
using GUI.Components;
using GUI.Components.StartPanel;
using MonoComponents;
using Scellecs.Morpeh;
using Scripts.Settings.Mono;
using Settings.Components;
using SoundPlaying.Data;
using SoundPlaying.UpdateSystems;

namespace GUI.UpdateSystems.SettingsPanel
{
    public class SettingsPanelInitializeSystem : ISystem
    {
        [Inject] private SoundFactorySystem _soundFactorySystem;
        [Inject] private SoundSettings _soundSettings;
        [Inject] private HudSettings _hudSettings;

        [Inject] private Stash<Destroy> _destroy;
        [Inject] private Stash<GlobalSettingsChanged> _globalSettingsChanged;
        [Inject] private Stash<WriteGlobalSettings> _writeGlobalSettings;
        [Inject] private Stash<UiMonoLink> _uiMonoLink;
        
        public World World { get; set; }

        private Filter _settingsPanelFilter;

        public void OnAwake()
        {
            _settingsPanelFilter = World.Filter
                .With<UiSettingsPanel>()
                .With<UiMonoLink>()
                .With<UiInitializePanel>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _settingsPanelFilter)
            {
                var component = _uiMonoLink.Get(entity).Value.GetComponent<SettingsPanelView>();
                
                component.MusicSlider.SetValueWithoutNotify(_soundSettings.MusicVolume);
                component.EffectsSlider.SetValueWithoutNotify(_soundSettings.EffectsVolume);
                component.HudControlsToggle.SetState(_hudSettings.hudControlsEnabled);

                component.BackButton.onClick.AddListener(() =>
                {
                    _soundFactorySystem.Create(SoundType.ClickButton).Play();
                    
                    _destroy.Add(entity);
                    _writeGlobalSettings.Add(entity);
                });

                component.EffectsSlider.onValueChanged.AddListener(value =>
                {
                    _soundSettings.EffectsVolume = value;
                    _globalSettingsChanged.Add(World.CreateEntity());
                });

                component.MusicSlider.onValueChanged.AddListener(value =>
                {
                    _soundSettings.MusicVolume = value;
                    _globalSettingsChanged.Add(World.CreateEntity());
                });

                component.HudControlsToggle.OnClick = state =>
                {
                    _soundFactorySystem.Create(SoundType.ClickButton).Play();
                    _hudSettings.hudControlsEnabled = state;
                    _globalSettingsChanged.Add(World.CreateEntity());
                };
            }
        }
        
        public void Dispose()
        {
            _settingsPanelFilter = null;
        }
    }
}