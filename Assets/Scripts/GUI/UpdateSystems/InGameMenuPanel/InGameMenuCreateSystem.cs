﻿using DI.Attributes;
using GUI.Components;
using GUI.Components.InGameMenuPanel;
using GUI.Containers;
using Scellecs.Morpeh;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Scripts.GUI.UpdateSystems.InGameMenuPanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class InGameMenuCreateSystem : ISystem
    {
        [Inject] private ScreenResources _screenResources;
        [Inject] private UiCanvasContainer _uiCanvasContainer;
        
        [Inject] private Stash<UiCreateInGameMenu> _uiCreateInGameMenu;
        [Inject] private Stash<UiMonoLink> _uiMonoLink;
        [Inject] private Stash<UiInitializePanel> _uiInitializePanel;
        [Inject] private Stash<UiInGameMenu> _uiInGameMenu;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<UiCreateInGameMenu>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                _uiCreateInGameMenu.Remove(entity);
                
                _uiMonoLink.Set(entity, new UiMonoLink
                {
                    Value = Object.Instantiate(_screenResources.InGameMenuPanel, _uiCanvasContainer.Screens)
                });
                _uiInGameMenu.Add(entity);
                _uiInitializePanel.Add(entity);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}