﻿using Common.Components;
using DI.Attributes;
using Gameplay.Components;
using GUI.Components;
using GUI.Components.InGameMenuPanel;
using GUI.Components.SettingsPanel;
using GUI.Components.StartPanel;
using Level.Components;
using MonoComponents;
using Scellecs.Morpeh;
using SoundPlaying.Data;
using SoundPlaying.UpdateSystems;
using Unity.IL2CPP.CompilerServices;

namespace Scripts.GUI.UpdateSystems.InGameMenuPanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class InGameMenuInitializeSystem : ISystem
    {
        [Inject] private SoundFactorySystem _soundFactory;
        
        [Inject] private Stash<UiInitializePanel> _uiInitializePanel;
        [Inject] private Stash<UiMonoLink> _uiMonoLink;
        [Inject] private Stash<Destroy> _destroy;
        [Inject] private Stash<DestroyCurrentLevel> _destroyCurrentLevel;
        [Inject] private Stash<UiCreateStartGamePanel> _uiCreateStartGamePanel;
        [Inject] private Stash<UiCreateSettingsPanel> _uiCreateSettingsPanel;
        [Inject] private Stash<ResumeGame> _resumeGame;
        [Inject] private Stash<CleanLevel> _cleanLevel;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<UiInGameMenu>()
                .With<UiMonoLink>()
                .With<UiInitializePanel>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                _uiInitializePanel.Remove(entity);

                ref var monoLink = ref _uiMonoLink.Get(entity);
                var view = monoLink.Value.GetComponent<InGameMenuView>();
                
                view.closeButton.onClick.AddListener(() =>
                {
                    _soundFactory.Create(SoundType.ClickButton).Play();
                    _destroy.Add(entity);
                    _resumeGame.Add(World.CreateEntity());
                });
                
                view.menuButton.onClick.AddListener(() =>
                {
                    _soundFactory.Create(SoundType.ClickButton).Play();
                    _destroy.Add(entity);
                    _cleanLevel.Add(World.CreateEntity());
                    _destroyCurrentLevel.Add(World.CreateEntity());
                    _uiCreateStartGamePanel.Add(World.CreateEntity());
                    _resumeGame.Add(World.CreateEntity());
                });
                
                view.settingsButton.onClick.AddListener(() =>
                {
                    _soundFactory.Create(SoundType.ClickButton).Play();
                    _uiCreateSettingsPanel.Add(World.CreateEntity());
                });
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}