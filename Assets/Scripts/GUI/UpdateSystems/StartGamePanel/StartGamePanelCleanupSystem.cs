﻿using Common.Components;
using DI.Attributes;
using GUI.Components.StartPanel;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace GUI.UpdateSystems.StartGamePanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class StartGamePanelCleanupSystem : ISystem
    {
        [Inject] private Stash<UiStartGamePanelMonoLink> _uiStartGamePanelMonoLink;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<UiStartGamePanelMonoLink>()
                .With<Destroy>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var monoLink = ref _uiStartGamePanelMonoLink.Get(entity);
                Object.Destroy(monoLink.view.gameObject);
                Object.Destroy(monoLink.carContainer.gameObject);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}