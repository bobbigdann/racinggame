﻿using System.Globalization;
using Constants;
using DI.Attributes;
using GUI.Components.StartPanel;
using Scellecs.Morpeh;
using Player;
using Services;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace GUI.UpdateSystems.StartGamePanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class UpdateSelectedVehicleSystem : ISystem
    {
        [Inject] private PlayerVehiclesResources _playerVehiclesResources;
        [Inject] private ICameraService _cameraService;
        [Inject] private IPlayerData _playerData;
        
        [Inject] private Stash<UiStartGamePanelMonoLink> _uiStartGamePanelMonoLink;
        [Inject] private Stash<UpdateSelectedVehicle> _updateSelectedVehicle;
        [Inject] private Stash<CurrentSelectedVehicle> _currentSelectedVehicle;
        [Inject] private Stash<CurrentVehicleMonoLink> _currentVehicleMonoLink;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<UiStartGamePanelMonoLink>()
                .With<CurrentSelectedVehicle>()
                .With<UpdateSelectedVehicle>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var spawnedVehicle = ref _currentVehicleMonoLink.Get(entity, out bool vehicleExists);
                if (vehicleExists)
                    Object.Destroy(spawnedVehicle.value);

                int vehiclesCount = _playerVehiclesResources.vehicles.Count;
                ref var currentVehicle = ref _currentSelectedVehicle.Get(entity);
                if (currentVehicle.value < 0)
                    currentVehicle.value = vehiclesCount - 1;
                else if (currentVehicle.value >= vehiclesCount)
                    currentVehicle.value = 0;
                
                PlayerPrefs.SetInt(PlayerPrefsKeys.VehicleId, currentVehicle.value);
                
                ref var monoLink = ref _uiStartGamePanelMonoLink.Get(entity);
                var container = _playerVehiclesResources.vehicles[currentVehicle.value];
                var vehicleGO = Object.Instantiate(container.menuVehicle, monoLink.carContainer.carParent);
                
                _cameraService.CinemachineCamera.VirtualCameraGameObject.SetActive(false);
                _cameraService.SetPosition(monoLink.carContainer.cameraPosition.position, true);
                _cameraService.SetRotation(monoLink.carContainer.cameraPosition.rotation, true);

                monoLink.view.accelerationText.text = ((int)container.acceleration).ToString();
                monoLink.view.accelerationFull.fillAmount =
                    container.acceleration / VehicleHudConstants.MaxViewParameterValue;
                    
                monoLink.view.maxSpeedText.text = ((int)container.maxSpeed).ToString();
                monoLink.view.maxSpeedFull.fillAmount =
                    container.maxSpeed / VehicleHudConstants.MaxViewParameterValue;
                
                monoLink.view.controllabilityText.text = ((int)container.controllability).ToString();
                monoLink.view.controllabilityFull.fillAmount =
                    container.controllability / VehicleHudConstants.MaxViewParameterValue;

                if (!_playerData.Vehicles.Contains(currentVehicle.value))
                {
                    monoLink.view.buyButton.gameObject.SetActive(true);
                    monoLink.view.priceText.text = container.price.ToString();
                }
                else
                {
                    monoLink.view.buyButton.gameObject.SetActive(false);
                }

                _updateSelectedVehicle.Remove(entity);
                _currentVehicleMonoLink.Set(entity, new CurrentVehicleMonoLink
                {
                    value = vehicleGO
                });
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}