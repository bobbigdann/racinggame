﻿using DI.Attributes;
using GUI.Components;
using GUI.Components.StartPanel;
using GUI.Containers;
using Scellecs.Morpeh;
using Services;
using Settings.ScriptableObjects.References;
using UnityEngine;

namespace GUI.UpdateSystems.StartGamePanel
{
    public class StartGamePanelCreateSystem : ISystem
    {
        [Inject] private readonly ScreenResources _screenResources;
        [Inject] private readonly UiCanvasContainer _uiCanvasContainer;

        [Inject] private Stash<UiStartGamePanelMonoLink> _startGameCache;
        [Inject] private Stash<UiInitializePanel> _initializeCache;
        [Inject] private Stash<UiCreateStartGamePanel> _uiCreateStartGamePanel;
        
        public World World { get; set; }

        private Filter _createPanelFilter;
        
        public void OnAwake()
        {
            _createPanelFilter = World.Filter.With<UiCreateStartGamePanel>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _createPanelFilter)
            {
                var view = Object.Instantiate(_screenResources.StartGamePanel, _uiCanvasContainer.Screens);
                var carContainer = Object.Instantiate(_screenResources.CarContainer);
                
                _startGameCache.Set(entity, new UiStartGamePanelMonoLink
                {
                    view = view,
                    carContainer = carContainer
                });
                _initializeCache.Add(entity);
                _uiCreateStartGamePanel.Remove(entity);
            }
        }
        
        public void Dispose()
        {
            _createPanelFilter = null;
        }
    }
}