﻿using Common.Components;
using Constants;
using DI.Attributes;
using GUI.Components;
using GUI.Components.SelectLevelPanel;
using GUI.Components.SettingsPanel;
using GUI.Components.StartPanel;
using Scellecs.Morpeh;
using Player;
using Res.Extensions;
using Settings.ScriptableObjects.References;
using SoundPlaying.Data;
using SoundPlaying.UpdateSystems;
using UnityEngine;

namespace GUI.UpdateSystems.StartGamePanel
{
    public class StartGamePanelInitializeSystem : ISystem
    {
        [Inject] private PlayerVehiclesResources _playerVehiclesResources;
        [Inject] private readonly SoundFactorySystem _soundFactorySystem;
        [Inject] private readonly IPlayerData _playerData;

        [Inject] private Stash<Destroy> _destroyCache;
        [Inject] private Stash<UiStartGamePanelMonoLink> _uiStartGamePanelMonoLink;
        [Inject] private Stash<CurrentSelectedVehicle> _currentSelectedVehicle;
        [Inject] private Stash<UpdateSelectedVehicle> _updateSelectedVehicle;
        [Inject] private Stash<UiCreateSelectLevelPanel> _uiCreateSelectLevelPanel;

        public World World { get; set; }

        private Filter _startGameFilter;

        public void OnAwake()
        {
            _startGameFilter = World.Filter
                .With<UiStartGamePanelMonoLink>()
                .With<UiInitializePanel>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _startGameFilter)
            {
                ref var monoLink = ref _uiStartGamePanelMonoLink.Get(entity);
                var view = monoLink.view;
                view.startGameButton.onClick.AddListener(() =>
                {
                    _soundFactorySystem.Create(SoundType.ClickButton).Play();
                    
                    var selectedVehicle = _currentSelectedVehicle.Get(entity);
                    if (!_playerData.Vehicles.Contains(selectedVehicle.value)) return;

                    var selectLevelEntity = World.CreateEntity();
                    _uiCreateSelectLevelPanel.Add(selectLevelEntity);
                    _currentSelectedVehicle.Set(selectLevelEntity, new CurrentSelectedVehicle
                    {
                        value = selectedVehicle.value,
                    });
                    _destroyCache.Add(entity);
                });
                
                view.buyButton.onClick.AddListener(() =>
                {
                    _soundFactorySystem.Create(SoundType.ClickButton).Play();
                    
                    var selectedVehicle = _currentSelectedVehicle.Get(entity);
                    var container = _playerVehiclesResources.vehicles[selectedVehicle.value];
                    if (_playerData.GetMoney() < container.price) return;
                    
                    _playerData.Vehicles.Add(selectedVehicle.value);
                    _playerData.TakeMoney(container.price);
                    view.buyButton.gameObject.SetActive(false);
                });
                view.settingsButton.onClick.AddListener(() =>
                {
                    World.CreateEntity().AddComponent<UiCreateSettingsPanel>();
                    
                    _soundFactorySystem.Create(SoundType.ClickButton).Play();
                });
                view.nextCarButton.onClick.AddListener(() =>
                {
                    _soundFactorySystem.Create(SoundType.ClickButton).Play();
                    ref var selectedVehicle = ref _currentSelectedVehicle.Get(entity);
                    selectedVehicle.value++;
                    _updateSelectedVehicle.Add(entity);
                });
                view.prevCarButton.onClick.AddListener(() =>
                {
                    _soundFactorySystem.Create(SoundType.ClickButton).Play();
                    ref var selectedVehicle = ref _currentSelectedVehicle.Get(entity);
                    selectedVehicle.value--;
                    _updateSelectedVehicle.Add(entity);
                });

                int selectedVehicle = PlayerPrefs.HasKey(PlayerPrefsKeys.VehicleId)
                    ? PlayerPrefs.GetInt(PlayerPrefsKeys.VehicleId)
                    : 0;
                
                _currentSelectedVehicle.Set(entity, new CurrentSelectedVehicle
                {
                    value = selectedVehicle
                });
                _updateSelectedVehicle.Add(entity);
            }
        }
        
        public void Dispose()
        {
            _startGameFilter = null;
        }
    }
}