﻿using DI.Attributes;
using GUI.Components;
using GUI.Components.GameOverPanel;
using GUI.Containers;
using Scellecs.Morpeh;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Scripts.GUI.UpdateSystems.GameOverPanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class GameOverPanelCreateSystem : ISystem
    {
        [Inject] private readonly ScreenResources _screenResources;
        [Inject] private readonly UiCanvasContainer _uiCanvasContainer;

        [Inject] private Stash<UiMonoLink> _uiMonoLink;
        [Inject] private Stash<UiGameOverPanel> _uiGameOverPanel;
        [Inject] private Stash<UiInitializePanel> _uiInitializePanel;
        [Inject] private Stash<UiCreateGameOverPanel> _uiCreateGameOverPanel;
        
        public World World { get; set; }

        private Filter _createFilter;
        
        public void OnAwake()
        {
            _createFilter = World.Filter.With<UiCreateGameOverPanel>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _createFilter)
            {
                _uiMonoLink.Set(entity, new UiMonoLink
                {
                    Value = Object.Instantiate(_screenResources.GameOverPanel, _uiCanvasContainer.Screens)
                });

                _uiGameOverPanel.Add(entity);
                _uiInitializePanel.Add(entity);
                _uiCreateGameOverPanel.Remove(entity);
            }
        }

        public void Dispose()
        {
            _createFilter = null;
        }
    }
}