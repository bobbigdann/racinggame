﻿using Common.Components;
using DI.Attributes;
using GUI.Components;
using GUI.Components.GameOverPanel;
using GUI.Components.StartPanel;
using Level.Components;
using MonoComponents;
using Scellecs.Morpeh;
using Scores.Components;
using Settings.ScriptableObjects.References;
using SoundPlaying.Data;
using SoundPlaying.UpdateSystems;
using Unity.IL2CPP.CompilerServices;

namespace GUI.UpdateSystems.GameOverPanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class GameOverPanelInitializeSystem : ISystem
    {
        [Inject] private readonly SoundFactorySystem _soundFactorySystem;
        [Inject] private readonly SettingsContainer _settingsContainer;
        
        [Inject] private Stash<CurrentScore> _currentScore;
        [Inject] private Stash<UiMonoLink> _uiMonoLink;
        [Inject] private Stash<InitializeLevel> _initializeLevel;
        [Inject] private Stash<Destroy> _destroy;
        [Inject] private Stash<DestroyCurrentLevel> _destroyCurrentLevel;
        [Inject] private Stash<UiCreateStartGamePanel> _uiCreateStartGamePanel;
        
        public World World { get; set; }

        private Filter _filter;
        private Filter _scoreFilter;
        
        public void OnAwake()
        {
            _scoreFilter = World.Filter
                .With<CurrentScore>();
            
            _filter = World.Filter
                .With<UiGameOverPanel>()
                .With<UiInitializePanel>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_scoreFilter.IsEmpty() || _filter.IsEmpty()) return;

            int score = 0;
            foreach (var entity in _scoreFilter)
            {
                ref var currentScore = ref _currentScore.Get(entity);
                score = currentScore.value;
            }
            
            foreach (var entity in _filter)
            {
                ref var monoLink = ref _uiMonoLink.Get(entity);
                var component = monoLink.Value.GetComponent<GameOverPanelView>();

                component.scoreText.text = score.ToString();
                component.moneyText.text = ((int)(score * _settingsContainer.CoefficientScoreToMoney)).ToString();
                component.restartButton.onClick.AddListener(() =>
                {
                    _soundFactorySystem.Create(SoundType.ClickButton).Play();
                    
                    _initializeLevel.Add(World.CreateEntity());
                    _destroy.Add(entity);
                });
                component.goToMenuButton.onClick.AddListener(() =>
                {
                    _soundFactorySystem.Create(SoundType.ClickButton).Play();
                    
                    _destroyCurrentLevel.Add(World.CreateEntity());
                    _uiCreateStartGamePanel.Add(World.CreateEntity());
                    _destroy.Add(entity);
                });
            }
        }

        public void Dispose()
        {
            _scoreFilter = null;
            _filter = null;
        }
    }
}