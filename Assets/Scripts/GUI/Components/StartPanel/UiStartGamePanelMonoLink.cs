﻿using GUI.MonoComponents.StartGamePanel;
using MonoComponents;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace GUI.Components.StartPanel
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct UiStartGamePanelMonoLink : IComponent
    {
        public StartGamePanelView view;
        public CarContainer carContainer;
    }
}