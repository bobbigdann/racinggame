﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Common.UpdateSystems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class TimerUpdateSystem : ISystem
    {
        public World World { get; set; }

        private Filter _timersFilter;
        
        [Inject] private Stash<Timer> _timersCache;

        public void OnAwake()
        {
            _timersFilter = World.Filter.With<Timer>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _timersFilter)
            {
                ref var component = ref _timersCache.Get(entity);
                component.CurrentTime += deltaTime;

                if(component.FinalTime <= 0f) continue;
                if (component.CurrentTime < component.FinalTime) continue;
                
                component.Callback?.Invoke();
                if (component.Loop)
                {
                    component.CurrentTime = 0f;
                }
                else
                {
                    _timersCache.Remove(entity);
                }
            }
        }
        
        public void Dispose()
        {
            _timersFilter = null;
        }
    }
}