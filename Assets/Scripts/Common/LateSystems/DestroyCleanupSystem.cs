﻿using Common.Components;
using DI.Attributes;
using GUI.Components;
using Level.Components;
using Scellecs.Morpeh;
using UnityEngine;

namespace Common.LateSystems
{
    public class DestroyCleanupSystem : ILateSystem
    {
        public World World { get; set; }

        private Filter _destroyFilter;

        [Inject] private Stash<UiMonoLink> _uiMonoCache;
        [Inject] private Stash<RuntimeLevelObject> _runtimeLevelCache;
        
        public void OnAwake()
        {
            _destroyFilter = World.Filter.With<Destroy>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _destroyFilter)
            {
                var uiMonoLink = _uiMonoCache.Get(entity, out var uiMonoLinkExist);
                if (uiMonoLinkExist)
                {
                    Object.Destroy(uiMonoLink.Value);
                }

                var runtimeLevelObject = _runtimeLevelCache.Get(entity, out var runtimeLevelObjectExist);
                if (runtimeLevelObjectExist)
                {
                    Object.Destroy(runtimeLevelObject.value);
                }
            }
        }
        
        public void Dispose()
        {
            _destroyFilter = null;
        }
    }
}