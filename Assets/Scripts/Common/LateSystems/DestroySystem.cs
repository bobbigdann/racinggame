﻿using Common.Components;
using Scellecs.Morpeh;

namespace Common.LateSystems
{
    public class DestroySystem : ILateSystem
    {
        public World World { get; set; }

        private Filter _destroyFilter;
        
        public void OnAwake()
        {
            _destroyFilter = World.Filter.With<Destroy>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _destroyFilter)
            {
                entity.Dispose();
            }
        }
        
        public void Dispose()
        {
            _destroyFilter = null;
        }
    }
}