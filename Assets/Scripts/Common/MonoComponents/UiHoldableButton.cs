﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Scripts.HUD.MonoComponents
{
    public class UiHoldableButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public bool State => _state;

        private bool _state;

        public void OnPointerDown(PointerEventData eventData)
        {
            _state = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _state = false;
        }
    }
}