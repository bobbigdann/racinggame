﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Scripts.HUD.MonoComponents
{
    public class UiToggleButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private GameObject activeStateGO;
        [SerializeField] private GameObject inactiveStateGO;
        
        public Action<bool> OnClick;

        private bool _state;

        private void Awake()
        {
            SetState(false);
        }

        public void SetState(bool state)
        {
            _state = state;
            activeStateGO.SetActive(_state);
            inactiveStateGO.SetActive(!_state);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            SetState(!_state);
            OnClick?.Invoke(_state);
        }
    }
}