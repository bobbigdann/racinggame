﻿using System;
using Scellecs.Morpeh;
using Scripts.HUD.MonoComponents;
using Unity.IL2CPP.CompilerServices;

namespace Scripts.HUD.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct HudControls : IComponent
    {
        public UiHoldableButton gasButton;
        public UiHoldableButton brakeButton;
        public UiHoldableButton handbrakeButton;
        public UiHoldableButton turnRightButton;
        public UiHoldableButton turnLeftButton;
    }
}