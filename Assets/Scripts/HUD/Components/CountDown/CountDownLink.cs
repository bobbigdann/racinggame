﻿using Scellecs.Morpeh;
using Scripts.HUD.MonoComponents;
using Unity.IL2CPP.CompilerServices;

namespace Scripts.HUD.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct CountDownLink : IComponent
    {
        public HudCountDown value;
    }
}