﻿using Scellecs.Morpeh;
using System;
using TMPro;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Scripts.HUD.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct HudScore : IComponent
    {
        public TextMeshProUGUI scoreText;

        [HideInInspector] public int lastScore;
    }
}