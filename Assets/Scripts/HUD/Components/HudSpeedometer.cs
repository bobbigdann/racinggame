﻿using Scellecs.Morpeh;
using System;
using TMPro;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.HUD.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct HudSpeedometer : IComponent
    {
        public TextMeshProUGUI speedText;
        public RectTransform rpmArrow;
        public TextMeshProUGUI gearText;
    }
}