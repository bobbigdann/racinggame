﻿using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using Scripts.HUD.Components;

namespace Scripts.HUD.Providers
{
    public class HudBackButtonProvider : MonoProvider<HudBackButton>
    {
        private void Start()
        {
            Entity.AddComponent<InitializeHudBackButton>();
        }
    }
}