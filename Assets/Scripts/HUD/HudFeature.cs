﻿using DI.Builders;
using HUD.Systems;
using Scellecs.Morpeh;
using Scripts.HUD.Systems;
using Scripts.HUD.Systems.HudElemets;

namespace Scripts.HUD
{
    public class HudFeature : ISystemsFeature
    {
        public SystemsGroup GetGroup(World world, IContainerBuilder container)
        {
            var group = world.CreateSystemsGroup();
            
            group.AddSystem(container.Create<InitializeHudSystem>());
            group.AddSystem(container.Create<HudSpeedometerSystem>());
            group.AddSystem(container.Create<HudScoresSystem>());
            group.AddSystem(container.Create<DestroyHudSystem>());

            group.AddSystem(container.Create<HudCountDownCreateSystem>());
            group.AddSystem(container.Create<HudCountDownTimerSystem>());
            group.AddSystem(container.Create<HudCountDownSetSizeSystem>());
            group.AddSystem(container.Create<HudCountDownDestroySystem>());
            group.AddSystem(container.Create<HudControlsSystem>());

            group.AddSystem(container.Create<HudInitializeBackButtonSystem>());

            return group;
        }
    }
}