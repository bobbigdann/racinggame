﻿using DI.Attributes;
using Scellecs.Morpeh;
using Scripts.HUD.Components;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Player;

namespace HUD.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class HudControlsSystem : ISystem
    {
        [Inject] private Stash<HudControls> _hudControls;
        [Inject] private Stash<VehicleInputs> _vehicleInputs;
        
        public World World { get; set; }

        private Filter _hudFilter;
        private Filter _playerFilter;

        public void OnAwake()
        {
            _hudFilter = World.Filter
                .With<HudControls>();

            _playerFilter = World.Filter
                .With<VehicleControlledByLocalPlayer>()
                .With<VehicleInputs>()
                .With<VehicleLocalInputsFromHud>()
                .Without<VehicleAutoGasInput>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_playerFilter.IsEmpty()) return;
            
            foreach (var hudEntity in _hudFilter)
            {
                ref var controls = ref _hudControls.Get(hudEntity);
                
                foreach (var entity in _playerFilter)
                {
                    ref var inputs = ref _vehicleInputs.Get(entity);
                    inputs.gasPressed = controls.gasButton.State;
                    inputs.brakePressed = controls.brakeButton.State;
                    inputs.handbrakePressed = controls.handbrakeButton.State;
                    inputs.turnRightPressed = controls.turnRightButton.State;
                    inputs.turnLeftPressed = controls.turnLeftButton.State;
                }
            }
        }

        public void Dispose()
        {
            _hudFilter = null;
            _playerFilter = null;
        }
    }
}