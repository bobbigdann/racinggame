﻿using Scripts.HUD.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Scores.Components;
using Unity.IL2CPP.CompilerServices;

namespace HUD.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class HudScoresSystem : ISystem
    {
        [Inject] private Stash<CurrentScore> _currentScore;
        [Inject] private Stash<HudScore> _hudScore;
        
        public World World { get; set; }

        private Filter _scoresFilter;
        private Filter _hudFilter;

        public void OnAwake()
        {
            _scoresFilter = World.Filter.With<CurrentScore>();
            _hudFilter = World.Filter.With<HudScore>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_scoresFilter.IsEmpty() || _hudFilter.IsEmpty()) return;

            int score = 0;
            foreach (var entity in _scoresFilter)
            {
                ref var currentScore = ref _currentScore.Get(entity);
                score = currentScore.value;
            }

            foreach (var entity in _hudFilter)
            {
                ref var hud = ref _hudScore.Get(entity);
                if (hud.lastScore == score) continue;

                hud.lastScore = score;
                hud.scoreText.text = score.ToString();
            }
        }

        public void Dispose()
        {
        }
    }
}