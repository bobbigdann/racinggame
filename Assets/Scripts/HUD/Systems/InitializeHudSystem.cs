﻿using DI.Attributes;
using GUI.Components;
using GUI.Containers;
using Scellecs.Morpeh;
using Scripts.HUD.Components;
using Scripts.Settings.Mono;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Scripts.HUD.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class InitializeHudSystem : ISystem
    {
        [Inject] private ScreenResources _screenResources;
        [Inject] private UiCanvasContainer _uiCanvasContainer;
        [Inject] private HudSettings _hudSettings;
        
        [Inject] private Stash<UiMonoLink> _uiMonoLink;
        [Inject] private Stash<GameHud> _gameHud;
        [Inject] private Stash<InitializeHud> _initializeHud;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter.With<InitializeHud>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                _initializeHud.Remove(entity);
                var hudGO = Object.Instantiate(_screenResources.HudPanel, _uiCanvasContainer.Screens);
                
                _gameHud.Add(entity);
                _uiMonoLink.Set(entity, new UiMonoLink
                {
                    Value = hudGO
                });

                if (_hudSettings.hudControlsEnabled)
                    Object.Instantiate(_screenResources.HudControlsPanel, hudGO.transform);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}