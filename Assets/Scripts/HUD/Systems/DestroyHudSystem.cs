﻿using Common.Components;
using Scripts.HUD.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace HUD.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class DestroyHudSystem : ISystem
    {
        [Inject] private Stash<Destroy> _destroy;
        
        public World World { get; set; }

        private Filter _destroyFilter;
        private Filter _hudFilter;
        
        public void OnAwake()
        {
            _destroyFilter = World.Filter.With<DestroyHud>();
            _hudFilter = World.Filter.With<GameHud>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var _ in _destroyFilter)
            {
                foreach (var entity in _hudFilter)
                {
                    _destroy.Add(entity);
                }
            }
        }

        public void Dispose()
        {
        }
    }
}