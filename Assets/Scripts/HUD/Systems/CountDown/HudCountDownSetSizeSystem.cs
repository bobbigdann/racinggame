﻿using Scripts.GUI;
using Scripts.HUD.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using UnityEngine.UIElements.Experimental;

namespace HUD.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class HudCountDownSetSizeSystem : ISystem
    {
        [Inject] private Stash<CountDown> _countDown;
        [Inject] private Stash<CountDownLink> _countDownLink;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<CountDown>()
                .With<CountDownLink>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var countDown = ref _countDown.Get(entity);
                ref var link = ref _countDownLink.Get(entity);
                var hud = link.value;

                float t = countDown.timer / 1f;
                t = hud.invertEasing
                    ? EasingsUtils.EvaluateInvert(t, hud.easing)
                    : EasingsUtils.Evaluate(t, hud.easing);

                float fontSize = Mathf.Lerp(hud.minFontSize, hud.maxFontSize, t);
                hud.countDownText.fontSize = fontSize;
                
                if (hud.countValue == countDown.remainingSeconds) continue;

                hud.countValue = countDown.remainingSeconds;
                hud.countDownText.text = countDown.remainingSeconds.ToString();
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}