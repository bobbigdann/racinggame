﻿using Scripts.HUD.Components;
using DI.Attributes;
using GUI.Containers;
using Scellecs.Morpeh;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace HUD.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class HudCountDownCreateSystem : ISystem
    {
        [Inject] private UiCanvasContainer _uiCanvasContainer;
        [Inject] private ScreenResources _screenResources;
        
        [Inject] private Stash<CreateCountDown> _createCountDown;
        [Inject] private Stash<CountDown> _countDown;
        [Inject] private Stash<CountDownLink> _countDownLink;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter.With<CreateCountDown>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                var hud = Object.Instantiate(_screenResources.HudCountDownPanel, _uiCanvasContainer.Screens);
                _countDown.Set(entity, new CountDown
                {
                    remainingSeconds = 3,
                    timer = 1f
                });
                _countDownLink.Set(entity, new CountDownLink
                {
                    value = hud
                });
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}