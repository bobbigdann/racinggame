﻿using Common.Components;
using Scripts.HUD.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace HUD.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class HudCountDownTimerSystem : ISystem
    {
        [Inject] private Stash<CountDown> _countDown;
        [Inject] private Stash<Destroy> _destroy;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<CountDown>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var countDown = ref _countDown.Get(entity);
                countDown.timer -= deltaTime;
                if (countDown.timer > 0) continue;

                countDown.remainingSeconds--;
                countDown.timer = 1f;

                if (countDown.remainingSeconds <= 0)
                    _destroy.Add(entity);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}