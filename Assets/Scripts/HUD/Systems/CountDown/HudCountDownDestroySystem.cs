﻿using Common.Components;
using Scripts.HUD.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace HUD.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class HudCountDownDestroySystem : ISystem
    {
        [Inject] private Stash<CountDownLink> _countDownLink;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<CountDown>()
                .With<CountDownLink>()
                .With<Destroy>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var link = ref _countDownLink.Get(entity);
                Object.Destroy(link.value.gameObject);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}