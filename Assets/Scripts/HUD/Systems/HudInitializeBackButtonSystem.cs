﻿using DI.Attributes;
using Gameplay.Components;
using GUI.Components.InGameMenuPanel;
using Scellecs.Morpeh;
using Scripts.HUD.Components;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace HUD.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class HudInitializeBackButtonSystem : ISystem
    {
        [Inject] private Stash<InitializeHudBackButton> _initializeHudBackButton;
        [Inject] private Stash<HudBackButton> _hudBackButton;
        [Inject] private Stash<UiCreateInGameMenu> _uiCreateInGameMenu;
        [Inject] private Stash<PauseGame> _pauseGame;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<HudBackButton>()
                .With<InitializeHudBackButton>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                _initializeHudBackButton.Remove(entity);

                Debug.Log("Initializing hud back button");
                ref var button = ref _hudBackButton.Get(entity);
                button.button.onClick.AddListener(() =>
                {
                    Debug.Log("Hud back button press");
                    _uiCreateInGameMenu.Add(World.CreateEntity());
                    _pauseGame.Add(World.CreateEntity());
                });
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}