﻿using System.Globalization;
using Constants;
using DI.Attributes;
using Scellecs.Morpeh;
using Scripts.HUD.Components;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Gears;
using Vehicles.Components.Vehicles.Player;
using Vehicles.Components.Vehicles.Serializable;

namespace Scripts.HUD.Systems.HudElemets
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class HudSpeedometerSystem : ISystem
    {
        [Inject] private Stash<HudSpeedometer> _hudSpeedometer;
        [Inject] private Stash<VehicleRuntimeData> _vehicleRuntimeData;
        [Inject] private Stash<VehicleCurrentGear> _vehicleCurrentGear;
        [Inject] private Stash<VehicleView> _vehicleView;

        public World World { get; set; }

        private Filter _hudFilter;
        private Filter _vehicleFilter;

        public void OnAwake()
        {
            _hudFilter = World.Filter
                .With<HudSpeedometer>();

            _vehicleFilter = World.Filter
                .With<VehicleControlledByLocalPlayer>()
                .With<VehicleView>()
                .With<VehicleRuntimeData>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_hudFilter.IsEmpty() || _vehicleFilter.IsEmpty()) return;

            float speed = 0f;
            float rpm = 0f;
            int gear = 0;
            foreach (var entity in _vehicleFilter)
            {
                ref var view = ref _vehicleView.Get(entity);
                ref var runtimeData = ref _vehicleRuntimeData.Get(entity);
                ref var currentGear = ref _vehicleCurrentGear.Get(entity);

                speed = runtimeData.speed;
                rpm = runtimeData.rpm / view.engineData.maxRpm;
                gear = currentGear.gearId + 1;
            }

            foreach (var entity in _hudFilter)
            {
                ref var hud = ref _hudSpeedometer.Get(entity);
                hud.speedText.text = Mathf.RoundToInt(speed).ToString(CultureInfo.InvariantCulture);
                hud.rpmArrow.eulerAngles = new Vector3(0f, 0f, -rpm * 180f);
                hud.gearText.text = gear.ToString(CultureInfo.InvariantCulture);
            }
        }

        public void Dispose()
        {
            _hudFilter = null;
            _vehicleFilter = null;
        }
    }
}