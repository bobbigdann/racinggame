﻿using Scripts.GUI;
using TMPro;
using UnityEngine;

namespace Scripts.HUD.MonoComponents
{
    public class HudCountDown : MonoBehaviour
    {
        public TextMeshProUGUI countDownText;

        public float maxFontSize;
        public float minFontSize;
        public EasingType easing;
        public bool invertEasing;

        [HideInInspector] public int countValue;
    }
}