﻿using Scellecs.Morpeh;
using Scripts.NPCSpawn.ScriptableObjects;
using Unity.IL2CPP.CompilerServices;

namespace Scripts.NPCSpawn.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct NPCSpawner : IComponent
    {
        public NPCCarsList carsList;
        public TrafficSettings trafficSettings;
    }
}