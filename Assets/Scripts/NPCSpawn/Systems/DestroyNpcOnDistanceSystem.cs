﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Scripts.NPCSpawn.Components;
using Scripts.Settings.Settings;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Player;

namespace Scripts.NPCSpawn.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class DestroyNpcOnDistanceSystem : ISystem
    {
        [Inject] private LevelSettings _levelSettings;

        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<SpawnedNpcList> _spawnedNpcList;
        [Inject] private Stash<Destroy> _destroy;
        
        public World World { get; set; }
        
        private Filter _spawnerFilter;
        private Filter _playerCarFilter;

        public void OnAwake()
        {
            _spawnerFilter = World.Filter
                .With<NPCSpawner>()
                .With<SpawnedNpcList>();
            
            _playerCarFilter = World.Filter
                .With<VehicleControlledByLocalPlayer>()
                .With<VehicleUnityView>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var playerEntity in _playerCarFilter)
            {
                foreach (var entity in _spawnerFilter)
                {
                    ref var spawnedNpcList = ref _spawnedNpcList.Get(entity);
                    if (spawnedNpcList.value.Count == 0) continue;
                    
                    ref var playerView = ref _vehicleUnityView.Get(playerEntity);
                    float targetZ = playerView.transform.position.z - _levelSettings.roadDestroyDistance;

                    var first = spawnedNpcList.value.First.Value;
                    ref var firstView = ref _vehicleUnityView.Get(first);
                    if (firstView.transform.position.z > targetZ) continue;
                    
                    spawnedNpcList.value.RemoveFirst();
                    _destroy.Add(first);
                }
            }
        }

        public void Dispose()
        {
            _spawnerFilter = null;
            _playerCarFilter = null;
        }
    }
}