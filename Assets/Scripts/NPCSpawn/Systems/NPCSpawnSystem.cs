﻿using DI.Attributes;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using RoadSpawn.Components;
using Scripts.NPCSpawn.Components;
using Scripts.Settings.Settings;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.NPC;
using Vehicles.Components.Vehicles.Player;

namespace Scripts.NPCSpawn.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class NPCSpawnSystem : ISystem
    {
        [Inject] private LevelSettings _levelSettings;
        [Inject] private VehiclePhysicsParameters _physicsParameters;
        
        [Inject] private Stash<NPCSpawner> _npcSpawner;
        [Inject] private Stash<NPCSpawnerTimer> _npcSpawnerTimer;
        [Inject] private Stash<RailwayData> _railwayData;
        [Inject] private Stash<VehicleCurrentLineRailwayNpc> _vehicleCurrentLineRailwayNpc;
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<SpawnedNpcList> _spawnedNpcList;
        [Inject] private Stash<VehicleStartVelocity> _vehicleStartVelocity;
        [Inject] private Stash<VehicleControlledByNpc> _vehicleControlledByNpc;

        public World World { get; set; }

        private Filter _spawnerFilter;
        private Filter _playerFilter;
        private Filter _railwayFilter;
        
        public void OnAwake()
        {
            _spawnerFilter = World.Filter
                .With<NPCSpawner>()
                .With<NPCSpawnerTimer>();

            _playerFilter = World.Filter
                .With<VehicleControlledByLocalPlayer>()
                .With<VehicleUnityView>();

            _railwayFilter = World.Filter
                .With<RailwayData>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _spawnerFilter)
            {
                ref var timer = ref _npcSpawnerTimer.Get(entity);
                timer.time -= deltaTime;
                if (timer.time > 0f) continue;

                ref var spawner = ref _npcSpawner.Get(entity);
                var traffic = spawner.trafficSettings;
                timer.time = Random.Range(traffic.minSpawnTime, traffic.maxSpawnTime);

                float z = 0f;
                ref var spawnedList = ref _spawnedNpcList.Get(entity);
                if (spawnedList.value.Count > 0)
                {
                    var last = spawnedList.value.Last.Value;
                    ref var lastView = ref _vehicleUnityView.Get(last);
                    
                    z = lastView.transform.position.z;
                }
                
                foreach (var playerEntity in _playerFilter)
                {
                    ref var unityView = ref _vehicleUnityView.Get(playerEntity);
                    z = Mathf.Max(unityView.transform.position.z, z);
                }
                
                z += _levelSettings.npcSpawnZOffset;

                var cars = spawner.carsList.cars;
                int carId = Random.Range(0, cars.Count);

                foreach (var railwayEntity in _railwayFilter)
                {
                    ref var railwayData = ref _railwayData.Get(railwayEntity);
                    int lineId = Random.Range(0, railwayData.pointsLines.Count);
                    float x = railwayData.pointsLines[lineId].position.x;

                    var position = Vector3.forward * z;
                    position.x = x;

                    var carGO = Object.Instantiate(cars[carId]);
                    carGO.transform.position = position;

                    var carProvider = carGO.GetComponent<EntityProvider>();
                    var npcEntity = carProvider.Entity;
                    spawnedList.value.AddLast(npcEntity);
                    _vehicleControlledByNpc.Set(npcEntity);
                    _vehicleCurrentLineRailwayNpc.Set(npcEntity, new VehicleCurrentLineRailwayNpc
                    {
                        Index = lineId,
                        X = x
                    });
                    _vehicleStartVelocity.Set(npcEntity, new VehicleStartVelocity
                    {
                        value = _physicsParameters.npcStartVelocity,
                    });
                }
            }
        }

        public void Dispose()
        {
            _spawnerFilter = null;
            _playerFilter = null;
            _railwayFilter = null;
        }
    }
}