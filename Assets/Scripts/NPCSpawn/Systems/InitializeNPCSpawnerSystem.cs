﻿using System.Collections.Generic;
using DI.Attributes;
using Level.Components;
using Scellecs.Morpeh;
using Scripts.NPCSpawn.Components;
using Scripts.NPCSpawn.ScriptableObjects;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;

namespace Scripts.NPCSpawn.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class InitializeNPCSpawnerSystem : ISystem
    {
        [Inject] private TrafficSettingsResources _trafficResources;
        [Inject] private LevelsDataResources _levelsDataResources;
        [Inject] private SettingsContainer _settings;
        
        [Inject] private Stash<NPCSpawner> _npcSpawner;
        [Inject] private Stash<NPCSpawnerTimer> _npcSpawnerTimer;
        [Inject] private Stash<InitializeLevel> _initializeLevel;
        [Inject] private Stash<CurrentLevel> _currentLevel;
        [Inject] private Stash<SpawnedNpcList> _spawnedNpcList;
        
        public World World { get; set; }

        private Filter _initializeFilter;
        private Filter _currentLevelFilter;

        public void OnAwake()
        {
            _initializeFilter = World.Filter.With<InitializeLevel>();
            _currentLevelFilter = World.Filter.With<CurrentLevel>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_initializeFilter.IsEmpty()) return;

            foreach (var levelEntity in _currentLevelFilter)
            {
                ref var currentLevel = ref _currentLevel.Get(levelEntity);
                var levelData = _levelsDataResources.GetDataByLevelId(currentLevel.levelId);
                foreach (var entity in _initializeFilter)
                {
                    var trafficSettings = _trafficResources.value[currentLevel.trafficId];
                    var carsList = levelData.NPCCarsList;
                    
                    var spawnerEntity = World.CreateEntity();
                    _npcSpawner.Set(spawnerEntity, new NPCSpawner
                    {
                        trafficSettings = trafficSettings,
                        carsList = carsList
                    });
                    
                    _npcSpawnerTimer.Set(spawnerEntity, new NPCSpawnerTimer
                    {
                        time = _settings.NPCSpawnDelay
                    });
                    
                    _spawnedNpcList.Set(spawnerEntity, new SpawnedNpcList
                    {
                        value = new LinkedList<Entity>()
                    });
                }
            }
        }

        public void Dispose()
        {
            _initializeFilter = null;
            _currentLevelFilter = null;
        }
    }
}