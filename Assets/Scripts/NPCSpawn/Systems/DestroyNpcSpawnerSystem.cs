﻿using Common.Components;
using DI.Attributes;
using Level.Components;
using Scellecs.Morpeh;
using Scripts.NPCSpawn.Components;
using Unity.IL2CPP.CompilerServices;

namespace Scripts.NPCSpawn.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class DestroyNpcSpawnerSystem : ISystem
    {
        [Inject] private Stash<Destroy> _destroy;
        [Inject] private Stash<SpawnedNpcList> _spawnedNpcList;
        
        public World World { get; set; }

        private Filter _spawnerFilter;
        private Filter _cleanLevelFilter;

        public void OnAwake()
        {
            _spawnerFilter = World.Filter.With<NPCSpawner>();
            _cleanLevelFilter = World.Filter.With<CleanLevel>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _cleanLevelFilter)
            {
                foreach (var spawnerEntity in _spawnerFilter)
                {
                    _destroy.Add(spawnerEntity);
                    ref var spawnedNpcList = ref _spawnedNpcList.Get(spawnerEntity);
                    foreach (var npcEntity in spawnedNpcList.value)
                    {
                        _destroy.Set(npcEntity);
                    }
                    spawnedNpcList.value.Clear();
                }
            }
        }

        public void Dispose()
        {
            _spawnerFilter = null;
            _cleanLevelFilter = null;
        }
    }
}