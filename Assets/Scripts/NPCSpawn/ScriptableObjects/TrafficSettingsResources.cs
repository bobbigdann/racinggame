﻿using UnityEngine;

namespace Scripts.NPCSpawn.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Tools/Traffic Parameters List", fileName = "NewTrafficParametersList")]
    public class TrafficSettingsResources : ScriptableObject
    {
        public TrafficSettings[] value;
    }
}