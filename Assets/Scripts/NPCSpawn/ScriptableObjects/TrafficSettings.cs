﻿using UnityEngine;

namespace Scripts.NPCSpawn.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Tools/Traffic Settings", fileName = "NewTrafficSettings")]
    public class TrafficSettings : ScriptableObject
    {
        public int maxCarsSpawnCount;
        public float minSpawnTime;
        public float maxSpawnTime;
    }
}