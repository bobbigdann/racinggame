﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts.NPCSpawn.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Tools/NPC Cars List", fileName = "NewNPCCarsList")]
    public class NPCCarsList : ScriptableObject
    {
        public List<GameObject> cars;
    }
}