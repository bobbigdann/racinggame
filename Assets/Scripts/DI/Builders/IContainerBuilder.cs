﻿using System;

namespace DI.Builders
{
    public interface IContainerBuilder
    {
        void Register<T>(T @object);
        void RegisterAs(Type type, object @object);
        void Register<T1, T2>(T2 @object) where T2 : T1;
        T Create<T>(params object[] args) where T : new();
        T CreateAndRegister<T>(params object[] args) where T : new();
    }
}