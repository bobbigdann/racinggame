﻿using Scripts.Settings.Settings;
using UnityEngine;

namespace Scripts.Settings.Mono
{
    public class GlobalSettings : MonoBehaviour
    {
        public SoundSettings soundSettings;
        public HudSettings hudSettings;
        public VehicleSettings vehicleSettings;
        public LevelSettings levelSettings;
    }
}