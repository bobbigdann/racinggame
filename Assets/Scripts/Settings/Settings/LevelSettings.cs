﻿using System;
using UnityEngine.Serialization;

namespace Scripts.Settings.Settings
{
    [Serializable]
    public class LevelSettings
    {
        public float roadSpawnDistance;
        public float roadDestroyDistance;
        public float metersForScores;
        public float npcSpawnZOffset;
    }
}