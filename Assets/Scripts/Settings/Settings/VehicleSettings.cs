﻿using System;
using UnityEngine;

namespace Scripts.Settings.Settings
{
    [Serializable]
    public class VehicleSettings
    {
        public KeyCode gasKey;
        public KeyCode brakeKey;
        public KeyCode handbrakeKey;
        public KeyCode turnRightKey;
        public KeyCode turnLeftKey;
        public KeyCode gearShiftUpKey;
        public KeyCode gearShiftDownKey;
    }
}