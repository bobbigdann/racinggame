﻿using System;

namespace Scripts.Settings.Settings
{
    [Serializable]
    public class SoundSettings
    {
        public float EffectsVolume;
        public float MusicVolume;
    }
}