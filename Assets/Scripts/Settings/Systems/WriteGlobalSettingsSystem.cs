﻿using Constants;
using Scripts.Settings.Settings;
using DI.Attributes;
using Scellecs.Morpeh;
using Scripts.Settings.Mono;
using Settings.Components;
using UnityEngine;

namespace Settings.Systems
{
    public class WriteGlobalSettingsSystem : ISystem
    {
        [Inject] private SoundSettings _soundSettings;
        [Inject] private HudSettings _hudSettings;
        
        public World World { get; set; }

        private Filter _readSettingsFilter;
        
        public void OnAwake()
        {
            _readSettingsFilter = World.Filter.With<WriteGlobalSettings>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _readSettingsFilter)
            {
                PlayerPrefs.SetFloat(PlayerPrefsKeys.EffectVolume, _soundSettings.EffectsVolume);
                PlayerPrefs.SetFloat(PlayerPrefsKeys.MusicVolume, _soundSettings.MusicVolume);
                PlayerPrefs.SetInt(PlayerPrefsKeys.HudControlsEnabled, _hudSettings.hudControlsEnabled ? 1 : 0);
                PlayerPrefs.Save();
            }
        }
        
        public void Dispose()
        {
            _readSettingsFilter = null;
        }
    }
}