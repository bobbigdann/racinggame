﻿using Common.Components;
using Constants;
using Scripts.Settings.Settings;
using DI.Attributes;
using Scellecs.Morpeh;
using Scripts.Settings.Mono;
using Settings.Components;
using UnityEngine;

namespace Settings.Systems
{
    public class ReadGlobalSettingsSystem : ISystem
    {
        [Inject] private SoundSettings _soundSettings;
        [Inject] private HudSettings _hudSettings;
        
        [Inject] private Stash<GlobalSettingsChanged> _changedSettingsCache;
        
        public World World { get; set; }

        private Filter _readSettingsFilter;
        
        public void OnAwake()
        {
            _readSettingsFilter = World.Filter.With<ReadGlobalSettings>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _readSettingsFilter)
            {
                _soundSettings.EffectsVolume = PlayerPrefs.GetFloat(PlayerPrefsKeys.EffectVolume, 1f);
                _soundSettings.MusicVolume = PlayerPrefs.GetFloat(PlayerPrefsKeys.MusicVolume, 1f);
                _hudSettings.hudControlsEnabled = PlayerPrefs.GetInt(PlayerPrefsKeys.HudControlsEnabled, 0) == 1;

                _changedSettingsCache.Add(entity);
            }
        }
        
        public void Dispose()
        {
            _readSettingsFilter = null;
        }
    }
}