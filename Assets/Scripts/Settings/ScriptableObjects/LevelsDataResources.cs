﻿using System;
using System.Collections.Generic;
using Scripts.NPCSpawn.ScriptableObjects;
using RoadSpawn.Providers;
using UnityEngine;

namespace Settings.ScriptableObjects.References
{
    [CreateAssetMenu(menuName = "Tools/Level Data Resources", fileName = "NewLevelDataResources")]
    public class LevelsDataResources : ScriptableObject
    {
        [SerializeField] public List<LevelDataItemResources> data;

        public LevelDataItemResources GetDataByLevelId(int levelId)
        {
            if (data == null || data.Count == 0) return default;
            
            if (levelId < 0) levelId = 0;
            if (levelId >= data.Count) levelId = data.Count - 1;

            return data[levelId];
        }
    }

    [Serializable]
    public struct LevelDataItemResources
    {
        [field: SerializeField] public RoadSpawnerProvider RoadSpawner { get; set; }
        [field: SerializeField] public RoadSpawnerProvider RoadsideSpawner { get; set; }
        [field: SerializeField] public NPCCarsList NPCCarsList { get; set; }
        [field: SerializeField] public Sprite LevelImage { private set; get; }
    }
}