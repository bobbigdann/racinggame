﻿using Unity.Mathematics;
using UnityEngine;

namespace Settings.ScriptableObjects.References
{
    [CreateAssetMenu(fileName = "Vehicle Physics Parameters", menuName = "Vehicles/PhysicsParameters", order = 0)]
    public class VehiclePhysicsParameters : ScriptableObject
    {
        public int physicsStepsCount;
        public float3 gravity;
        public int playerStartVelocity;
        public int npcStartVelocity;
        public CollisionDetectionMode collisionDetectionMode;
    }
}