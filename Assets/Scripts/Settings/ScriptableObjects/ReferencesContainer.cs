﻿using System.Collections.Generic;
using UnityEngine;

namespace Settings.ScriptableObjects.References
{
    [CreateAssetMenu(menuName = "Tools/References/ReferencesContainer", fileName = "ReferencesContainer")]
    public class ReferencesContainer : ScriptableObject
    {
        [field: SerializeField] public List<ScriptableObject> References { get; set; }
    }
}