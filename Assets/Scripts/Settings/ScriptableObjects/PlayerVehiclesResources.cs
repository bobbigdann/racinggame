﻿using System;
using System.Collections.Generic;
using Constants;
using UnityEngine;

namespace Settings.ScriptableObjects.References
{
    [CreateAssetMenu(menuName = "Tools/Player Vehicles Resources", fileName = "NewPlayerVehiclesResources")]
    public class PlayerVehiclesResources : ScriptableObject
    {
        public List<PlayerVehicleContainer> vehicles;
    }

    [Serializable]
    public class PlayerVehicleContainer
    {
        public GameObject gameVehicle;
        public GameObject menuVehicle;

        public int price;

        [Range(0, VehicleHudConstants.MaxViewParameterValue)] public float acceleration;
        [Range(0, VehicleHudConstants.MaxViewParameterValue)] public float maxSpeed;
        [Range(0, VehicleHudConstants.MaxViewParameterValue)] public float controllability;
    }
}