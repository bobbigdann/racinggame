﻿using GUI.MonoComponents.StartGamePanel;
using MonoComponents;
using Scripts.HUD.Components;
using Scripts.HUD.MonoComponents;
using UnityEngine;

namespace Settings.ScriptableObjects.References
{
    [CreateAssetMenu(menuName = "Tools/Screen Resources", fileName = "NewScreenResources")]
    public class ScreenResources : ScriptableObject
    {
        [field: SerializeField] public StartGamePanelView StartGamePanel { get; private set; }
        [field: SerializeField] public CarContainer CarContainer { get; private set; }
        [field: SerializeField] public GameObject SettingsPanel { get; private set; }
        [field: SerializeField] public GameObject SelectLevelPanel { get; private set; }
        [field: SerializeField] public GameObject HudPanel { get; private set; }
        [field: SerializeField] public HudCountDown HudCountDownPanel { get; private set; }
        [field: SerializeField] public GameObject HudControlsPanel { get; private set; }
        [field: SerializeField] public GameObject GameOverPanel { get; private set; }
        [field: SerializeField] public GameObject InGameMenuPanel { get; private set; }
    }
}