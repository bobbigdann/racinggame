﻿using UnityEngine;

namespace Settings.ScriptableObjects.References
{
    [CreateAssetMenu(menuName = "Tools/Settings Container", fileName = "NewSettingsContainer")]
    public class SettingsContainer : ScriptableObject
    {
        [field: SerializeField] public float CoefficientScoreToMoney { private set; get; }
        [field: SerializeField] public float NPCSpawnDelay { private set; get; }
    }
}