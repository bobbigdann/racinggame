﻿using Scellecs.Morpeh;
using Player;
using Res.Components;
using Res.Data;

namespace Res.Extensions
{
    public static class PlayerMoneyExtensions
    {
        public static void GiveMoney(this IPlayerData playerData, int value)
        {
            playerData.Resources[ResourceType.Money] += value;
            World.Default.CreateEntity().SetComponent(new PlayerUpdateResource(ResourceType.Money));
        }

        public static void TakeMoney(this IPlayerData playerData, int value)
        {
            playerData.Resources[ResourceType.Money] -= value;
            World.Default.CreateEntity().SetComponent(new PlayerUpdateResource(ResourceType.Money));
        }

        public static int GetMoney(this IPlayerData playerData)
        {
            return playerData.Resources[ResourceType.Money];
        }
    }
}