﻿using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using Res.Components;
using Res.Data;
using Unity.IL2CPP.CompilerServices;

namespace Res.Providers
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class PlayerMoneyViewProvider : MonoProvider<PlayerMoneyView>
    {
        private void Start()
        {
            Entity.SetComponent(new PlayerUpdateResource(ResourceType.Money));
        }
    }
}