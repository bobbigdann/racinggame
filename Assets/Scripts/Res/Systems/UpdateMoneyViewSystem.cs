﻿using System.Runtime.InteropServices;
using DI.Attributes;
using Scellecs.Morpeh;
using Player;
using Res.Components;
using Res.Data;
using Res.Extensions;
using Unity.IL2CPP.CompilerServices;

namespace Res.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class UpdateMoneyViewSystem : ISystem
    {
        [Inject] private readonly IPlayerData _playerData;
        [Inject] private readonly Stash<PlayerUpdateResource> _updateResourceCache;
        [Inject] private readonly Stash<PlayerMoneyView> _viewCache;
        
        public World World { get; set; }

        private Filter _resourceChangedFilter;
        private Filter _viewFilter;
        
        public void OnAwake()
        {
            _resourceChangedFilter = World.Filter.With<PlayerUpdateResource>();
            _viewFilter = World.Filter.With<PlayerMoneyView>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var changedEntity in _resourceChangedFilter)
            {
                var resource = _updateResourceCache.Get(changedEntity);
                if (resource.Type != ResourceType.Money) continue;
                
                foreach (var viewEntity in _viewFilter)
                {
                    ref var component = ref _viewCache.Get(viewEntity);
                    component.text.text = _playerData.GetMoney().ToString();
                }
            }
        }
        
        public void Dispose()
        {
            _viewFilter = null;
            _resourceChangedFilter = null;
        }
    }
}