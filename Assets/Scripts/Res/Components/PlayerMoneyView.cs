﻿using System;
using Scellecs.Morpeh;
using TMPro;
using Unity.IL2CPP.CompilerServices;

namespace Res.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct PlayerMoneyView : IComponent
    {
        public TextMeshProUGUI text;
    }
}