﻿using Scellecs.Morpeh;
using Res.Data;
using Unity.IL2CPP.CompilerServices;

namespace Res.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct PlayerUpdateResource : IComponent
    {
        public ResourceType Type;

        public PlayerUpdateResource(ResourceType type)
        {
            Type = type;
        }
    }
}