﻿using DI.Attributes;
using GUI.Components.StartPanel;
using Scellecs.Morpeh;
using Player.Components;
using Scores.Components;
using Settings.Components;
using SoundPlaying.Data;
using SoundPlaying.UpdateSystems;

public class StartGameSystem : IInitializer
{
    [Inject] private readonly SoundFactorySystem _soundFactorySystem;
    
    public World World { get; set; }

    public void OnAwake()
    {
        _soundFactorySystem.Create(SoundType.MainTheme)
            .SetLoop(true)
            .Play();

        World.Default.CreateEntity().AddComponent<UiCreateStartGamePanel>();
        World.Default.CreateEntity().AddComponent<ReadGlobalSettings>();
        World.Default.CreateEntity().AddComponent<CurrentScore>();
        World.Default.CreateEntity().AddComponent<PlayerInitializeData>();
    }

    public void Dispose()
    {
        
    }
}