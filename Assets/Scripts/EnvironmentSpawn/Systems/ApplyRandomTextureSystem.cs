﻿using DI.Attributes;
using EnvironmentSpawn.Components;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace EnvironmentSpawn.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class ApplyRandomTextureSystem : ISystem
    {
        public World World { get; set; }

        private Filter _filter;

        [Inject] private Stash<ApplyRandomTexture> _applyRandomTexture;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<ApplyRandomTexture>()
                .With<RandomTextureInitialize>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var applyTexture = ref _applyRandomTexture.Get(entity);
                var texture = applyTexture.textures[Random.Range(0, applyTexture.textures.Count)];
                
                var propertyBlock = new MaterialPropertyBlock();
                propertyBlock.SetTexture(applyTexture.propertyName, texture);
                applyTexture.renderer.SetPropertyBlock(propertyBlock);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}