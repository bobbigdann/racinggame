﻿using DI.Attributes;
using EnvironmentSpawn.Components;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace EnvironmentSpawn.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class EnvironmentSpawnSystem : ISystem
    {
        public World World { get; set; }

        private Filter _spawnerFilter;

        [Inject] private Stash<EnvironmentSpawner> _environmentSpawner;
        [Inject] private Stash<EnvironmentSpawnerInitialized> _environmentSpawnerInitialized;

        public void OnAwake()
        {
            _spawnerFilter = World.Filter
                .With<EnvironmentSpawner>()
                .Without<EnvironmentSpawnerInitialized>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _spawnerFilter)
            {
                _environmentSpawnerInitialized.Add(entity);
                
                ref var spawner = ref _environmentSpawner.Get(entity);
                float chance = Random.Range(0f, 1f);
                if (chance > spawner.chance) continue;
                
                var objects = spawner.list.prefabs;
                if (objects.Count == 0) continue;
                
                var randomObject = objects[Random.Range(0, objects.Count)];
                var spawnedGO = Object.Instantiate(randomObject, spawner.parent);
                
                spawnedGO.SetActive(true);
                spawnedGO.transform.localPosition = Vector3.zero;
                spawnedGO.transform.localRotation = Quaternion.Euler(0f, spawner.rotation, 0f);
            }
        }

        public void Dispose()
        {
            _spawnerFilter = null;
        }
    }
}