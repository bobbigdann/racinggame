﻿using Scellecs.Morpeh;
using System;
using EnvironmentSpawn.ScriptableObjects;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace EnvironmentSpawn.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct EnvironmentSpawner : IComponent
    {
        public Transform parent;
        [Range(0, 1)] public float chance;
        public float rotation;
        public EnvironmentPrefabsList list;
    }
}