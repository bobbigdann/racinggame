﻿using System.Collections.Generic;
using UnityEngine;

namespace EnvironmentSpawn.ScriptableObjects
{
    [CreateAssetMenu(fileName = "NewEnvironmentPrefabsList", menuName = "Road Spawn Data/Environment Prefabs List")]
    public class EnvironmentPrefabsList : ScriptableObject
    {
        public List<GameObject> prefabs;
    }
}