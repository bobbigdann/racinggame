﻿using EnvironmentSpawn.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;

namespace EnvironmentSpawn.Providers
{
    public class ApplyRandomTextureProvider : MonoProvider<ApplyRandomTexture>
    {
        private void Start()
        {
            Entity.AddComponent<RandomTextureInitialize>();
        }
    }
}