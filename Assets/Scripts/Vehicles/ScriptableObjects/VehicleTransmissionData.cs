﻿using System.Collections.Generic;
using UnityEngine;

namespace Vehicles.ScriptableObjects
{
    [CreateAssetMenu(fileName = "VehicleTransmissionData", menuName = "Vehicles/Data/Transmission")]
    public class VehicleTransmissionData : ScriptableObject
    {
        [SerializeField] public float gearShiftingDelay;
        [SerializeField] public List<float> gears;
        [SerializeField] public float reverseGear;
    }
}