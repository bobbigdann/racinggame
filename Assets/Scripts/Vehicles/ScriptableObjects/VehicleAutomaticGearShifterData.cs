﻿using UnityEngine;

namespace Vehicles.ScriptableObjects
{
    [CreateAssetMenu(fileName = "VehicleAutomaticGearShifterData", menuName = "Vehicles/Data/Automatic Gear Shifter")]
    public class VehicleAutomaticGearShifterData : ScriptableObject
    {
        [SerializeField] public float shiftUpRpm;
        [SerializeField] public float shiftDownRpm;
    }
}