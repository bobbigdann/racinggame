﻿using UnityEngine;

namespace Vehicles.ScriptableObjects
{
    [CreateAssetMenu(fileName = "VehicleSteeringData", menuName = "Vehicles/Data/Steering", order = 0)]
    public class VehicleSteeringData : ScriptableObject
    {
        [SerializeField] public bool ackerman;
        [SerializeField] public bool instantRebind;
        
        [Header("Ackerman parameters")]
        [SerializeField] public bool inverseAckerman;
        [SerializeField] public float wheelBase;
        [SerializeField] public float rearTrack;
        [SerializeField] public float turnRadius;
        [SerializeField] [Range(0.5f, 20f)] public float steeringSpeed;

        [Header("Non ackerman parameters")]
        [SerializeField] public float minSpeed;
        [SerializeField] public float maxSpeed;
        [SerializeField] public float steeringSpeedMultiplier;
        [SerializeField] public AnimationCurve angleCurve;
        [SerializeField] public AnimationCurve steeringSpeedCurve;
    }
}