﻿using UnityEngine;

namespace Vehicles.ScriptableObjects
{
    [CreateAssetMenu(fileName = "VehicleHelpers", menuName = "Vehicles/Data/Helpers", order = 0)]
    public class VehicleHelpersData : ScriptableObject
    {
        [Header("Traction control")]
        [SerializeField] public bool tractionEnabled;
        [SerializeField] public float tractionSlipThreshold;

        [Header("ABS")]
        [SerializeField] public bool absEnabled;
        [SerializeField] public float absSlipThreshold;
    }
}