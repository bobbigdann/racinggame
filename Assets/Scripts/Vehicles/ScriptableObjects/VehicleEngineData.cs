﻿using UnityEngine;

namespace Vehicles.ScriptableObjects
{
    [CreateAssetMenu(fileName = "VehicleEngineData", menuName = "Vehicles/Data/Engine")]
    public class VehicleEngineData : ScriptableObject
    {
        [SerializeField] public float minRpm;
        [SerializeField] public float maxRpm;
        
        [Header("New vehicles used values")]
        [SerializeField] public float maxSpeedForward;
        [SerializeField] public float maxSpeedReverse;
        [SerializeField] public float reverseTorque;
        [SerializeField] public float maxTorque;
        [SerializeField] public AnimationCurve torqueCurve;
        [SerializeField] public AnimationCurve torqueToRpmCurve;
        [SerializeField] public float brakePower;
        [SerializeField] public float handbrakePower;
    }
}