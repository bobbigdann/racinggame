﻿using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using Vehicles.Components.Wheels;
using Vehicles.Components.Wheels.Serializable;

namespace Vehicles.Providers.Wheels
{
    public class WheelViewProvider : MonoProvider<WheelView>
    {
        protected override void Initialize()
        {
            Entity.SetComponent(new WheelInitialize
            {
                thisObject = gameObject
            });
        }
    }
}