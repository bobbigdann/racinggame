﻿using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Serializable;

namespace Vehicles.Providers.Vehicle
{
    [RequireComponent(typeof(Rigidbody))]
    public class VehicleViewProvider : MonoProvider<VehicleView>
    {
        protected override void Initialize()
        {
            Entity.SetComponent(new VehicleUnityView
            {
                gameObject = gameObject,
                transform = transform,
                rigidbody = GetComponent<Rigidbody>()
            });
            Entity.SetComponent(new VehicleInitialize());
        }
    }
}