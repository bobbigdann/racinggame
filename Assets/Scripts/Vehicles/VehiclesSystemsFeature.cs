﻿using DI.Builders;
using Scellecs.Morpeh;
using Scripts;
using Vehicles.Initializers;
using Vehicles.Systems.FixedSystems.Vehicle;
using Vehicles.Systems.FixedSystems.Wheels;
using Vehicles.Systems.UpdateSystems.Vehicle;
using Vehicles.Systems.UpdateSystems.Vehicle.Npc;
using Vehicles.Systems.UpdateSystems.Vehicle.Player;
using Vehicles.Systems.UpdateSystems.Wheels;

namespace Vehicles
{
    public class VehiclesSystemsFeature : ISystemsFeature
    {
        public SystemsGroup GetGroup(World world, IContainerBuilder container)
        {
            var group = world.CreateSystemsGroup();
            
            group.AddInitializer(container.Create<SurfaceParametersMapperInitializer>());
            
            // Update wheels systems
            group.AddSystem(container.Create<WheelInitializeSystem>());
            
            // Update vehicle systems -------------
            // Initialize
            group.AddSystem(container.Create<VehiclePlayerInitializeSystem>());
            group.AddSystem(container.Create<VehicleNpcInitializeSystem>());
            group.AddSystem(container.Create<VehicleApplyStartVelocitySystem>());
            group.AddSystem(container.Create<VehicleInitializeSystem>());
            
            // Player and AI inputs
            group.AddSystem(container.Create<VehiclePlayerInputsSystem>());
            group.AddSystem(container.Create<VehicleNpcMovingSystem>());
            group.AddSystem(container.Create<VehicleAutoGasInputSystem>());
            
            // Apply inputs
            group.AddSystem(container.Create<VehicleApplyBrakeTorqueSystem>());
            group.AddSystem(container.Create<VehicleApplySteeringSystem>());
            
            // Gears
            group.AddSystem(container.Create<VehicleAutomaticGearShiftSystem>());
            group.AddSystem(container.Create<VehicleManualGearShiftSystem>());
            group.AddSystem(container.Create<VehicleGearShiftingDelaySystem>());

            // Other
            group.AddSystem(container.Create<VehicleNpcChangeLineSystem>());
            group.AddSystem(container.Create<VehicleMonoDebugSystem>());
            group.AddSystem(container.Create<VehiclePlayerCollisionCheckSystem>());

            group.AddSystem(container.Create<VehiclePauseSystem>());
            group.AddSystem(container.Create<VehicleResumeSystem>());

            // Fixed systems -------------
            group.AddSystem(container.Create<VehicleCalculateRpmSystem>());
            group.AddSystem(container.Create<VehicleInfoValuesSystem>());
            group.AddSystem(container.Create<VehicleEnginePowerSystem>());
            group.AddSystem(container.Create<VehiclePrePhysicsSystem>());
            group.AddSystem(container.Create<WheelPrePhysicsSystem>());
            group.AddSystem(container.Create<WheelVisualsSystem>());
            group.AddSystem(container.Create<VehiclePhysicsSystem>());
            group.AddSystem(container.Create<VehiclePostPhysicsSystem>());

            return group;
        }
    }
}