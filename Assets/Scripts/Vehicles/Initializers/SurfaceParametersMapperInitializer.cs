﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Data;

namespace Vehicles.Initializers
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class SurfaceParametersMapperInitializer : IInitializer
    {
        [Inject] private SurfaceParametersMapper _parametersMapper;
        
        public World World { get; set; }

        public void OnAwake()
        {
            _parametersMapper.Initialize();
        }

        public void Dispose()
        {
            _parametersMapper.Dispose();
        }
    }
}