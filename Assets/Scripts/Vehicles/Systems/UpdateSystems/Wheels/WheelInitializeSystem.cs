﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Wheels;
using Vehicles.Components.Wheels.Serializable;
using Vehicles.Providers.Vehicle;

namespace Vehicles.Systems.UpdateSystems.Wheels
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class WheelInitializeSystem : ISystem
    {
        [Inject] private Stash<WheelInitialize> _wheelInitialize;
        [Inject] private Stash<WheelVehicleLink> _wheelVehicleLink;
        [Inject] private Stash<WheelGroundHit> _wheelHit;
        [Inject] private Stash<WheelSuspension> _wheelSuspension;
        [Inject] private Stash<WheelRpm> _wheelRpm;
        [Inject] private Stash<WheelGroundParams> _wheelGroundParams;
        [Inject] private Stash<WheelSmoothTotalSlip> _wheelSmoothTotalSlip;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<WheelView>()
                .With<WheelInitialize>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var initialize = ref _wheelInitialize.Get(entity);
                var parentVehicle = initialize.thisObject.GetComponentInParent<VehicleViewProvider>();

                if (!ReferenceEquals(parentVehicle, null))
                {
                    _wheelVehicleLink.Set(entity, new WheelVehicleLink
                    {
                        value = parentVehicle.Entity
                    });
                }

                _wheelHit.Add(entity);
                _wheelRpm.Add(entity);
                _wheelSuspension.Add(entity);
                _wheelGroundParams.Add(entity);
                _wheelSmoothTotalSlip.Add(entity);
                
                _wheelInitialize.Remove(entity);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}