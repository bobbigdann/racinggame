﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Vehicles.Serializable;
using Vehicles.Components.Wheels;
using Vehicles.Components.Wheels.Axles;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleApplySteeringSystem : ISystem
    {
        [Inject] private Stash<VehicleView> _vehicleView;
        [Inject] private Stash<VehicleInputs> _vehicleInputs;
        [Inject] private Stash<VehicleWheels> _vehicleWheels;
        [Inject] private Stash<VehicleRuntimeData> _vehicleRuntimeData;

        [Inject] private Stash<WheelSteerAngle> _wheelSteerAngle;
        [Inject] private Stash<WheelRight> _wheelRight;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleInputs>()
                .With<VehicleWheels>()
                .With<VehicleRuntimeData>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var view = ref _vehicleView.Get(entity);
                ref var inputs = ref _vehicleInputs.Get(entity);
                ref var wheels = ref _vehicleWheels.Get(entity);
                if (view.steering.ackerman)
                {
                    float targetSteerAngle = 0f;
                    if (inputs.turnLeftPressed) targetSteerAngle = -1f;
                    else if (inputs.turnRightPressed) targetSteerAngle = 1f;
                    
                    foreach (var wheelEntity in wheels.steering)
                    {
                        bool isRightWheel = _wheelRight.Has(wheelEntity);
                        bool isTurnRight = targetSteerAngle > 0f;

                        float multiplier = isRightWheel ? 1f : -1f;
                        if (isTurnRight != view.steering.inverseAckerman)
                        {
                            multiplier *= -1f;
                        }

                        float divisor = view.steering.turnRadius + multiplier * view.steering.rearTrack * 2f;
                        float angle = divisor > 0
                            ? Mathf.Rad2Deg * Mathf.Atan(view.steering.wheelBase / divisor) * targetSteerAngle
                            : 0f;

                        ref var steer = ref _wheelSteerAngle.Get(wheelEntity);

                        if (view.steering.instantRebind && Mathf.Approximately(angle, 0f))
                            steer.value = 0f;
                        else
                            steer.value = Mathf.Lerp(steer.value, angle, view.steering.steeringSpeed * deltaTime);
                    }
                }
                else
                {
                    ref var runtimeData = ref _vehicleRuntimeData.Get(entity);

                    float targetSteerAngle = 0f;
                    float ratio = Mathf.Clamp01((runtimeData.speed - view.steering.minSpeed) / (view.steering.maxSpeed - view.steering.minSpeed));
                    float steeringSpeed = view.steering.steeringSpeedCurve.Evaluate(ratio) * view.steering.steeringSpeedMultiplier;

                    if (inputs.turnLeftPressed)
                    {
                        targetSteerAngle = -view.steering.angleCurve.Evaluate(ratio);
                    }
                    else if (inputs.turnRightPressed)
                    {
                        targetSteerAngle = view.steering.angleCurve.Evaluate(ratio);
                    }

                    bool hasSteerInput = inputs.turnLeftPressed || inputs.turnRightPressed;
                    foreach (var wheelEntity in wheels.steering)
                    {
                        ref var steer = ref _wheelSteerAngle.Get(wheelEntity);

                        if (view.steering.instantRebind && !hasSteerInput)
                            steer.value = 0f;
                        else
                            steer.value = Mathf.Lerp(steer.value, targetSteerAngle, steeringSpeed * deltaTime);
                    }
                }
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}