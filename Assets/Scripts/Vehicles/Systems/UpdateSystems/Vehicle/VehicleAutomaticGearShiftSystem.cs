﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Gears;
using Vehicles.Components.Vehicles.Serializable;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleAutomaticGearShiftSystem : ISystem
    {
        [Inject] private Stash<VehicleRuntimeData> _vehicleRuntimeData;
        [Inject] private Stash<VehicleAutomaticGearShifter> _vehicleAutomaticGearShifter;
        [Inject] private Stash<VehicleCurrentGear> _vehicleCurrentGear;
        [Inject] private Stash<VehicleView> _vehicleView;
        [Inject] private Stash<VehicleShiftingGear> _vehicleShiftingGear;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleCurrentGear>()
                .With<VehicleView>()
                .With<VehicleRuntimeData>()
                .With<VehicleAutomaticGearShifter>()
                .Without<VehicleManualGearShifter>()
                .Without<VehicleShiftingGear>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var runtimeData = ref _vehicleRuntimeData.Get(entity);
                ref var shifter = ref _vehicleAutomaticGearShifter.Get(entity);
                ref var view = ref _vehicleView.Get(entity);
                ref var currentGear = ref _vehicleCurrentGear.Get(entity);

                if (runtimeData.rpm < shifter.data.shiftDownRpm && currentGear.gearId > 0)
                {
                    currentGear.gearId--;
                    _vehicleShiftingGear.Set(entity, new VehicleShiftingGear
                    {
                        time = view.transmission.gearShiftingDelay,
                    });
                }
                else if (runtimeData.rpm > shifter.data.shiftUpRpm &&
                         currentGear.gearId < view.transmission.gears.Count - 1)
                {
                    currentGear.gearId++;
                    _vehicleShiftingGear.Set(entity, new VehicleShiftingGear
                    {
                        time = view.transmission.gearShiftingDelay,
                    });
                }
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}