﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Gears;
using Vehicles.Components.Vehicles.Serializable;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleManualGearShiftSystem : ISystem
    {
        [Inject] private Stash<VehicleView> _vehicleView;
        [Inject] private Stash<VehicleCurrentGear> _vehicleCurrentGear;
        [Inject] private Stash<VehicleInputs> _vehicleInputs;
        [Inject] private Stash<VehicleShiftingGear> _vehicleShiftingGear;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleInputs>()
                .With<VehicleManualGearShifter>()
                .With<VehicleCurrentGear>()
                .Without<VehicleAutomaticGearShifter>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var inputs = ref _vehicleInputs.Get(entity);
                ref var currentGear = ref _vehicleCurrentGear.Get(entity);
                ref var view = ref _vehicleView.Get(entity);
                
                if (inputs.gearShiftUpPressed && currentGear.gearId < view.transmission.gears.Count)
                {
                    currentGear.gearId++;
                    _vehicleShiftingGear.Set(entity, new VehicleShiftingGear
                    {
                        time = view.transmission.gearShiftingDelay,
                    });
                }
                else if (inputs.gearShiftDownPressed && currentGear.gearId > 0)
                {
                    currentGear.gearId--;
                    _vehicleShiftingGear.Set(entity, new VehicleShiftingGear
                    {
                        time = view.transmission.gearShiftingDelay,
                    });
                }
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}