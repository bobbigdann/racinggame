﻿using DI.Attributes;
using Gameplay.Components;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Wheels;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleResumeSystem : ISystem
    {
        [Inject] private Stash<VehiclePaused> _vehiclePaused;
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<VehicleWheels> _vehicleWheels;
        [Inject] private Stash<WheelPaused> _wheelPaused;
        
        public World World { get; set; }

        private Filter _filter;
        private Filter _vehicleFilter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<ResumeGame>();

            _vehicleFilter = World.Filter
                .With<VehicleUnityView>()
                .With<VehicleWheels>()
                .With<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_filter.IsEmpty()) return;

            foreach (var entity in _vehicleFilter)
            {
                ref var paused = ref _vehiclePaused.Get(entity);
                ref var unityView = ref _vehicleUnityView.Get(entity);
                
                unityView.rigidbody.constraints = RigidbodyConstraints.None;
                unityView.rigidbody.velocity = paused.pausedVelocity;
                
                _vehiclePaused.Remove(entity);

                ref var wheels = ref _vehicleWheels.Get(entity);
                foreach (var wheel in wheels.all)
                {
                    _wheelPaused.Remove(wheel);
                }
            }
        }

        public void Dispose()
        {
            _filter = null;
            _vehicleFilter = null;
        }
    }
}