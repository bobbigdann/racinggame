﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Gears;
using Vehicles.Components.Vehicles.Serializable;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleMonoDebugSystem : ISystem
    {
        [Inject] private Stash<VehicleMonoDebug> _vehicleMonoDebug;
        [Inject] private Stash<VehicleRuntimeData> _vehicleRuntimeData;
        [Inject] private Stash<VehicleCurrentGear> _vehicleCurrentGear;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleMonoDebug>()
                .With<VehicleCurrentGear>()
                .With<VehicleRuntimeData>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var debug = ref _vehicleMonoDebug.Get(entity);
                ref var data = ref _vehicleRuntimeData.Get(entity);
                ref var gear = ref _vehicleCurrentGear.Get(entity);

                debug.value.speed = data.speed;
                debug.value.isGoingReverse = data.isGoingReverse;
                debug.value.torque = data.torque;
                debug.value.torquePerWheel = data.torquePerWheel;
                debug.value.rpm = data.rpm;
                debug.value.gear = gear.gearId;
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}