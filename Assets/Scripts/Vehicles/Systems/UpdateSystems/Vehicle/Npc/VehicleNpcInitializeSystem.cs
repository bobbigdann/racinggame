﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.NPC;

namespace Vehicles.Systems.UpdateSystems.Vehicle.Npc
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleNpcInitializeSystem : ISystem
    {
        [Inject] private Stash<Timer> _timers;
        [Inject] private Stash<VehicleChangeLineNpc> _changeLine;

        private Filter _vehiclesNpcFilter;

        private const float TimeForChangeLine = 10f;
        
        public World World { get; set; }
        
        public void OnAwake()
        {
            _vehiclesNpcFilter = World.Filter
                .With<VehicleControlledByNpc>()
                .With<VehicleInitialize>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var npcEntity in _vehiclesNpcFilter)
            {
                _timers.Set(npcEntity, new Timer
                {
                    FinalTime = TimeForChangeLine,
                    Loop = true,
                    Callback = () => _changeLine.Set(npcEntity)
                });
            }
        }

        public void Dispose()
        {
            _vehiclesNpcFilter = null;
        }
    }
}