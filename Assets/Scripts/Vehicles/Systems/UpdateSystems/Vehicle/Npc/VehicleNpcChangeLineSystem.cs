﻿using DI.Attributes;
using RoadSpawn.Components;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.NPC;

namespace Vehicles.Systems.UpdateSystems.Vehicle.Npc
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleNpcChangeLineSystem : ISystem
    {
        [Inject] private Stash<RailwayData> _railwayData;
        [Inject] private Stash<VehicleCurrentLineRailwayNpc> _vehicleCurrentLineRailwayNpc;
        [Inject] private Stash<VehicleChangeLineNpc> _vehicleChangeLineNpc;

        private Filter _npcVehiclesFilter;
        private Filter _railwayDataFilter;
        
        public World World { get; set; }
        
        public void OnAwake()
        {
            _npcVehiclesFilter = World.Filter
                .With<VehicleControlledByNpc>()
                .With<VehicleCurrentLineRailwayNpc>()
                .With<VehicleChangeLineNpc>()
                .Without<VehiclePaused>();

            _railwayDataFilter = World.Filter.With<RailwayData>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_npcVehiclesFilter.IsEmpty()) return;
            
            foreach (var railwayEntity in _railwayDataFilter)
            {
                ref var railway = ref _railwayData.Get(railwayEntity);

                foreach (var npcEntity in _npcVehiclesFilter)
                {
                    ref var currentLineComponent = ref _vehicleCurrentLineRailwayNpc.Get(npcEntity);

                    var index = currentLineComponent.Index;

                    if (index <= 0) index++;
                    else if (index >= railway.pointsLines.Count - 1) index--;
                    else index += Random.Range(0, 2) * 2 - 1;

                    var position = railway.pointsLines[index].position;
                    currentLineComponent.Index = index;
                    currentLineComponent.X = position.x;
                    
                    _vehicleChangeLineNpc.Remove(npcEntity);
                }
            }
        }

        public void Dispose()
        {
            _npcVehiclesFilter = null;
            _railwayDataFilter = null;
        }
    }
}