﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.NPC;

namespace Vehicles.Systems.UpdateSystems.Vehicle.Npc
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleNpcMovingSystem : ISystem
    {
        [Inject] private Stash<VehicleInputs> _vehicleInputs;
        [Inject] private Stash<VehicleCurrentLineRailwayNpc> _currentLineRailway;
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;

        private Filter _npcVehiclesFilter;
        
        private const float MaxAngle = 10f;
            
        public World World { get; set; }
        
        public void OnAwake()
        {
            _npcVehiclesFilter = World.Filter
                .With<VehicleControlledByNpc>()
                .With<VehicleUnityView>()
                .With<VehicleCurrentLineRailwayNpc>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _npcVehiclesFilter)
            {
                var unityView = _vehicleUnityView.Get(entity);
                ref var currentLineComponent = ref _currentLineRailway.Get(entity);

                var inputs = new VehicleInputs
                {
                    gasPressed = true
                };

                var distance = currentLineComponent.X - unityView.transform.position.x;
                var maxAngle = Mathf.Clamp(distance * 3f, -MaxAngle, MaxAngle);
                
                var rotation = unityView.transform.rotation.eulerAngles;
                var rotationY = rotation.y <= 180 ? rotation.y : rotation.y - 360;

                inputs.turnRightPressed = false;
                inputs.turnLeftPressed = false;

                if (Mathf.Abs(maxAngle - rotationY) > 5f)
                {
                    if (distance > 0)
                    {
                        if (rotationY < maxAngle)
                        {
                            inputs.turnRightPressed = true;
                        }
                        else
                        {
                            inputs.turnLeftPressed = true;
                        }
                    }
                    else
                    {
                        if (rotationY > maxAngle)
                        {
                            inputs.turnLeftPressed = true;
                        }
                        else
                        {
                            inputs.turnRightPressed = true;
                        }
                    }
                }

                _vehicleInputs.Set(entity, inputs);
            }
        }

        public void Dispose()
        {
            _npcVehiclesFilter = null;
        }
    }
}