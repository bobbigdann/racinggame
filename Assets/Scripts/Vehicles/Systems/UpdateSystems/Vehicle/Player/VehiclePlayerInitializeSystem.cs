﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Services;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Player;
using Vehicles.Components.Vehicles.Serializable;

namespace Vehicles.Systems.UpdateSystems.Vehicle.Player
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehiclePlayerInitializeSystem : ISystem
    {
        [Inject] private ICameraService _cameraService;
        
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<VehicleAutoGasInput> _vehicleAutoGasInput;
        [Inject] private Stash<VehiclePlayerCollisionReceiverLink> _vehiclePlayerCollisionReceiverLink;
        [Inject] private Stash<VehicleCheckCollision> _vehicleCheckCollision;
        [Inject] private Stash<Timer> _timer;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleControlledByLocalPlayer>()
                .With<VehiclePlayerCollisionReceiverLink>()
                .With<VehicleInitialize>()
                .With<VehicleView>()
                .With<VehicleUnityView>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var unityView = ref _vehicleUnityView.Get(entity);
                
                _cameraService.SetFollowTo(unityView.transform);
                _cameraService.SetLookAt(unityView.transform);
                
                _vehicleAutoGasInput.Add(entity);

                var timerEntity = World.CreateEntity();
                _timer.Set(timerEntity, new Timer
                {
                    FinalTime = 3f,
                    Callback = () =>
                    {
                        _vehicleAutoGasInput.Remove(entity);
                    }
                });

                ref var collisionReceiver = ref _vehiclePlayerCollisionReceiverLink.Get(entity);
                collisionReceiver.value.onCollision = () =>
                {
                    _vehicleCheckCollision.Set(entity);
                };
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}