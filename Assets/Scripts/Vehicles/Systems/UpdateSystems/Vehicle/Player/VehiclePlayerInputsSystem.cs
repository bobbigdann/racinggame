﻿using DI.Attributes;
using Scellecs.Morpeh;
using Scripts.Settings.Settings;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Player;

namespace Vehicles.Systems.UpdateSystems.Vehicle.Player
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehiclePlayerInputsSystem : ISystem
    {
        [Inject] private VehicleSettings _vehicleSettings;
        [Inject] private Stash<VehicleInputs> _vehicleInputs;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleControlledByLocalPlayer>()
                .With<VehicleInputs>()
                .Without<VehicleAutoGasInput>()
                .Without<VehicleLocalInputsFromHud>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var inputs = ref _vehicleInputs.Get(entity);
                inputs.gasPressed = Input.GetKey(_vehicleSettings.gasKey);
                inputs.brakePressed = Input.GetKey(_vehicleSettings.brakeKey);
                inputs.handbrakePressed = Input.GetKey(_vehicleSettings.handbrakeKey);
                inputs.gearShiftUpPressed = Input.GetKey(_vehicleSettings.gearShiftUpKey);
                inputs.gearShiftDownPressed = Input.GetKey(_vehicleSettings.gearShiftDownKey);
                inputs.turnLeftPressed = Input.GetKey(_vehicleSettings.turnLeftKey);
                inputs.turnRightPressed = Input.GetKey(_vehicleSettings.turnRightKey);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}