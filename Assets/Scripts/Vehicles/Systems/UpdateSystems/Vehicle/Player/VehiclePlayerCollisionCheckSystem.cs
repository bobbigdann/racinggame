﻿using DI.Attributes;
using Gameplay.Components;
using GUI.Components.GameOverPanel;
using Level.Components;
using Scellecs.Morpeh;
using Scripts.Settings.Settings;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Player;

namespace Vehicles.Systems.UpdateSystems.Vehicle.Player
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehiclePlayerCollisionCheckSystem : ISystem
    {
        [Inject] private VehicleSettings _vehicleSettings;
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<VehicleCheckCollision> _vehicleCheckCollision;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleControlledByLocalPlayer>()
                .With<VehicleUnityView>()
                .With<VehicleCheckCollision>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                // ref var unityView = ref _vehicleUnityView.Get(entity);
                // var velocity = unityView.rigidbody.velocity;
                // float speed = velocity.z;
                // if (speed >= _vehicleSettings.speedGameOverThreshold) continue;
                
                World.CreateEntity().AddComponent<UiCreateGameOverPanel>();
                World.CreateEntity().AddComponent<CleanLevel>();
                World.CreateEntity().AddComponent<LoseGame>();
                _vehicleCheckCollision.Remove(entity);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}