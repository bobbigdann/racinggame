﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Gears;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleGearShiftingDelaySystem : ISystem
    {
        [Inject] private Stash<VehicleShiftingGear> _vehicleShiftingGear;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleShiftingGear>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var timer = ref _vehicleShiftingGear.Get(entity);
                timer.time -= deltaTime;
                if (timer.time <= 0f) _vehicleShiftingGear.Remove(entity);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}