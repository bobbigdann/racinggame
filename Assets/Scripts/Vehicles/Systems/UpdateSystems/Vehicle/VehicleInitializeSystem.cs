﻿using System;
using DI.Attributes;
using Scellecs.Morpeh;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Gears;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Vehicles.Serializable;
using Vehicles.Components.Wheels;
using Vehicles.Components.Wheels.Axles;
using Vehicles.Data;
using Vehicles.Providers.Wheels;
using Vehicles.Utils;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleInitializeSystem : ISystem
    {
        [Inject] private VehiclePhysicsParameters _vehiclePhysicsParameters;
        
        [Inject] private Stash<VehicleView> _vehicleView;
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<VehicleInitialize> _vehicleInitialize;
        [Inject] private Stash<VehicleWheels> _vehicleWheels;
        [Inject] private Stash<VehicleWheelsIdsUnsafe> _vehicleWheelsIdsUnsafe;
        [Inject] private Stash<VehicleRuntimeData> _vehicleRuntimeData;
        [Inject] private Stash<VehicleCurrentGear> _vehicleCurrentGear;
        [Inject] private Stash<VehicleSubStepData> _vehicleSubStepData;
        [Inject] private Stash<VehicleInputs> _vehicleInputs;

        [Inject] private Stash<WheelBrakeTorque> _wheelBrakeTorque;
        [Inject] private Stash<WheelSteerAngle> _wheelSteerAngle;
        [Inject] private Stash<WheelTorque> _wheelTorque;
        [Inject] private Stash<WheelFrontAxle> _wheelFrontAxle;
        [Inject] private Stash<WheelRearAxle> _wheelRearAxle;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleUnityView>()
                .With<VehicleInitialize>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var view = ref _vehicleView.Get(entity);
                ref var unityView = ref _vehicleUnityView.Get(entity);
                
                unityView.rigidbody.useGravity = false;
                unityView.rigidbody.collisionDetectionMode = _vehiclePhysicsParameters.collisionDetectionMode;
                unityView.rigidbody.centerOfMass = view.centerOfMass.localPosition;

                var wheelProviders = unityView.gameObject.GetComponentsInChildren<WheelViewProvider>(true);
                var allWheels = new Entity[wheelProviders.Length];
                for (int i = 0; i < allWheels.Length; i++)
                {
                    allWheels[i] = wheelProviders[i].Entity;
                }

                foreach (var wheel in allWheels)
                {
                    _wheelBrakeTorque.Add(wheel);
                }

                var steeringWheels = GetEntitiesByStash(allWheels, _wheelFrontAxle);
                foreach (var wheel in steeringWheels)
                {
                    _wheelSteerAngle.Add(wheel);
                }
                
                var handbrakeWheels = GetEntitiesByStash(allWheels, _wheelRearAxle);

                var wheelIds = new EntityId[allWheels.Length];
                for (int i = 0; i < wheelIds.Length; i++)
                {
                    wheelIds[i] = allWheels[i].ID;
                }

                var powerWheels = view.driveType switch
                {
                    VehicleDriveType.FrontWheelsDrive => steeringWheels,
                    VehicleDriveType.RearWheelsDrive => handbrakeWheels,
                    VehicleDriveType.AllWheelsDrive => allWheels,
                    _ => throw new ArgumentException(
                        $"Error getting power wheels on vehicle initialize: Unknown drive type {view.driveType}")
                };
                
                _vehicleWheels.Set(entity, new VehicleWheels
                {
                    all = allWheels,
                    steering = steeringWheels,
                    handbrake = handbrakeWheels,
                    power = powerWheels,
                    allEntityIds = wheelIds
                });
                
                _vehicleWheelsIdsUnsafe.Set(entity, new VehicleWheelsIdsUnsafe
                {
                    value = SimpleNativeArray<EntityId>.AsViewOf(wheelIds)
                });

                foreach (var wheel in powerWheels)
                {
                    _wheelTorque.Add(wheel);
                }
                
                _vehicleCurrentGear.Set(entity, new VehicleCurrentGear
                {
                    gearId = 0,
                });

                _vehicleRuntimeData.Add(entity);
                _vehicleSubStepData.Add(entity);
                _vehicleInputs.Add(entity);

                _vehicleInitialize.Remove(entity);
            }
        }

        private static Entity[] GetEntitiesByStash<T>(Entity[] entities, Stash<T> stash) where T : struct, IComponent
        {
            int count = 0;
            foreach (var entity in entities)
            {
                if (stash.Has(entity)) count++;
            }

            if (count == entities.Length) return entities;

            var result = new Entity[count];
            int index = 0;
            foreach (var entity in entities)
            {
                if (!stash.Has(entity)) continue;

                result[index] = entity;
                index++;
            }

            return result;
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}