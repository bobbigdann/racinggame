﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleAutoGasInputSystem : ISystem
    {
        [Inject] private Stash<VehicleInputs> _vehicleInputs;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleAutoGasInput>()
                .With<VehicleInputs>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var inputs = ref _vehicleInputs.Get(entity);
                inputs.gasPressed = true;
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}