﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleApplyStartVelocitySystem : ISystem
    {
        [Inject] private Stash<VehicleStartVelocity> _vehicleStartVelocity;
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleInitialize>()
                .With<VehicleUnityView>()
                .With<VehicleStartVelocity>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var unityView = ref _vehicleUnityView.Get(entity);
                ref var startVelocity = ref _vehicleStartVelocity.Get(entity);
                unityView.rigidbody.AddForce(unityView.transform.forward * startVelocity.value, ForceMode.Impulse);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}