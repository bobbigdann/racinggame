﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Vehicles.Serializable;
using Vehicles.Components.Wheels;

namespace Vehicles.Systems.UpdateSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleApplyBrakeTorqueSystem : ISystem
    {
        [Inject] private Stash<VehicleView> _vehicleView;
        [Inject] private Stash<VehicleInputs> _vehicleInputs;
        [Inject] private Stash<VehicleWheels> _vehicleWheels;
        [Inject] private Stash<VehicleRuntimeData> _vehicleRuntimeData;

        [Inject] private Stash<WheelBrakeTorque> _wheelBrakeTorque;
        [Inject] private Stash<WheelGroundHit> _wheelGroundHit;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleInputs>()
                .With<VehicleWheels>()
                .With<VehicleRuntimeData>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var view = ref _vehicleView.Get(entity);
                ref var inputs = ref _vehicleInputs.Get(entity);
                ref var wheels = ref _vehicleWheels.Get(entity);
                ref var runtimeData = ref _vehicleRuntimeData.Get(entity);
                
                if (inputs.handbrakePressed)
                {
                    foreach (var wheelEntity in wheels.handbrake)
                    {
                        ref var brake = ref _wheelBrakeTorque.Get(wheelEntity);
                        brake.value = view.engineData.handbrakePower;
                    }
                    
                    continue;
                }

                bool brakePressed = runtimeData.isGoingReverse ? inputs.gasPressed : inputs.brakePressed;
                if (brakePressed)
                {
                    foreach (var wheelEntity in wheels.all)
                    {
                        ref var hit = ref _wheelGroundHit.Get(wheelEntity, out bool exist);
                        if (!exist) continue;

                        ref var brake = ref _wheelBrakeTorque.Get(wheelEntity);
                        brake.value = view.helpers.absEnabled && Mathf.Abs(hit.forwardSlip) > view.helpers.absSlipThreshold
                            ? 0f
                            : view.engineData.brakePower;
                    }
                    
                    continue;
                }

                foreach (var wheelEntity in wheels.all)
                {
                    ref var brake = ref _wheelBrakeTorque.Get(wheelEntity);
                    brake.value = 0f;
                }
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}