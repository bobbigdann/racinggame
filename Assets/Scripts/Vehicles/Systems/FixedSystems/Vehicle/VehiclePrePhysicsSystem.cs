﻿using DI.Attributes;
using Scellecs.Morpeh;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using Unity.Mathematics;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Vehicles.Serializable;

namespace Vehicles.Systems.FixedSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehiclePrePhysicsSystem : IFixedSystem
    {
        [Inject] private VehiclePhysicsParameters _parameters;
        
        [Inject] private Stash<VehicleView> _vehicleView;
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<VehicleSubStepData> _vehicleSubStepData;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleUnityView>()
                .With<VehicleSubStepData>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_filter.IsEmpty()) return;
            
            Physics.SyncTransforms();
            
            foreach (var entity in _filter)
            {
                ref var view = ref _vehicleView.Get(entity);
                ref var unityView = ref _vehicleUnityView.Get(entity);
                ref var subStepData = ref _vehicleSubStepData.Get(entity);

                var localCenterOfMass = view.centerOfMass.localPosition;
                unityView.rigidbody.centerOfMass = localCenterOfMass;

                subStepData.localToWorld = unityView.transform.localToWorldMatrix;
                subStepData.position = unityView.rigidbody.position +
                                       subStepData.localToWorld.MultiplyVector(subStepData.centerOfMass);
                subStepData.rotation = unityView.rigidbody.rotation;
                subStepData.velocity = unityView.rigidbody.velocity;
                subStepData.angularVelocity = unityView.rigidbody.angularVelocity;
                subStepData.drag = unityView.rigidbody.drag;
                subStepData.angularDrag = unityView.rigidbody.angularDrag;
                subStepData.mass = unityView.rigidbody.mass;
                subStepData.inertia = unityView.rigidbody.inertiaTensor;
                subStepData.centerOfMass = localCenterOfMass;

                subStepData.stepsCount = _parameters.physicsStepsCount;

#if UNITY_EDITOR && VEHICLES_DEBUG_ENABLED
                float length = 0.5f;
                Debug.DrawLine(subStepData.position, subStepData.position + length * math.mul(subStepData.rotation, math.forward()), Color.blue);
                Debug.DrawLine(subStepData.position, subStepData.position + length * math.mul(subStepData.rotation, math.right()), Color.red);
                Debug.DrawLine(subStepData.position, subStepData.position + length * math.mul(subStepData.rotation, math.up()), Color.green);
#endif
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}