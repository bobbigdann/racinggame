﻿using DI.Attributes;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Native;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using Unity.Jobs;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Vehicles.Serializable;
using Vehicles.Components.Wheels;
using Vehicles.Systems.FixedSystems.Vehicle.Jobs;

namespace Vehicles.Systems.FixedSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehiclePhysicsSystem : IFixedSystem
    {
        [Inject] private VehiclePhysicsParameters _parameters;
        
        [Inject] private Stash<VehicleSubStepData> _vehicleSubStepData;
        [Inject] private Stash<VehicleWheelsIdsUnsafe> _vehicleWheelsIdsUnsafe;

        [Inject] private Stash<WheelGroundHit> _wheelGroundHit;
        [Inject] private Stash<WheelTorque> _wheelTorque;
        [Inject] private Stash<WheelBrakeTorque> _wheelBrakeTorque;
        [Inject] private Stash<WheelRpm> _wheelRpm;
        [Inject] private Stash<WheelSmoothTotalSlip> _wheelSmoothTotalSlip;
        [Inject] private Stash<WheelSuspension> _wheelSuspension;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleWheelsIdsUnsafe>()
                .With<VehicleSubStepData>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_filter.IsEmpty()) return;

            var nativeFilter = _filter.AsNative();
            var handle = new VehicleProcessWheelsJob
            {
                entities = nativeFilter,

                vehicleSubStepData = _vehicleSubStepData.AsNative(),
                vehicleWheelsIdsUnsafe = _vehicleWheelsIdsUnsafe.AsNative(),

                wheelGroundHit = _wheelGroundHit.AsNative(),
                wheelTorque = _wheelTorque.AsNative(),
                wheelBrakeTorque = _wheelBrakeTorque.AsNative(),
                wheelRpm = _wheelRpm.AsNative(),
                wheelSmoothTotalSlip = _wheelSmoothTotalSlip.AsNative(),
                wheelSuspension = _wheelSuspension.AsNative(),

                deltaTime = deltaTime,
                gravity = _parameters.gravity,
            }.ScheduleParallel(nativeFilter.length, 1, default);
            
            handle.Complete();
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}