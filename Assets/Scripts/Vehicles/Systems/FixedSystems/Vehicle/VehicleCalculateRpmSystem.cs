﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Gears;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Vehicles.Serializable;
using Vehicles.Components.Wheels;

namespace Vehicles.Systems.FixedSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleCalculateRpmSystem : IFixedSystem
    {
        [Inject] private Stash<VehicleView> _vehicleView;
        [Inject] private Stash<VehicleWheels> _vehicleWheels;
        [Inject] private Stash<VehicleRuntimeData> _vehicleRuntimeData;
        [Inject] private Stash<VehicleCurrentGear> _vehicleCurrentGear;

        [Inject] private Stash<WheelRpm> _wheelRpm;
        [Inject] private Stash<WheelGroundHit> _wheelGroundHit;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleCurrentGear>()
                .With<VehicleRuntimeData>()
                .With<VehicleWheels>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var currentGear = ref _vehicleCurrentGear.Get(entity);
                ref var view = ref _vehicleView.Get(entity);
                ref var runtimeData = ref _vehicleRuntimeData.Get(entity);
                ref var wheels = ref _vehicleWheels.Get(entity);

                float rpm = 0f;
                int groundedWheels = 0;
                foreach (var wheel in wheels.power)
                {
                    ref var hit = ref _wheelGroundHit.Get(wheel);
                    if (!hit.grounded) continue;
                    
                    ref var wheelRpm = ref _wheelRpm.Get(wheel);
                    rpm += wheelRpm.value;
                    groundedWheels++;
                }

                rpm /= groundedWheels;

                float gearValue = runtimeData.isGoingReverse
                    ? view.transmission.reverseGear
                    : view.transmission.gears[currentGear.gearId];

                rpm *= gearValue;
                runtimeData.rpm = Mathf.RoundToInt(rpm);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}