﻿using Constants;
using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Serializable;

namespace Vehicles.Systems.FixedSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleInfoValuesSystem : IFixedSystem
    {
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<VehicleRuntimeData> _vehicleRuntimeData;
        [Inject] private Stash<VehicleView> _vehicleView;
        [Inject] private Stash<VehicleInputs> _vehicleInputs;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleUnityView>()
                .With<VehicleRuntimeData>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var runtimeData = ref _vehicleRuntimeData.Get(entity);
                ref var unityView = ref _vehicleUnityView.Get(entity);
                ref var inputs = ref _vehicleInputs.Get(entity);

                var velocity = unityView.rigidbody.velocity;
                runtimeData.speed = velocity.magnitude * VehicleConstants.VelocityToSpeedMultiplier;

                if (runtimeData.speed < VehicleConstants.MinSpeedForBrake)
                {
                    runtimeData.isGoingReverse = inputs.brakePressed;
                    continue;
                }
                
                var inverseDirection = unityView.transform.InverseTransformDirection(velocity);
                runtimeData.isGoingReverse = inverseDirection.z < 0;
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}