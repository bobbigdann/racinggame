﻿using DI.Attributes;
using Scellecs.Morpeh;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;
using Unity.Mathematics;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Vehicles.Serializable;

namespace Vehicles.Systems.FixedSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehiclePostPhysicsSystem : IFixedSystem
    {
        [Inject] private VehiclePhysicsParameters _parameters;
        
        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<VehicleSubStepData> _vehicleSubStepData;
        [Inject] private Stash<VehicleInputs> _vehicleInputs;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleUnityView>()
                .With<VehicleSubStepData>()
                .With<VehicleInputs>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var unityView = ref _vehicleUnityView.Get(entity);
                ref var subStepData = ref _vehicleSubStepData.Get(entity);
                ref var inputs = ref _vehicleInputs.Get(entity);

                bool hasInput = inputs.gasPressed || inputs.brakePressed;
                bool hasVelocity = math.lengthsq(subStepData.velocity) > 0.0001f;
                bool hasAngularVelocity = math.lengthsq(subStepData.angularVelocity) > 0.0001f;

                if (!hasInput && !hasVelocity && !hasAngularVelocity)
                {
                    unityView.rigidbody.Sleep();
                }
                else
                {
                    var force = subStepData.totalLinearImpulse +
                                  _parameters.gravity * subStepData.mass * deltaTime;
                    unityView.rigidbody.AddForce(force, ForceMode.Impulse);
                    unityView.rigidbody.AddTorque(subStepData.totalAngularImpulse, ForceMode.Impulse);

#if UNITY_EDITOR && VEHICLES_DEBUG_ENABLED
                    Debug.DrawRay(unityView.rigidbody.position, force, Color.red, deltaTime);
#endif
                }
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}