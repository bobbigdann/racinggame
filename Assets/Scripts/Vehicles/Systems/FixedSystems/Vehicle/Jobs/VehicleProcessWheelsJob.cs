﻿using System.Runtime.CompilerServices;
using Scellecs.Morpeh.Native;
using Unity.Burst;
using Unity.Burst.CompilerServices;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Wheels;
using Vehicles.Utils;

namespace Vehicles.Systems.FixedSystems.Vehicle.Jobs
{
    [BurstCompile(FloatPrecision.Standard, FloatMode.Fast, CompileSynchronously = true)]
    public struct VehicleProcessWheelsJob : IJobFor
    {
        [ReadOnly] public NativeFilter entities;

        public NativeStash<VehicleSubStepData> vehicleSubStepData;
        [ReadOnly] public NativeStash<VehicleWheelsIdsUnsafe> vehicleWheelsIdsUnsafe;

        public NativeStash<WheelGroundHit> wheelGroundHit;
        [ReadOnly] public NativeStash<WheelTorque> wheelTorque;
        [ReadOnly] public NativeStash<WheelBrakeTorque> wheelBrakeTorque;
        [WriteOnly] public NativeStash<WheelRpm> wheelRpm;
        public NativeStash<WheelSmoothTotalSlip> wheelSmoothTotalSlip;
        public NativeStash<WheelSuspension> wheelSuspension;

        [ReadOnly] public float deltaTime;
        [ReadOnly] public float3 gravity;

        public void Execute(int index)
        {
            var entityId = entities[index];
            ref var subStepData = ref vehicleSubStepData.Get(entityId);
            ref var wheelIds = ref vehicleWheelsIdsUnsafe.Get(entityId);

            int stepsCount = subStepData.stepsCount;
            float stepDeltaTime = deltaTime / stepsCount;

            subStepData.invertedMass = 1f / subStepData.mass;
            
            subStepData.linearImpulse = float3.zero;
            subStepData.totalLinearImpulse = float3.zero;
            subStepData.angularImpulse = float3.zero;
            subStepData.totalAngularImpulse = float3.zero;

            subStepData.transformForward = math.mul(subStepData.rotation, math.forward());
            subStepData.transformRight = math.mul(subStepData.rotation, math.right());
            subStepData.transformUp = math.mul(subStepData.rotation, math.up());

            subStepData.stepInitPosition = subStepData.position;
            subStepData.stepInitRotation = subStepData.rotation;

            subStepData.invertedInertia.x = subStepData.inertia.x == 0 ? 1e-8f : 1f / subStepData.inertia.x;
            subStepData.invertedInertia.y = subStepData.inertia.y == 0 ? 1e-8f : 1f / subStepData.inertia.y;
            subStepData.invertedInertia.z = subStepData.inertia.z == 0 ? 1e-8f : 1f / subStepData.inertia.z;

            subStepData.worldInvertedInertia = math.abs(subStepData.localToWorld.MultiplyVector(subStepData.invertedInertia));

            for (int i = 0, length = wheelIds.value.Length; i < length; i++)
            {
                ref var wheelId = ref wheelIds.value.GetAsRef(i);
                ref var hit = ref wheelGroundHit.Get(in wheelId);
                ref var suspension = ref wheelSuspension.Get(in wheelId);
                
                if (Hint.Likely(hit.grounded))
                {
                    suspension.targetPoint = hit.wheelPosition;
                    suspension.length = math.clamp(-hit.worldToLocal.MultiplyPoint3x4(suspension.targetPoint).y,
                        0f,
                        hit.suspensionMaxLength);
                }
                else
                {
                    suspension.length = math.lerp(suspension.length, hit.suspensionMaxLength, deltaTime * 6f);
                    suspension.damperForce = 0;
                }

                suspension.velocity = (suspension.length - suspension.prevLength) / deltaTime;
                suspension.compressionPercent = (hit.suspensionMaxLength - suspension.length) / hit.suspensionMaxLength;
                suspension.force = hit.grounded
                    ? hit.suspensionMaxForce * math.clamp(suspension.compressionPercent, 0f, 1f)
                    : 0f;

                hit.suspensionLoad = 0f;

                if (Hint.Likely(hit.grounded))
                {
                    float springVelocityAbs = math.abs(suspension.velocity);
                    float reducedVelocity = math.clamp(springVelocityAbs / 10f, 0f, 1f);
                    suspension.damperForce = suspension.velocity < 0
                        ? hit.suspensionBumpForce * reducedVelocity
                        : -hit.suspensionReboundForce * reducedVelocity;
                    suspension.damperForce *= 10f;

                    hit.suspensionLoad = math.clamp(suspension.force + suspension.damperForce, 0f, math.INFINITY);
                }

                suspension.prevLength = suspension.length;
                
                if (Hint.Unlikely(!hit.grounded)) continue;

                var surfaceNormal = hit.normal;
                if (hit.forwardSpeed < 2f && hit.sidewaysSpeed > -2f)
                {
                    var altNormal = math.normalize(hit.wheelPosition - hit.point);
                    float surfaceAltDot = math.dot(surfaceNormal, altNormal);
                    if (surfaceAltDot < 0.99f)
                    {
                        surfaceNormal = altNormal;

                        var cross = math.cross(hit.up, surfaceNormal);
                        float crossMagnitude = math.length(cross);
                        float directionSign = math.dot(cross, hit.sidewaysDir) >= 0f ? 1f : -1f;
                        var bumpForce = hit.forwardDir * (directionSign * hit.suspensionLoad * crossMagnitude);
                        AddForceAtPosition(ref subStepData, bumpForce * stepsCount, hit.point);
                    }
                }

                var suspensionForceVector = hit.suspensionLoad * surfaceNormal;
                AddForceAtPosition(ref subStepData, suspensionForceVector * stepsCount, hit.originPosition);
                hit.totalForce += suspensionForceVector;
            }

            for (int step = 0; step < stepsCount; step++)
            {
                for (int i = 0, length = wheelIds.value.Length; i < length; i++)
                {
                    ref var wheelId = ref wheelIds.value.GetAsRef(i);
                    ref var hit = ref wheelGroundHit.Get(wheelId);
                    ref var torque = ref wheelTorque.Get(wheelId, out bool torqueExist);
                    ref var brakeTorque = ref wheelBrakeTorque.Get(wheelId, out bool brakeExist);
                    ref var rpm = ref wheelRpm.Get(wheelId);

                    if (Hint.Likely(step > 0))
                    {
                        hit.forward = math.mul(subStepData.stepRotationDelta, hit.forward);
                        hit.right = math.mul(subStepData.stepRotationDelta, hit.right);
                        hit.up = math.mul(subStepData.stepRotationDelta, hit.up);
                        
                        hit.forwardDir = math.mul(subStepData.stepRotationDelta, hit.forwardDir);
                        hit.sidewaysDir = math.mul(subStepData.stepRotationDelta, hit.sidewaysDir);
                        
                        hit.originRotation = math.mul(subStepData.stepRotationDelta, hit.originRotation);

                        hit.wheelPosition += subStepData.stepPositionDelta;
                        hit.originPosition += subStepData.stepPositionDelta;
                        hit.point += subStepData.stepPositionDelta;
                    }

                    var contactVelocity = GetPointVelocity(ref subStepData, hit.wheelPosition - hit.up * hit.radius);

                    if (Hint.Likely(hit.grounded))
                    {
                        hit.forwardSpeed = math.dot(contactVelocity, hit.forwardDir);
                        hit.sidewaysSpeed = math.dot(contactVelocity, hit.sidewaysDir);
                    }
                    else
                    {
                        hit.forwardSpeed = 0f;
                        hit.sidewaysSpeed = 0f;
                    }

                    float torqueValue = torqueExist ? torque.value : 0f;
                    float brakeValue = brakeExist ? brakeTorque.value : 0f;
                    
                    float inertia = 0.5f * hit.mass * hit.radius * hit.radius;
                    
                    CalculateFriction(
                        stepDeltaTime,
                        in hit.radius,
                        inertia,
                        in hit.curveValues,
                        in hit.peakSlip,
                        torqueValue,
                        brakeValue + torqueValue * hit.relativeDragTorque + hit.dragTorque,
                        in hit.forwardSpeed,
                        in hit.sidewaysSpeed,
                        in hit.forwardSlipCoefficient,
                        in hit.sidewaysSlipCoefficient,
                        in hit.forwardForceCoefficient,
                        in hit.sidewaysForceCoefficient,
                        in hit.frictionCircleStrength,
                        in hit.frictionCircleShape,
                        ref hit.angularVelocity,
                        hit.suspensionLoad,
                        
                        out hit.forwardSlip,
                        out hit.sidewaysSlip,
                        out hit.forwardForce,
                        out hit.sidewaysForce);

                    if (Hint.Likely(hit.grounded))
                    {
                        var surfaceForce = hit.sidewaysDir * hit.sidewaysForce + hit.forwardDir * hit.forwardForce;
                        AddForceAtPosition(ref subStepData, in surfaceForce, hit.point + subStepData.stepPositionDelta);
                        hit.totalForce += surfaceForce / stepsCount;
                    }

                    rpm.value = hit.angularVelocity * 9.55f;
                }

                subStepData.velocity += gravity * stepDeltaTime;

                float invertedMassDelta = subStepData.invertedMass * stepDeltaTime;
                subStepData.velocity += subStepData.linearImpulse * invertedMassDelta;

                subStepData.angularVelocity.x +=
                    subStepData.angularImpulse.x * subStepData.worldInvertedInertia.x * stepDeltaTime;
                subStepData.angularVelocity.y +=
                    subStepData.angularImpulse.y * subStepData.worldInvertedInertia.y * stepDeltaTime;
                subStepData.angularVelocity.z +=
                    subStepData.angularImpulse.z * subStepData.worldInvertedInertia.z * stepDeltaTime;

                float dragMultiplier = math.max(1.0f - subStepData.drag * stepDeltaTime, 0f);
                subStepData.velocity *= dragMultiplier;

                float dragDelta = subStepData.angularDrag * stepDeltaTime;
                subStepData.angularVelocity -= subStepData.angularVelocity * dragDelta;

                var eulerRotationDelta = subStepData.angularVelocity * stepDeltaTime;
                subStepData.rotation = math.mul(quaternion.Euler(eulerRotationDelta), subStepData.rotation);

                subStepData.position += subStepData.velocity * stepDeltaTime;

                subStepData.totalLinearImpulse += subStepData.linearImpulse * stepDeltaTime;
                subStepData.totalAngularImpulse += subStepData.angularImpulse * stepDeltaTime;
                
                subStepData.linearImpulse = float3.zero;
                subStepData.angularImpulse = float3.zero;

                subStepData.stepPositionDelta = subStepData.position - subStepData.stepInitPosition;
                subStepData.stepRotationDelta =
                    math.normalize(math.mul(subStepData.rotation, math.inverse(subStepData.stepInitRotation)));
                
                subStepData.localToWorld *= Matrix4x4.TRS(subStepData.stepPositionDelta, subStepData.stepRotationDelta, Vector3.one);

                subStepData.transformForward = math.mul(subStepData.stepRotationDelta, subStepData.transformForward);
                subStepData.transformRight = math.mul(subStepData.stepRotationDelta, subStepData.transformRight);
                subStepData.transformUp = math.mul(subStepData.stepRotationDelta, subStepData.transformUp);

                subStepData.stepInitPosition = subStepData.position;
                subStepData.stepInitRotation = subStepData.rotation;
            }

            for (int i = 0, length = wheelIds.value.Length; i < length; i++)
            {
                ref var wheelId = ref wheelIds.value.GetAsRef(i);
                ref var hit = ref wheelGroundHit.Get(in wheelId);
                ref var slip = ref wheelSmoothTotalSlip.Get(in wheelId);

                slip.value = Mathf.Lerp(slip.value, 
                    Mathf.Abs(hit.forwardSlip) + Mathf.Abs(hit.sidewaysSlip),
                    5f * deltaTime);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void AddForceAtPosition(ref VehicleSubStepData subStepData, in float3 force, in float3 position)
        {
            subStepData.linearImpulse += force;
            subStepData.angularImpulse += math.cross(position - subStepData.position, force);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static float3 GetPointVelocity(ref VehicleSubStepData subStepData, in float3 worldPoint)
        {
            var cross = math.cross(subStepData.angularVelocity, worldPoint - subStepData.position);
            return subStepData.velocity + cross;
        }

        private static void CalculateFriction(
            in float deltaTime,
            in float radius,
            float inertia,
            in SimpleNativeArray<float> curveSpan,
            in float peakSlip,
            float torque,
            float brake,
            in float forwardSpeed,
            in float sidewaysSpeed,
            in float forwardSlipCoefficient,
            in float sidewaysSlipCoefficient,
            in float forwardForceCoefficient,
            in float sidewaysForceCoefficient,
            in float frictionCircleStrength,
            in float frictionCircleShape,
            ref float angularVelocity,
            float suspensionLoad,
            out float forwardSlip,
            out float sidewaysSlip,
            out float forwardForce,
            out float sidewaysForce)
        {
            inertia = math.max(inertia, 0.0001f);

            float loadX = 14000f * (1f - math.exp(-0.00012f * suspensionLoad));
            float loadY = 24000f * (1f - math.exp(-0.0001f * suspensionLoad));

            float absForwardSpeed = math.abs(forwardSpeed);

            if (Hint.Likely(absForwardSpeed >= 0.01f))
            {
                forwardSlip = (forwardSpeed - angularVelocity * radius) / absForwardSpeed;
            }
            else
            {
                forwardSlip = (forwardSpeed - angularVelocity * radius) / 2.2f;
            }

            forwardSlip *= forwardSlipCoefficient;
            forwardSlip = math.clamp(forwardSlip, -1f, 1f);

            angularVelocity += torque / inertia * deltaTime;

            brake *= -math.sign(angularVelocity);
            float tbCap = math.abs(angularVelocity) * inertia / deltaTime;
            float tbr = math.abs(brake) - math.abs(tbCap);
            tbr = math.clamp(tbr, 0f, math.INFINITY);
            brake = math.clamp(brake, -tbCap, tbCap);
            angularVelocity += brake / inertia * deltaTime;

            int forwardCurveItem = (int)(math.clamp(math.abs(forwardSlip), 0f, 1f) * curveSpan.Length - 1);
            float maxTorque = curveSpan[forwardCurveItem] * loadX * radius;
            float errorTorque = (angularVelocity - forwardSpeed / radius) * inertia / deltaTime;
            float surfaceTorque = math.clamp(errorTorque, -maxTorque, maxTorque);

            angularVelocity -= surfaceTorque / inertia * deltaTime;
            forwardForce = surfaceTorque / radius;
            
            tbr *= -math.sign(angularVelocity);
            tbCap = math.abs(angularVelocity) * inertia / deltaTime;
            tbr = math.clamp(tbr, -tbCap, tbCap);
            angularVelocity += tbr / inertia * deltaTime;

            if (Hint.Likely(absForwardSpeed > 0.3f))
            {
                sidewaysSlip = math.degrees(math.atan(sidewaysSpeed / absForwardSpeed));
                sidewaysSlip /= 120f;
            }
            else
            {
                sidewaysSlip = sidewaysSpeed * (0.002f / deltaTime);
            }

            sidewaysSlip *= sidewaysSlipCoefficient;
            sidewaysSlip = math.clamp(sidewaysSlip, -1f, 1f);

            int sidewaysCurveItem = (int)(math.clamp(math.abs(sidewaysSlip), 0f, 1f) * (curveSpan.Length - 1));
            sidewaysForce = -math.sign(sidewaysSlip) * curveSpan[sidewaysCurveItem] * loadY;

            if (Hint.Unlikely(loadX < 0.001f)) forwardSlip = 0f;
            if (Hint.Unlikely(loadY < 0.001f)) sidewaysSlip = 0f;

            if (Hint.Likely(frictionCircleStrength > 0))
            {
                if (Hint.Likely(math.abs(forwardSpeed) > 0.1f || math.abs(sidewaysSpeed) > 0.1f
                                                              || math.abs(angularVelocity) > .20f))
                {
                    float s = forwardSlip / peakSlip;
                    float a = sidewaysSlip / peakSlip;
                    float rho = s * s + a * a;

                    if (Hint.Unlikely(rho > 1))
                    {
                        float beta = math.atan2(sidewaysSlip, forwardSlip * frictionCircleShape);
                        math.sincos(beta, out float sinBeta, out float cosBeta);
                        float f = loadX * cosBeta * cosBeta + loadY * sinBeta * sinBeta;

                        float inverceSlipCircle = 1f - frictionCircleStrength;
                        forwardForce = inverceSlipCircle * forwardForce + frictionCircleStrength * (-f * cosBeta);
                        sidewaysForce = inverceSlipCircle * sidewaysForce + frictionCircleStrength * (-f * sinBeta);
                    }
                }
            }

            forwardForce *= forwardForceCoefficient;
            sidewaysForce *= sidewaysForceCoefficient;
        }
    }
}