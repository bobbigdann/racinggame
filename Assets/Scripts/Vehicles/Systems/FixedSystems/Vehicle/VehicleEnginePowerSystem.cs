﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Gears;
using Vehicles.Components.Vehicles.Physics;
using Vehicles.Components.Vehicles.Serializable;
using Vehicles.Components.Wheels;

namespace Vehicles.Systems.FixedSystems.Vehicle
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class VehicleEnginePowerSystem : IFixedSystem
    {
        [Inject] private Stash<VehicleView> _vehicleView;
        [Inject] private Stash<VehicleInputs> _vehicleInputs;
        [Inject] private Stash<VehicleWheels> _vehicleWheels;
        [Inject] private Stash<VehicleRuntimeData> _vehicleRuntimeData;
        [Inject] private Stash<VehicleCurrentGear> _vehicleCurrentGear;
        [Inject] private Stash<VehicleShiftingGear> _vehicleShiftingGear;

        [Inject] private Stash<WheelTorque> _wheelTorque;
        [Inject] private Stash<WheelGroundHit> _wheelGroundHit;

        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<VehicleView>()
                .With<VehicleCurrentGear>()
                .With<VehicleRuntimeData>()
                .With<VehicleInputs>()
                .With<VehicleWheels>()
                .Without<VehiclePaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var view = ref _vehicleView.Get(entity);
                ref var inputs = ref _vehicleInputs.Get(entity);
                ref var wheels = ref _vehicleWheels.Get(entity);
                ref var runtimeData = ref _vehicleRuntimeData.Get(entity);
                ref var currentGear = ref _vehicleCurrentGear.Get(entity);

                float torque = 0f;
                if (!_vehicleShiftingGear.Has(entity))
                {
                    if (runtimeData.isGoingReverse && inputs.brakePressed &&
                        runtimeData.speed < view.engineData.maxSpeedReverse)
                    {
                        torque = -view.engineData.reverseTorque;
                    }
                    else if (!runtimeData.isGoingReverse && inputs.gasPressed &&
                             runtimeData.speed < view.engineData.maxSpeedForward)
                    {
                        float rpmT = (runtimeData.rpm - view.engineData.minRpm) /
                                     (view.engineData.maxRpm - view.engineData.minRpm);
                        torque = view.engineData.maxTorque * view.engineData.torqueCurve.Evaluate(Mathf.Clamp01(rpmT));
                    }

                    float gearValue = runtimeData.isGoingReverse
                        ? view.transmission.reverseGear
                        : view.transmission.gears[currentGear.gearId];

                    torque /= gearValue;
                }

                float torquePerWheel = torque / wheels.power.Length;
                
                runtimeData.torque = torque;
                runtimeData.torquePerWheel = torquePerWheel;
                
                foreach (var wheelEntity in wheels.power)
                {
                    ref var wheelTorque = ref _wheelTorque.Get(wheelEntity, out bool exist);
                    if (!exist) continue;

                    ref var hit = ref _wheelGroundHit.Get(wheelEntity);
                    wheelTorque.value = view.helpers.tractionEnabled && Mathf.Abs(hit.forwardSlip) > view.helpers.tractionSlipThreshold
                        ? 0f
                        : torquePerWheel;
                }
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}