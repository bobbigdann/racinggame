﻿using Constants;
using DI.Attributes;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Native;
using Unity.Collections;
using Unity.IL2CPP.CompilerServices;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Vehicles.Components.Wheels;
using Vehicles.Components.Wheels.Serializable;
using Vehicles.Data;
using Vehicles.Systems.FixedSystems.Wheels.Jobs;
using Vehicles.Utils;

namespace Vehicles.Systems.FixedSystems.Wheels
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class WheelPrePhysicsSystem : IFixedSystem
    {
        [Inject] private SurfaceParametersMapper _surfaceParametersMapper;
        
        [Inject] private Stash<WheelView> _wheelView;
        [Inject] private Stash<WheelGroundHit> _wheelGroundHit;
        [Inject] private Stash<WheelSteerAngle> _wheelSteerAngle;
        [Inject] private Stash<WheelGroundParams> _wheelGroundParams;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<WheelView>()
                .With<WheelGroundHit>()
                .With<WheelGroundParams>()
                .With<WheelVehicleLink>()
                .Without<WheelPaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_filter.IsEmpty()) return;

            bool hitTriggers = Physics.queriesHitTriggers;
            bool hitBackfaces = Physics.queriesHitBackfaces;

            Physics.queriesHitTriggers = false;
            Physics.queriesHitBackfaces = false;

            int entitiesCount = 0;
            
            foreach (var entity in _filter)
            {
                ref var view = ref _wheelView.Get(entity);
                ref var hit = ref _wheelGroundHit.Get(entity);

                var scale = view.transform.lossyScale;

                hit.originPosition = view.transform.position;
                hit.originRotation = view.transform.rotation;

                hit.radius = view.data.radius * scale.y;

                hit.suspensionMaxLength = view.data.maxLength * scale.y;
                hit.suspensionMaxForce = view.data.maxForce;
                hit.suspensionBumpForce = view.data.bumpForce;
                hit.suspensionReboundForce = view.data.reboundForce;

                hit.maxDistance = hit.suspensionMaxLength + hit.radius;

                hit.frictionCircleStrength = view.data.frictionCircleStrength;
                hit.frictionCircleShape = view.data.frictionCircleShape;

                hit.mass = view.data.mass;
                hit.worldToLocal = view.transform.worldToLocalMatrix;

                entitiesCount++;
            }

            var nativeFilter = _filter.AsNative(entitiesCount);
            var nativeWheelHit = _wheelGroundHit.AsNative();

            var raycastCommands = new NativeArray<RaycastCommand>(nativeFilter.length, Allocator.TempJob);
            var raycastHits = new NativeArray<RaycastHit>(nativeFilter.length, Allocator.TempJob,
                NativeArrayOptions.UninitializedMemory);

            var handle = new WheelPrepareInitialDataJob
            {
                entities = nativeFilter,
                wheelGroundHit = nativeWheelHit,
                wheelSteerAngle = _wheelSteerAngle.AsNative(),
                raycastCommands = raycastCommands,
                layerMask = VehicleConstants.WheelRaycastLayerMask,
            }.ScheduleParallel(nativeFilter.length, 16, default);

            handle = RaycastCommand.ScheduleBatch(raycastCommands, raycastHits, 1, handle);

            handle = new WheelProcessRaycastResultsJob
            {
                entities = nativeFilter,
                wheelGroundHit = nativeWheelHit,
                raycastHits = raycastHits,
            }.ScheduleParallel(nativeFilter.length, 16, handle);
            
            handle.Complete();

            int i = 0;
            foreach (var entity in _filter)
            {
                ref var view = ref _wheelView.Get(entity);
                ref var hit = ref _wheelGroundHit.Get(entity);
                ref var groundParams = ref _wheelGroundParams.Get(entity);

                hit.forwardForceCoefficient = view.data.forwardForceCoefficient;
                hit.forwardSlipCoefficient = view.data.forwardSlipCoefficient;
                hit.sidewaysForceCoefficient = view.data.sidewaysForceCoefficient;
                hit.sidewaysSlipCoefficient = view.data.sidewaysSlipCoefficient;

                groundParams.previous = groundParams.current;

                var raycastHit = raycastHits[i];

                groundParams.current = hit.grounded
                    ? GetSurfaceParameters(raycastHit.collider)
                    : _surfaceParametersMapper.surfaceParameters[0];

                hit.curveValues = SimpleNativeArray<float>.AsViewOf(groundParams.current.curveValues);
                hit.peakSlip = groundParams.current.peakSlip;
                hit.dragTorque = groundParams.current.dragTorque;
                hit.relativeDragTorque = groundParams.current.relativeDragTorque;

                i++;

#if UNITY_EDITOR && VEHICLES_DEBUG_ENABLED
                Debug.DrawRay(hit.originPosition, hit.forwardDir, Color.blue, deltaTime);
                Debug.DrawRay(hit.originPosition, -hit.sidewaysDir, Color.red, deltaTime);

                if (raycastHit.colliderInstanceID != 0)
                {
                    Debug.DrawRay(hit.originPosition, raycastHit.point - (Vector3) hit.originPosition, Color.green, deltaTime);
                }
                else
                {
                    Debug.DrawRay(hit.originPosition, -hit.up * (view.data.maxLength + view.data.radius), Color.magenta, deltaTime);
                }
                
                Debug.DrawRay(hit.originPosition - hit.up * (hit.distance - view.data.radius), math.normalize(hit.forwardDir) * view.data.radius, Color.yellow, deltaTime);
                Debug.DrawRay(hit.originPosition - hit.up * (hit.distance - view.data.radius), -math.normalize(hit.forwardDir) * view.data.radius, Color.yellow, deltaTime);
#endif
            }

            raycastCommands.Dispose();
            raycastHits.Dispose();

            Physics.queriesHitTriggers = hitTriggers;
            Physics.queriesHitBackfaces = hitBackfaces;
        }

        private PacejkaSurfaceParameters GetSurfaceParameters(Collider collider)
        {
            if (ReferenceEquals(collider, null))
                return _surfaceParametersMapper.surfaceParameters[0];

            var sharedMaterial = collider.sharedMaterial;
            if (ReferenceEquals(sharedMaterial, null))
                return _surfaceParametersMapper.surfaceParameters[0];

            return _surfaceParametersMapper.GetByPhysicsMaterial(in sharedMaterial);
        }

        public void Dispose()
        {
            _filter = null;

            _wheelView = null;
            _wheelGroundHit = null;
            _wheelSteerAngle = null;
            _wheelGroundParams = null;
        }
    }
}