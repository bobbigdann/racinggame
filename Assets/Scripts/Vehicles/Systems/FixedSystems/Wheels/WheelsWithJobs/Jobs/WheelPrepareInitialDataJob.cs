﻿using Scellecs.Morpeh.Native;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Vehicles.Components.Wheels;

namespace Vehicles.Systems.FixedSystems.Wheels.Jobs
{
    [BurstCompile(CompileSynchronously = true)]
    public struct WheelPrepareInitialDataJob : IJobFor
    {
        [ReadOnly] public int layerMask;
        [ReadOnly] public NativeFilter entities;
        
        public NativeStash<WheelGroundHit> wheelGroundHit;
        [ReadOnly] public NativeStash<WheelSteerAngle> wheelSteerAngle;
        
        public NativeArray<RaycastCommand> raycastCommands;

        public void Execute(int index)
        {
            var entityId = entities[index];
            ref var hit = ref wheelGroundHit.Get(entityId);
            ref var steerAngle = ref wheelSteerAngle.Get(entityId, out bool steerAngleExist);
            float steerValue = steerAngleExist ? steerAngle.value : 0f;

            var up = math.mul(hit.originRotation, math.up());
            var steerRotation = quaternion.AxisAngle(up, math.radians(steerValue));
            hit.up = math.mul(steerRotation, up);

            hit.forward = math.mul(steerRotation, math.mul(hit.originRotation, math.forward()));
            hit.right = math.mul(steerRotation, math.mul(hit.originRotation, math.right()));

            hit.originRotation = math.mul(steerRotation, hit.originRotation);

            raycastCommands[index] = new RaycastCommand(
                hit.originPosition,
                -hit.up,
                hit.maxDistance,
                layerMask);
        }
    }
}