﻿using Scellecs.Morpeh.Native;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Vehicles.Components.Wheels;

namespace Vehicles.Systems.FixedSystems.Wheels.Jobs
{
    [BurstCompile(CompileSynchronously = true)]
    public struct WheelProcessRaycastResultsJob : IJobFor
    {
        [ReadOnly] public NativeFilter entities;
        
        public NativeStash<WheelGroundHit> wheelGroundHit;

        [ReadOnly] public NativeArray<RaycastHit> raycastHits;

        public void Execute(int index)
        {
            var entityId = entities[index];
            ref var hit = ref wheelGroundHit.Get(entityId);
            var raycastHit = raycastHits[index];

            bool grounded = raycastHit.colliderInstanceID != 0;
            float distanceToSuspensionExpansion = grounded ? raycastHit.distance : hit.maxDistance;
            hit.wheelPosition = hit.originPosition - hit.up * (distanceToSuspensionExpansion - hit.radius);
            hit.grounded = grounded;
            hit.forwardDir = math.normalize(math.cross(raycastHit.normal, -hit.right));
            hit.sidewaysDir = math.mul(quaternion.AxisAngle(raycastHit.normal, math.radians(90f)), hit.forwardDir);
            hit.distance = raycastHit.distance;
            hit.point = raycastHit.point;
            hit.normal = raycastHit.normal;
        }
    }
}