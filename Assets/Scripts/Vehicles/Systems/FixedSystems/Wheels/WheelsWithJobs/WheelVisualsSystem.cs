﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Wheels;
using Vehicles.Components.Wheels.Serializable;

namespace Vehicles.Systems.FixedSystems.Wheels
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class WheelVisualsSystem : IFixedSystem
    {
        [Inject] private Stash<WheelView> _wheelView;
        [Inject] private Stash<WheelGroundHit> _wheelGroundHit;
        [Inject] private Stash<WheelVisualRoot> _wheelVisualRoot;
        
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<WheelView>()
                .With<WheelGroundHit>()
                .With<WheelVisualRoot>()
                .Without<WheelPaused>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _filter)
            {
                ref var view = ref _wheelView.Get(entity);
                ref var hit = ref _wheelGroundHit.Get(entity);
                ref var visual = ref _wheelVisualRoot.Get(entity);

                hit.angle = hit.angle % 360f + hit.angularVelocity * view.data.radius * 2f * Mathf.Rad2Deg * deltaTime;
                
                var rotation = hit.originRotation * Quaternion.Euler(hit.angle, 0f, 0f);
                visual.value.SetPositionAndRotation(hit.wheelPosition, rotation);
            }
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}