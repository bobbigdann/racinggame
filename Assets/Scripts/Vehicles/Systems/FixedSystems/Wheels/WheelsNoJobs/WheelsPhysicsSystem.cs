﻿using DI.Attributes;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Components.Wheels;
using Vehicles.Components.Wheels.Serializable;

namespace Vehicles.Systems.FixedSystems.Wheels.WheelsNoJobs
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class WheelsPhysicsSystem : IFixedSystem
    {
        public World World { get; set; }

        private Filter _filter;

        public void OnAwake()
        {
            _filter = World.Filter
                .With<WheelView>()
                .With<WheelGroundHit>()
                .With<WheelGroundParams>()
                .With<WheelVehicleLink>()
                .Without<WheelPaused>();
        }

        public void OnUpdate(float deltaTime)
        {
        }

        public void Dispose()
        {
            _filter = null;
        }
    }
}