﻿using System.Collections.Generic;
using UnityEngine;

namespace Vehicles.Data
{
    [CreateAssetMenu(fileName = "SurfaceParametersMapper", menuName = "Vehicles/Surface/Mapper", order = 0)]
    public class SurfaceParametersMapper : ScriptableObject
    {
        [SerializeField] public PacejkaSurfaceParameters[] surfaceParameters;
        
        private Dictionary<PhysicMaterial, PacejkaSurfaceParameters> _mappedParameters;

        public PacejkaSurfaceParameters GetByPhysicsMaterial(in PhysicMaterial material)
        {
            if (_mappedParameters == null || !_mappedParameters.TryGetValue(material, out var result))
                return surfaceParameters[0];

            return result;
        }

        public void Initialize()
        {
            _mappedParameters = new Dictionary<PhysicMaterial, PacejkaSurfaceParameters>();
            if (surfaceParameters == null) return;
            
            foreach (var parameter in surfaceParameters)
            {
                parameter.UpdateFrictionCurve();
                _mappedParameters.Add(parameter.mappingMaterial, parameter);
            }
        }

        public void Dispose()
        {
            _mappedParameters = null;
        }
    }
}