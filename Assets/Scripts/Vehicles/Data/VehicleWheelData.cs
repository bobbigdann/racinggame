﻿using UnityEngine;

namespace Vehicles.Data
{
    [CreateAssetMenu(fileName = "WheelData", menuName = "Vehicles/Data/Wheel", order = 0)]
    public class VehicleWheelData : ScriptableObject
    {
        [SerializeField] public float mass;
        [SerializeField] public float radius;
        
        [Header("Spring")]
        [SerializeField] public float maxForce;
        [SerializeField] public float maxLength;

        [Header("Damper")]
        [SerializeField] public float bumpForce;
        [SerializeField] public float reboundForce;

        [Header("Friction")]
        [SerializeField] public float forwardForceCoefficient;
        [SerializeField] public float forwardSlipCoefficient;
        [SerializeField] public float sidewaysForceCoefficient;
        [SerializeField] public float sidewaysSlipCoefficient;
        [SerializeField] [Range(0f, 1f)] public float frictionCircleStrength;
        [SerializeField] [Range(1f, 3f)] public float frictionCircleShape;
    }
}