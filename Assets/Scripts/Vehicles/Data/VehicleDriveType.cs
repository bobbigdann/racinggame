﻿namespace Vehicles.Data
{
    public enum VehicleDriveType
    {
        FrontWheelsDrive,
        RearWheelsDrive,
        AllWheelsDrive,
    }
}