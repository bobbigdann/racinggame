﻿using UnityEngine;

namespace Vehicles.Data
{
    [CreateAssetMenu(fileName = "SurfaceParameters", menuName = "Vehicles/Surface/Parameters", order = 0)]
    public class PacejkaSurfaceParameters : ScriptableObject
    {
        public PhysicMaterial mappingMaterial;
        
        [SerializeField] [Range(0f, 30f)] public float stiffness;
        [SerializeField] [Range(0f, 5f)] public float shape;
        [SerializeField] [Range(0f, 2f)] public float peak;
        [SerializeField] [Range(0f, 2f)] public float curvature;

        [SerializeField] [HideInInspector] public float[] curveValues;
        [SerializeField] public float dragTorque;
        [SerializeField] [Range(0f, 1f)] public float relativeDragTorque;
        
        [Header("Readonly parameters, will be overwritten")]
        [SerializeField] public float peakSlip;
        [SerializeField] public AnimationCurve curve;

        private float GetPeakSlip()
        {
            float peak = -1;
            float yMax = 0;

            for (float i = 0; i < 1f; i += 0.01f)
            {
                float y = curve.Evaluate(i);
                if (y <= yMax) continue;

                yMax = y;
                peak = i;
            }

            return peak;
        }

        private float GetFrictionValue(float slip, float b, float c, float d, float e)
        {
            float t = Mathf.Abs(slip);
            return d * Mathf.Sin(c * Mathf.Atan(b * t - e * (b * t - Mathf.Atan(b * t))));
        }

        private void UpdateFrictionСurveValues()
        {
            curveValues = new float[128];
            for (int i = 0; i < 128; i++)
            {
                curveValues[i] = curve.Evaluate(i / 128f);
            }
        }

        public void UpdateFrictionCurve()
        {
            curve = new AnimationCurve();
            var frames = new Keyframe[20];
            float t = 0f;

            for (int i = 0; i < frames.Length; i++)
            {
                float value = GetFrictionValue(t, stiffness, shape, peak, curvature);
                curve.AddKey(t, value);

                t += i <= 10
                    ? 0.02f
                    : 0.1f;
            }

            for (int i = 0; i < frames.Length; i++)
            {
                curve.SmoothTangents(i, 0f);
            }

            peakSlip = GetPeakSlip();
            UpdateFrictionСurveValues();
        }
    }
}