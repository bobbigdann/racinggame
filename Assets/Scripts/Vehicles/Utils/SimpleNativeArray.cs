﻿using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.IL2CPP.CompilerServices;

namespace Vehicles.Utils
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public unsafe struct SimpleNativeArray<T> : IDisposable where T : unmanaged
    {
        private void* _buffer;
        private int _length;
        private Allocator _allocator;

        public int Length => _length;

        public SimpleNativeArray(int length, Allocator allocator, NativeArrayOptions options = NativeArrayOptions.ClearMemory)
        {
            long totalSize = UnsafeUtility.SizeOf<T>() * (long)length;
            _buffer = UnsafeUtility.Malloc(totalSize, UnsafeUtility.AlignOf<T>(), allocator);

            if (allocator != Allocator.None && options == NativeArrayOptions.ClearMemory)
            {
                UnsafeUtility.MemClear(_buffer, totalSize);
            }

            _length = length;
            _allocator = allocator;
        }

        public T this[int index]
        {
            get => UnsafeUtility.ReadArrayElement<T>(_buffer, index);
            set => UnsafeUtility.WriteArrayElement(_buffer, index, value);
        }

        public ref T GetAsRef(int index)
        {
            return ref UnsafeUtility.AsRef<T>((T*)_buffer + index);
        }

        public void CopyTo(SimpleNativeArray<T> destination, int sourceLength)
        {
            if (_length > destination.Length)
            {
                throw new ArgumentException("Length of other array is less than length of this array");
            }
            
            UnsafeUtility.MemCpy(destination._buffer, _buffer, sourceLength * UnsafeUtility.SizeOf<T>());
        }
        
        public void CopyTo(T[] otherArray, int sourceLength)
        {
            if (_length > otherArray.Length)
            {
                throw new ArgumentException("Length of other array is less than length of this array");
            }
            
            fixed (void* destination = otherArray)
            {
                UnsafeUtility.MemCpy(destination, _buffer, sourceLength * UnsafeUtility.SizeOf<T>());
            }
        }

        public void Dispose()
        {
            if (_allocator != Allocator.None)
            {
                UnsafeUtility.Free(_buffer, _allocator);
            }

            _buffer = null;
            _length = 0;
        }

        public static SimpleNativeArray<T> AsViewOf(T[] array)
        {
            return new SimpleNativeArray<T>
            {
                _buffer = UnsafeUtility.AddressOf(ref array[0]),
                _length = array.Length,
                _allocator = Allocator.None,
            };
        }
    }
}