﻿using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Data;

namespace Vehicles.Components.Wheels.Serializable
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct WheelView : IComponent
    {
        public VehicleWheelData data;
        public Transform transform;
    }
}