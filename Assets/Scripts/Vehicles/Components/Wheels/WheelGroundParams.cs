﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Data;

namespace Vehicles.Components.Wheels
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct WheelGroundParams : IComponent
    {
        public PacejkaSurfaceParameters current;
        public PacejkaSurfaceParameters previous;
    }
}