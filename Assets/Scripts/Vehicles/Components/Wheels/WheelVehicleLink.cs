﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Vehicles.Components.Wheels
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct WheelVehicleLink : IComponent
    {
        public Entity value;
    }
}