﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Unity.Mathematics;

namespace Vehicles.Components.Wheels
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct WheelSuspension : IComponent
    {
        public float3 targetPoint;
        public float length;
        public float damperForce;
        public float velocity;
        public float compressionPercent;
        public float force;
        public float prevLength;
    }
}