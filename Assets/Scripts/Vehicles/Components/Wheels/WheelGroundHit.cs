﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Unity.Mathematics;
using UnityEngine;
using Vehicles.Utils;

namespace Vehicles.Components.Wheels
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct WheelGroundHit : IComponent
    {
        public bool grounded;
        
        public float forwardSlip;
        public float forwardSpeed;
        public float forwardForce;

        public float sidewaysSlip;
        public float sidewaysSpeed;
        public float sidewaysForce;

        public float3 totalForce;

        public float3 originPosition;
        public quaternion originRotation;

        public float radius;
        public float mass;
        public float angle;
        public float angularVelocity;

        public Matrix4x4 worldToLocal;
        
        public float suspensionMaxLength;
        public float suspensionMaxForce;
        public float suspensionBumpForce;
        public float suspensionReboundForce;

        public float suspensionLoad;

        public float distance;
        public float maxDistance;

        public float frictionCircleStrength;
        public float frictionCircleShape;

        public float3 forward;
        public float3 up;
        public float3 right;

        public float3 forwardDir;
        public float3 sidewaysDir;
        public float3 point;
        public float3 normal;

        public float3 wheelPosition;

        public float forwardSlipCoefficient;
        public float forwardForceCoefficient;
        public float sidewaysSlipCoefficient;
        public float sidewaysForceCoefficient;

        public SimpleNativeArray<float> curveValues;
        public float peakSlip;
        public float dragTorque;
        public float relativeDragTorque;
    }
}