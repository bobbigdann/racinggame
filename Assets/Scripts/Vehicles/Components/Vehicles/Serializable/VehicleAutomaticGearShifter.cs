﻿using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.ScriptableObjects;

namespace Vehicles.Components.Vehicles.Serializable
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct VehicleAutomaticGearShifter : IComponent
    {
        [SerializeField] public VehicleAutomaticGearShifterData data;
    }
}