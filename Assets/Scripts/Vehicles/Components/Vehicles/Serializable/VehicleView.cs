﻿using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Data;
using Vehicles.ScriptableObjects;

namespace Vehicles.Components.Vehicles.Serializable
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct VehicleView : IComponent
    {
        public Transform centerOfMass;

        public VehicleDriveType driveType;
        public VehicleEngineData engineData;
        public VehicleHelpersData helpers;
        public VehicleSteeringData steering;
        public VehicleTransmissionData transmission;
    }
}