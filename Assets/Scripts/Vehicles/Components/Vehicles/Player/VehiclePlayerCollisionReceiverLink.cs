﻿using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.MonoComponents;

namespace Vehicles.Components.Vehicles.Player
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct VehiclePlayerCollisionReceiverLink : IComponent
    {
        public VehicleCollisionReceiver value;
    }
}