﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Vehicles.Components.Vehicles.Player
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct VehiclePlayerInitialize : IComponent
    {
        
    }
}