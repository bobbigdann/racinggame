﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Vehicles.Components.Vehicles.Physics
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct VehicleWheels : IComponent
    {
        public Entity[] all;
        public Entity[] steering;
        public Entity[] power;
        public Entity[] handbrake;

        public EntityId[] allEntityIds;
    }
}