﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Vehicles.Utils;

namespace Vehicles.Components.Vehicles.Physics
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct VehicleWheelsIdsUnsafe : IComponent
    {
        public SimpleNativeArray<EntityId> value;
    }
}