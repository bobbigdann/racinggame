﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using Unity.Mathematics;
using UnityEngine;

namespace Vehicles.Components.Vehicles.Physics
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct VehicleSubStepData : IComponent
    {
        public Matrix4x4 localToWorld;

        public float3 position;
        public quaternion rotation;
        
        public float3 velocity;
        public float3 angularVelocity;

        public float drag;
        public float angularDrag;
        public float mass;
        public float invertedMass;

        public float3 inertia;
        public float3 invertedInertia;
        public float3 worldInvertedInertia;

        public float3 centerOfMass;

        public int stepsCount;

        public float3 linearImpulse;
        public float3 totalLinearImpulse;
        public float3 angularImpulse;
        public float3 totalAngularImpulse;

        public float3 transformForward;
        public float3 transformRight;
        public float3 transformUp;

        public float3 stepInitPosition;
        public quaternion stepInitRotation;

        public float3 stepPositionDelta;
        public quaternion stepRotationDelta;
    }
}