﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Vehicles.Components.Vehicles
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct VehicleUnityView : IComponent
    {
        public GameObject gameObject;
        public Transform transform;
        public Rigidbody rigidbody;
    }
}