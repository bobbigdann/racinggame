﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Vehicles.Components.Vehicles
{
    public struct VehiclePaused : IComponent
    {
        public Vector3 pausedVelocity;
    }
}