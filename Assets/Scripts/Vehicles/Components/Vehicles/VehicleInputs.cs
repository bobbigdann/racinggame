﻿using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Vehicles.Components.Vehicles
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct VehicleInputs : IComponent
    {
        public bool gasPressed;
        public bool brakePressed;
        public bool handbrakePressed;
        public bool turnRightPressed;
        public bool turnLeftPressed;
        public bool gearShiftUpPressed;
        public bool gearShiftDownPressed;
    }
}