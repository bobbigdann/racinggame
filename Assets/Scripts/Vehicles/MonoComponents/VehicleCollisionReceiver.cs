﻿using System;
using UnityEngine;

namespace Vehicles.MonoComponents
{
    public class VehicleCollisionReceiver : MonoBehaviour
    {
        public Action onCollision;

        public void OnTriggerEnter(Collider other)
        {
            onCollision?.Invoke();
        }
    }
}