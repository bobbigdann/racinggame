﻿using UnityEngine;

namespace Vehicles.MonoComponents
{
    public class VehicleMonoDebugComponent : MonoBehaviour
    {
        public bool isGoingReverse;
        public float speed;
        public float torque;
        public float torquePerWheel;
        public int rpm;
        public int gear;
    }
}