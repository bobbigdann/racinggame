﻿using DI.Builders;
using Scellecs.Morpeh;

namespace Scripts
{
    public interface ISystemsFeature
    {
        public SystemsGroup GetGroup(World world, IContainerBuilder container);
    }
}