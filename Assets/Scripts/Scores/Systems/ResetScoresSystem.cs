﻿using DI.Attributes;
using Scellecs.Morpeh;
using Scores.Components;
using Unity.IL2CPP.CompilerServices;

namespace Scores.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class ResetScoresSystem : ISystem
    {
        [Inject] private Stash<CurrentScore> _currentScore;
        
        public World World { get; set; }

        private Filter _scoresFilter;
        private Filter _resetFilter;

        public void OnAwake()
        {
            _scoresFilter = World.Filter.With<CurrentScore>();
            _resetFilter = World.Filter.With<ResetScores>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_resetFilter.IsEmpty()) return;

            foreach (var entity in _scoresFilter)
            {
                ref var score = ref _currentScore.Get(entity);
                score.value = 0;
            }
        }

        public void Dispose()
        {
            _scoresFilter = null;
            _resetFilter = null;
        }
    }
}