﻿using Common.Components;
using Scripts.Settings.Settings;
using DI.Attributes;
using Scellecs.Morpeh;
using Scores.Components;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using Vehicles.Components.Vehicles;
using Vehicles.Components.Vehicles.Player;

namespace Scores.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class GetCurrentScoreSystem : ISystem
    {
        [Inject] private LevelSettings _levelSettings;

        [Inject] private Stash<VehicleUnityView> _vehicleUnityView;
        [Inject] private Stash<CurrentScore> _currentScore;
        
        public World World { get; set; }

        private Filter _scoreFilter;
        private Filter _playerFilter;

        public void OnAwake()
        {
            _scoreFilter = World.Filter
                .With<CurrentScore>();

            _playerFilter = World.Filter
                .With<VehicleUnityView>()
                .With<VehicleControlledByLocalPlayer>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_playerFilter.IsEmpty() || _scoreFilter.IsEmpty()) return;

            int score = 0;
            foreach (var entity in _playerFilter)
            {
                ref var unityView = ref _vehicleUnityView.Get(entity);
                float z = unityView.transform.position.z;
                score = Mathf.FloorToInt(z / _levelSettings.metersForScores);
            }

            foreach (var entity in _scoreFilter)
            {
                ref var currentScore = ref _currentScore.Get(entity);
                currentScore.value = score;
            }
        }

        public void Dispose()
        {
            _playerFilter = null;
            _scoreFilter = null;
        }
    }
}