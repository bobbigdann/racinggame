﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Scores.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct CurrentScore : IComponent
    {
        public int value;
    }
}