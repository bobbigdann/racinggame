﻿using DI.Attributes;
using Gameplay.Components;
using Scellecs.Morpeh;
using Player;
using Res.Extensions;
using Scores.Components;
using Settings.ScriptableObjects.References;
using Unity.IL2CPP.CompilerServices;

namespace Gameplay.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public class LoseGameSystem : ISystem
    {
        [Inject] private readonly SettingsContainer _settingsContainer;
        [Inject] private readonly IPlayerData _playerData;
        
        [Inject] private Stash<CurrentScore> _currentScore;
        [Inject] private Stash<LoseGame> _loseGame;
        
        public World World { get; set; }

        private Filter _loseFilter;
        private Filter _scoreFilter;
        
        public void OnAwake()
        {
            _loseFilter = World.Filter.With<LoseGame>();
            
            _scoreFilter = World.Filter
                .With<CurrentScore>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _loseFilter)
            {
                _loseGame.Remove(entity);
                
                foreach (var scoreEntity in _scoreFilter)
                {
                    ref var currentScore = ref _currentScore.Get(scoreEntity);
                    _playerData.GiveMoney((int) (currentScore.value * _settingsContainer.CoefficientScoreToMoney));
                }
            }
        }

        public void Dispose()
        {
            _loseFilter = null;
            _scoreFilter = null;
        }
    }
}