﻿namespace Constants
{
    public static class VehicleHudConstants
    {
        public const float SpeedToViewMultiplier = 3.6f;
        public const float RpmToViewMultiplier = 3.6f;
        public const float MaxViewParameterValue = 100f;
    }
}