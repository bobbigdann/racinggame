﻿using UnityEngine;

namespace Constants
{
    public static class VehicleConstants
    {
        public const float VelocityToSpeedMultiplier = 3.6f;
        public const float MinSpeedForBrake = 1;

        public const int WheelRaycastLayerMask = Physics.AllLayers & ~Physics.IgnoreRaycastLayer;
    }
}