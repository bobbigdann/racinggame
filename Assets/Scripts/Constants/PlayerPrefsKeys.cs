﻿namespace Constants
{
    public static class PlayerPrefsKeys
    {
        public const string EffectVolume = "EFFECTS_VOLUME_KEY";
        public const string MusicVolume = "MUSIC_VOLUME_KEY";
        public const string VehicleId = "VEHICLE_ID_KEY";
        public const string PlayerData = "PLAYER_DATA_KEY";
        public const string HudControlsEnabled = "HUD_CONTROLS_ENABLED";
    }
}