﻿using Unity.VisualScripting;
using UnityEngine;

namespace Pools
{
    public class AudioSourcePool
    {
        private readonly Transform _root;
        private readonly Pool<AudioSource> _pool;
        
        public AudioSourcePool()
        {
            _root = new GameObject("AudioSourcesPool").transform;
            _pool = new Pool<AudioSource>(() =>
            {
                var obj = new GameObject("AudioSource");
                obj.transform.SetParent(_root);

                var audioSource = obj.AddComponent<AudioSource>();
                return audioSource;
            }, 10);
        }

        public AudioSource Take()
        {
            return _pool.Take();
        }

        public void Return(AudioSource source)
        {
            _pool.Return(source);
        }
    }
}