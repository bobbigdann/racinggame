﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Res.Data;

namespace JsonModels
{
    [JsonObject]
    public class UserModel
    {
        [JsonProperty("resources")] public Dictionary<ResourceType, int> Resources = new();
        [JsonProperty("vehicles")] public List<int> Vehicles = new ();
    }
}