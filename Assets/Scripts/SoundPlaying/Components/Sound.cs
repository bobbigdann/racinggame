﻿using Scellecs.Morpeh;
using SoundPlaying.Data;
using Unity.IL2CPP.CompilerServices;

namespace SoundPlaying.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct Sound : IComponent
    {
        public SoundType type;
    }
}