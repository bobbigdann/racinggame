﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace SoundPlaying.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct SoundVolume : IComponent
    {
        public float Value;
    }
}