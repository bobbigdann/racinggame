﻿using Scellecs.Morpeh;
using SoundPlaying.Data;
using SoundPlaying.Extensions;
using UnityEngine;

namespace SoundPlaying.Builder
{
    public readonly struct SoundEntityBuilder
    {
        private readonly Entity _entity;
        
        public static implicit operator Entity(SoundEntityBuilder entityBuilder)
        {
            return entityBuilder._entity;
        }

        public SoundEntityBuilder(Entity entity)
        {
            _entity = entity;
        }
        
        public SoundEntityBuilder SetVolume(float volume)
        {
            _entity.SetVolume(volume);
            return this;
        }

        public SoundEntityBuilder SetAudioSource(AudioSource source)
        {
            _entity.SetAudioSource(source);
            return this;
        }

        public SoundEntityBuilder SetAudioClip(AudioClip clip)
        {
            _entity.SetAudioClip(clip);
            return this;
        }

        public SoundEntityBuilder SetCategory(SoundCategory category)
        {
            _entity.SetCategory(category);
            return this;
        }

        public SoundEntityBuilder SetLoop(bool isLoop)
        {
            _entity.SetLoop(isLoop);
            return this;
        }

        public SoundEntityBuilder Play()
        {
            _entity.Play();
            return this;
        }

        public SoundEntityBuilder Stop()
        {
            _entity.Stop();
            return this;
        }
    }
}