﻿using System;
using System.Collections.Generic;
using System.Linq;
using SoundPlaying.Data;
using UnityEngine;

namespace SoundPlaying.ScriptableObjects
{
    [Serializable]
    public class Sound
    {
        public SoundType soundType;
        public SoundCategory soundCategory;
        public AudioClip audioClip;
        
        [Range(0f, 1f)]
        public float soundVolume;
    }
    
    [CreateAssetMenu(fileName = "SoundData", menuName = "SoundData/SoundData")]
    public class SoundData : ScriptableObject
    {
        [SerializeField] private Sound[] _sounds;

        private void OnValidate()
        {
            var length = Enum.GetNames(typeof(SoundType)).Length;
            if (_sounds.Length != length) Array.Resize(ref _sounds, length);
            
            for (var i = 0; i < length; i++)
            {
                _sounds[i] ??= new Sound();
                _sounds[i].soundType = (SoundType) i;
            }
        }

        public Sound GetSoundByName(SoundType type)
        {
            var sound = _sounds.FirstOrDefault(t => t.soundType == type);
            if (sound == null)
            {
                throw new KeyNotFoundException($"Sound Data: Sound with name {type} not found");
            }
            return sound;
        }
    }
}