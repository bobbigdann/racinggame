﻿using Common.Components;
using Scellecs.Morpeh;
using SoundPlaying.Components;
using UnityEngine;

namespace SoundPlaying.Extensions
{
    public static class SoundEntityExtensions
    {
        public static void SetAudioSource(this Entity entity, AudioSource audioSource)
        {
            entity.SetComponent(new SoundAudioSource()
            {
                Value = audioSource
            });
            entity.SetComponent(new AudioSourceChanged());
        }
        
        public static void SetVolume(this Entity entity, float volume)
        {
            entity.SetComponent(new SoundVolume()
            {
                Value = volume
            });
            entity.SetComponent(new VolumeChanged());
        }
        
        public static void SetAudioClip(this Entity entity, AudioClip audioClip)
        {
            entity.SetComponent(new SoundAudioClip()
            {
                Value = audioClip
            });
            entity.SetComponent(new AudioClipChanged());
        }

        public static void SetLoop(this Entity entity, bool isLoop)
        {
            if(isLoop) entity.SetComponent(new Loop());
            entity.SetComponent(new LoopChanged());
        }

        public static void SetCategory(this Entity entity, Data.SoundCategory category)
        {
            entity.SetComponent(new SoundCategory()
            {
                Value = category
            });
        }

        public static void Play(this Entity entity)
        {
            entity.SetComponent(new SoundPlay());
        }

        public static void Stop(this Entity entity)
        {
            entity.SetComponent(new Destroy());
        }
    }
}