﻿namespace SoundPlaying.Data
{
    public enum SoundCategory
    {
        Music = 0,
        Effects = 1,
    }
}