﻿namespace SoundPlaying.Data
{
    public enum SoundType
    {
        RaceMusic = 0,
        MainTheme = 1,
        ClickButton = 2,
    }
}