﻿using Common.Components;
using Scripts.Settings.Settings;
using DI.Attributes;
using Scellecs.Morpeh;
using Settings.Components;
using SoundPlaying.Components;

namespace SoundPlaying.UpdateSystems
{
    public class SoundVolumeSystem : ISystem
    {
        [Inject] private SoundSettings _soundSettings;
        
        [Inject] private Stash<VolumeChanged> _changedVolumeCache;
        [Inject] private Stash<SoundAudioSource> _audioSourcesCache;
        [Inject] private Stash<SoundVolume> _volumeCache;
        [Inject] private Stash<SoundCategory> _categoryCache;
        
        public World World { get; set; }

        private Filter _soundFilter;
        private Filter _settingsFilter;
        
        public void OnAwake()
        {
            _soundFilter = World.Filter
                .With<Sound>()
                .With<SoundAudioSource>()
                .With<SoundVolume>()
                .With<SoundCategory>()
                .With<VolumeChanged>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _soundFilter)
            {
                var audioSource = _audioSourcesCache.Get(entity).Value;
                var volume = _volumeCache.Get(entity).Value;
                var category = _categoryCache.Get(entity).Value;

                audioSource.volume = GetGlobalVolume(category, volume);

                _changedVolumeCache.Remove(entity);
            }
        }

        private float GetGlobalVolume(Data.SoundCategory category, float multiplier)
        {
            return category switch
            {
                Data.SoundCategory.Effects => _soundSettings.EffectsVolume * multiplier,
                Data.SoundCategory.Music => _soundSettings.MusicVolume * multiplier,
                _ => multiplier
            };
        }

        public void Dispose()
        {
            _soundFilter = null;
        }
    }
}