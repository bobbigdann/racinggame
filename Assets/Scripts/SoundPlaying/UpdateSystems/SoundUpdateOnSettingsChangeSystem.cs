﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Settings.Components;
using SoundPlaying.Components;

namespace SoundPlaying.UpdateSystems
{
    public class SoundUpdateOnSettingsChangeSystem : ISystem
    {
        [Inject] private Stash<VolumeChanged> _volumeCache;
        
        public World World { get; set; }

        private Filter _soundFilter;
        private Filter _eventFilter;

        public void OnAwake()
        {
            _soundFilter = World.Filter
                .With<Sound>()
                .With<SoundVolume>()
                .Without<VolumeChanged>();

            _eventFilter = World.Filter
                .With<GlobalSettingsChanged>();
        }

        public void OnUpdate(float deltaTime)
        {
            if (_eventFilter.IsEmpty()) return;
            
            foreach (var sound in _soundFilter)
            {
                _volumeCache.Add(sound);
            }
        }
        
        public void Dispose()
        {
            _soundFilter = null;
            _eventFilter = null;
        }
    }
}