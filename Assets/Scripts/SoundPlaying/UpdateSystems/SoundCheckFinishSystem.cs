﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using SoundPlaying.Components;

namespace SoundPlaying.UpdateSystems
{
    public class SoundCheckFinishSystem : ISystem
    {
        [Inject] private Stash<Destroy> _destroyCache;
        [Inject] private Stash<SoundAudioSource> _audioSourcesCache;
        
        public World World { get; set; }

        private Filter _soundsFilter;

        public void OnAwake()
        {
            _soundsFilter = World.Filter
                .With<Sound>()
                .With<SoundAudioSource>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _soundsFilter)
            {
                var audioSource = _audioSourcesCache.Get(entity);
                if (!audioSource.Value.isPlaying)
                {
                    _destroyCache.Add(entity);
                }
            }
        }
        
        public void Dispose()
        {
            _soundsFilter = null;
        }
    }
}