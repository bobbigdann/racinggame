﻿using DI.Attributes;
using Scellecs.Morpeh;
using SoundPlaying.Components;

namespace SoundPlaying.UpdateSystems
{
    public class SoundPlaySystem : ISystem
    {
        [Inject] private Stash<SoundAudioSource> _audioSourcesCache;
        [Inject] private Stash<SoundPlay> _playCache;
        
        public World World { get; set; }

        private Filter _soundFilter;
        
        public void OnAwake()
        {
            _soundFilter = World.Filter
                .With<Sound>()
                .With<SoundAudioSource>()
                .With<SoundPlay>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _soundFilter)
            {
                var audioSource = _audioSourcesCache.Get(entity).Value;
                audioSource.Play();

                _playCache.Remove(entity);
            }
        }
        
        public void Dispose()
        {
            _soundFilter = null;
        }
    }
}