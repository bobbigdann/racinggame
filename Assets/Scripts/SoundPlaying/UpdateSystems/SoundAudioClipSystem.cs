﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using SoundPlaying.Components;

namespace SoundPlaying.UpdateSystems
{
    public class SoundAudioClipSystem : ISystem
    {
        [Inject] private Stash<AudioClipChanged> _changedClipCache;
        [Inject] private Stash<SoundAudioSource> _audioSourcesCache;
        [Inject] private Stash<SoundAudioClip> _audioClipCache;
        
        public World World { get; set; }

        private Filter _soundFilter;
        
        public void OnAwake()
        {
            _soundFilter = World.Filter
                .With<Sound>()
                .With<SoundAudioSource>()
                .With<SoundAudioClip>()
                .With<AudioClipChanged>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _soundFilter)
            {
                var audioSource = _audioSourcesCache.Get(entity).Value;
                var audioClip = _audioClipCache.Get(entity).Value;

                audioSource.clip = audioClip;

                _changedClipCache.Remove(entity);
            }
        }
        
        public void Dispose()
        {
            _soundFilter = null;
        }
    }
}