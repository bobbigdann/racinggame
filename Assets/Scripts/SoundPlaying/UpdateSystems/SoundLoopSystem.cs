﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using SoundPlaying.Components;

namespace SoundPlaying.UpdateSystems
{
    public class SoundLoopSystem : ISystem
    {
        [Inject] private Stash<LoopChanged> _changedLoopCache;
        [Inject] private Stash<SoundAudioSource> _audioSourcesCache;
        [Inject] private Stash<Loop> _loopCache;
        
        public World World { get; set; }

        private Filter _soundFilter;
        
        public void OnAwake()
        {
            _soundFilter = World.Filter
                .With<Sound>()
                .With<SoundAudioSource>()
                .With<LoopChanged>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _soundFilter)
            {
                _loopCache.Get(entity, out var exists);
                var audioSource = _audioSourcesCache.Get(entity).Value;
                audioSource.loop = exists;

                _changedLoopCache.Remove(entity);
            }
        }
        
        public void Dispose()
        {
            _soundFilter = null;
        }
    }
}