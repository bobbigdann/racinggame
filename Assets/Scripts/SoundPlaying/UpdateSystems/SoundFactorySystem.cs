﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Pools;
using SoundPlaying.Builder;
using SoundPlaying.Data;
using SoundPlaying.ScriptableObjects;
using UnityEngine;
using Sound = SoundPlaying.Components.Sound;

namespace SoundPlaying.UpdateSystems
{
    public class SoundFactorySystem : IInitializer
    {
        [Inject] private readonly AudioSourcePool _audioSourcePool;
        [Inject] private readonly SoundData _soundData;
        
        [Inject] private Stash<Sound> _soundCache;
        [Inject] private Stash<Destroy> _destroy;

        public World World { get; set; }

        private Filter _soundFilter;

        public void OnAwake()
        {
            _soundFilter = World.Filter.With<Sound>();
        }

        public SoundEntityBuilder Create(SoundCategory soundCategory)
        {
            var entity = World.CreateEntity();
            _soundCache.Add(entity);
            
            return new SoundEntityBuilder(entity)
                .SetCategory(soundCategory)
                .SetAudioSource(_audioSourcePool.Take());
        }

        public SoundEntityBuilder Create(SoundType soundType)
        {
            var entity = World.CreateEntity();
            _soundCache.Set(entity, new Sound
            {
                type = soundType,
            });

            var data = _soundData.GetSoundByName(soundType);
            return new SoundEntityBuilder(entity)
                .SetAudioSource(_audioSourcePool.Take())
                .SetCategory(data.soundCategory)
                .SetVolume(data.soundVolume)
                .SetAudioClip(data.audioClip)
                .SetLoop(false);
        }

        public void Destroy(SoundType soundType)
        {
            foreach (var entity in _soundFilter)
            {
                ref var sound = ref _soundCache.Get(entity);
                if (sound.type == soundType)
                    _destroy.Add(entity);
            }
        }
        
        public void Dispose()
        {
            
        }
    }
}