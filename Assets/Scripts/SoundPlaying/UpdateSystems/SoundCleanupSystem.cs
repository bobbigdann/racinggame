﻿using Common.Components;
using DI.Attributes;
using Scellecs.Morpeh;
using Pools;
using SoundPlaying.Components;

namespace SoundPlaying.UpdateSystems
{
    public class SoundCleanupSystem : ILateSystem
    {
        [Inject] private readonly AudioSourcePool _audioSourcePool;
        
        [Inject] private Stash<SoundAudioSource> _audioSourcesCache;
        
        private Filter _soundsFilter;
        
        public World World { get; set; }
        public void OnAwake()
        {
            _soundsFilter = World.Filter
                .With<Sound>()
                .With<SoundAudioSource>()
                .With<Destroy>();
        }

        public void OnUpdate(float deltaTime)
        {
            foreach (var entity in _soundsFilter)
            {
                var audioSource = _audioSourcesCache.Get(entity);

                if (audioSource.Value != null)
                {
                    audioSource.Value.Stop();
                    audioSource.Value.clip = null;
                    
                    _audioSourcePool.Return(audioSource.Value);
                }
            }
        }
        
        public void Dispose()
        {
            _soundsFilter = null;
        }
    }
}